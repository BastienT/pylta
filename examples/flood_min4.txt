PARAMETERS: n

LAYERS: L, L
STATES: L.0, L.1, L.2, L.3, L.crashing0, L.crashing1, L.crashing2, L.crashing3, L.crashed

CASE L.0:
  IF TRUE THEN L.0
  IF TRUE THEN L.crashing0

CASE L.1:
  IF L.0 + L.crashing0 > 0 THEN L.0
  IF L.0 + L.crashing0 > 0 THEN L.crashing0
  IF L.0 == 0 THEN L.1
  IF L.0 == 0 THEN L.crashing1

CASE L.2:
  IF L.0 + L.crashing0 > 0 THEN L.0
  IF L.0 + L.crashing0 > 0 THEN L.crashing0
  IF L.0 == 0 & L.1 + L.crashing1 > 0 THEN L.1
  IF L.0 == 0 & L.1 + L.crashing1 > 0 THEN L.crashing1
  IF L.0 == 0 & L.1 == 0 THEN L.2
  IF L.0 == 0 & L.1 == 0 THEN L.crashing2

CASE L.3:
  IF L.0 + L.crashing0 > 0 THEN L.0
  IF L.0 + L.crashing0 > 0 THEN L.crashing0
  IF L.0 == 0 & L.1 + L.crashing1 > 0 THEN L.1
  IF L.0 == 0 & L.1 + L.crashing1 > 0 THEN L.crashing1
  IF L.0 == 0 & L.1 == 0 & L.2 + L.crashing2 > 0 THEN L.2
  IF L.0 == 0 & L.1 == 0 & L.2 + L.crashing2 > 0 THEN L.crashing2
  IF L.0 == 0 & L.1 == 0 & L.2 == 0 THEN L.3
  IF L.0 == 0 & L.1 == 0 & L.2 == 0 THEN L.crashing3

CASE L.crashing0:
  IF TRUE THEN L.crashed

CASE L.crashing1:
  IF TRUE THEN L.crashed

CASE L.crashing2:
  IF TRUE THEN L.crashed

CASE L.crashing3:
  IF TRUE THEN L.crashed

CASE L.crashed:
  IF TRUE THEN L.crashed

WITH
  L.initial: L.0 + L.1 + L.2 + L.3 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
  L.one0: L.0 + L.crashing0 > 0
VERIFY: L.initial & ! L.one0 -> ! F L.one0

WITH
  L.initial: L.0 + L.1 + L.2 + L.3 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
  L.one1: L.1 + L.crashing1 > 0
VERIFY: L.initial & ! L.one1 -> ! F L.one1

WITH
  L.initial: L.0 + L.1 + L.2 + L.3 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
  L.one2: L.2 + L.crashing2 > 0
VERIFY: L.initial & ! L.one2 -> ! F L.one2

WITH
  L.initial: L.0 + L.1 + L.2 + L.3 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
  L.one3: L.3 + L.crashing3 > 0
VERIFY: L.initial & ! L.one3 -> ! F L.one3

// Liveness with k = 1
WITH
  L.full: L.0 + L.1 + L.2 + L.3 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
  L.clean_round: L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 < 1
  L.decide0: L.0 + L.crashing0 + L.crashed == n
  L.decide1: L.1 + L.crashing1 + L.crashed == n
  L.decide2: L.2 + L.crashing2 + L.crashed == n
  L.decide3: L.3 + L.crashing3 + L.crashed == n
VERIFY: G L.full -> G (L.clean_round -> X (G L.decide0 | G L.decide1 | G L.decide2 | G L.decide3))

// Liveness with k = 2
WITH
  L.full: L.0 + L.1 + L.2 + L.3 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
  L.clean_round: L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 < 2
  L.decide01: L.0 + L.1 + L.crashing0 + L.crashing1 + L.crashed == n
  L.decide02: L.0 + L.2 + L.crashing0 + L.crashing2 + L.crashed == n
  L.decide03: L.0 + L.3 + L.crashing0 + L.crashing3 + L.crashed == n
  L.decide12: L.1 + L.2 + L.crashing1 + L.crashing2 + L.crashed == n
  L.decide13: L.1 + L.3 + L.crashing1 + L.crashing3 + L.crashed == n
  L.decide23: L.2 + L.3 + L.crashing2 + L.crashing3 + L.crashed == n
VERIFY: G L.full -> G (L.clean_round -> X (G L.decide01 | G L.decide02 | G L.decide03 | G L.decide12 | G L.decide13 | G L.decide23))

// Liveness with k = 3
WITH
  L.full: L.0 + L.1 + L.2 + L.3 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
  L.clean_round: L.crashing0 + L.crashing1 + L.crashing2 + L.crashing3 < 3
  L.decide012: L.0 + L.1 + L.2 + L.crashing0 + L.crashing1 + L.crashing2 + L.crashed == n
  L.decide013: L.0 + L.1 + L.3 + L.crashing0 + L.crashing1 + L.crashing3 + L.crashed == n
  L.decide023: L.0 + L.2 + L.3 + L.crashing0 + L.crashing2 + L.crashing3 + L.crashed == n
  L.decide123: L.1 + L.2 + L.3 + L.crashing1 + L.crashing2 + L.crashing3 + L.crashed == n
VERIFY: G L.full -> G (L.clean_round -> X (G L.decide012 | G L.decide013 | G L.decide023 | G L.decide123))
