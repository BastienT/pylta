# Abstract
This artifact contains the model checking tool PyLTA described in our tool demonstration paper submission. The tool is written in Python 3, so all sources
are available. It is meant to be distributed with the GNU GPL 3 license.

The tool allows the reviewers to run model checking tasks on examples mentioned in the paper such as a model of the Phase King protocol.
The reviewer will be able to reproduce the examples mentioned in the core of the paper as well as the simpler one in the appendix meant for the public demonstration.

# TL;DR
Run

    install.sh

to install the tool with an **Internet connection**.
Run

    ./run_examples.sh

to run four main examples.

# Additional Requirements
There is only one proprietary package requirement which requires Internet connection: MathSAT SMT solver. 
The `install.sh` script installs all Ubuntu and Python packages (included here),
then runs:
    pysmt-install --msat
    pysmt-install --bdd
which downloads and installs the SMT solver, as well as a BDD package.

All other requirements are provided offline in the `packages` directory, and installed by the script.

# Reproducibility and Experiment Runtime
The script `run_examples.sh` executes 4 examples mentioned in the paper within a couple of seconds.

Other examples can be found in the `examples` directory.
An input file in this directory can be verified by running `python3 -m pylta [INPUT]`.

For instance,

    python3 -m pylta examples/flood_min2.txt

which is the model mentioned in the appendix.

