# Pylta

## Description

This project aims at providing tools for building and verifying Layered Threshold Automata (LTA). The concepts are explained in [this publication](https://drops.dagstuhl.de/opus/volltexte/2021/14392/).

## TLDR

Run the following command to run a few selected examples

        ./run_examples.sh

## Dependencies

- [python](https://www.python.org/) version 3.10.4. You can probably use your package manager to install it.
- [lark](https://pypi.org/project/lark/) version 1.1.2. I installed it with the command:

        python3 -m pip install lark
- [swig]
	sudo dnf install swig

- [gmpy2]
	python3 -m pip install gmpy2
      
- [spot](https://spot.lrde.epita.fr/) version 2.10.6. On Fedora, I had to run the following commands:

        sudo dnf config-manager --add-repo https://www.lrde.epita.fr/repo/fedora/lrde.repo
        sudo dnf install spot python3-spot spot-devel spot-doc

alternatively, compile SPOT from sources:
	./configure --prefix ~/.local
	make
	make install

If module spot is not found, you might have to do
	cp -r ~/.local/lib64/python3.10/site-packages/*buddy* ~/.local/lib/python3.10/site-packages/
	cp -r ~/.local/lib64/python3.10/site-packages/spot ~/.local/lib/python3.10/site-packages/

- [pysmt](https://pypi.org/project/PySMT/) version 0.9.0. I installed it with the command:

        python3 -m pip install PySMT

- [MathSat](https://mathsat.fbk.eu/) version 4.8.7. It should be present in `PYTHONPATH`. I installed it through pysmt using the command:

        pysmt-install --msat

- [CUDD](https://www.davidkebo.com/cudd) PySMT provides the required Python bindings when installed through the command:

        pysmt-install --bdd

The version numbers are indicative, any recent installation will probably work fine.

## Usage

- To run an example (for example reliable broadcast), enter the following command in the directory where this README is located.

        python3 -m pylta examples/rel_broadcast.txt

  This should print if the objectives defined in the input are valid and if not, show a counter example.

- Adding a `-v` option will show the abstract counter examples found as well as the predicates as they are added:

        python3 -m pylta examples/rel_broadcast.txt -v

- Adding a `-t` option will print the parsing and verification time:

        python3 -m pylta examples/rel_broadcast.txt -t

  The parsing time is counted separately because the parser is not yet optimised and can take more time than the verification itself on the smaller examples.

- Alternatively, the script [run\_examples.sh](run_examples.sh) will automatically launch a few select examples

        ./run_examples.sh

- The script [generate\_examples.sh](./generate_examples.sh) can create some additional examples in the [generated\_examples](./generated_examples) folder. For example:

        ./generate_examples.sh
        python3 -m pylta generated_examples/flood_min_termination_2_1.txt

  Note that these are mainly used to test the limits of PyLTA and are not guaranteed to run in a reasonable amount of time.

## License

PyLTA is released under the [GNU GENERAL PUBLIC LICENSE v3.0](https://www.gnu.org/licenses/gpl-3.0.html), you should have recieved a copy of the License with the program, it is otherwise available using the provided Link.
