
echo ''
echo ''
echo '  1/ Phase King Algorithm'
python3 -m pylta examples/phase_king.txt -t

echo ''
echo ''
echo '  2/ Phase King Algorithm with one too many faulty process'
python3 -m pylta examples/wrong_phase_king.txt -t

echo ''
echo ''
echo '  3/ Crash tolerant Ben Or Algorithm'
python3 -m pylta examples/ben_or_crash.txt -t

echo ''
echo ''
echo '  4/ Crash Tolerant Ben Or, first without termination assumtions then with one too many crash'
python3 -m pylta examples/wrong_ben_or_crash.txt -t
