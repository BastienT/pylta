"""
From 'Randomized k-set agreement in crash-prone and Byzantine
asynchronous systems' published in 2017 by Achour Mostefaoui, Hamouma
Mounen and Michel Raynal.

Crashed processes are encoded in a second LTA, a copy of the original,
but where the fairness conditions are only imposed on the first lta,
meaning htat processes in the faulty copy can stop at any time.  This
makes it easy to see what happens if more processes are allowed to
crash simply by changing the initial conditions.
"""
import itertools

import pylta.api

def k_set_agreement(m, k, goal):
    lta = pylta.api.lta()
    # Parameters
    n = lta.add_parameter("n")
    t = lta.add_parameter("t")
    f = lta.add_parameter("f")
    lta.add_parameter_relation(2*t < n)
    nd = lta.add_parameter("nd") # represents |n/(k+1)|
    lta.add_parameter_relation((k+1)*nd <= n) #k*n is linear because k is an integer and not a variable
    lta.add_parameter_relation((k+1)*(nd+1) > n)
    lta.add_parameter_relation(t < n - k*nd)
    lta.add_parameter_relation(f <= t)
    # States
    r = lta.add_layer("R")
    p = lta.add_layer("P")
    lta.set_cycle(r)
    r_correct = [] # r_correct[i] is the state of a correct process with est = i
    r_faulty = [] # same but with processes allowed to crash
    p_correct = [] # same with odd layers
    p_faulty = []
    r_decide = [] # r_decide[i] is the state of a process has decided value i
    for i in range(m):
        r_correct.append(lta.add_state(r, "c{}".format(i)))
        r_faulty.append(lta.add_state(r, "f{}".format(i)))
        p_correct.append(lta.add_state(p, "c{}".format(i)))
        p_faulty.append(lta.add_state(p, "f{}".format(i)))
        r_decide.append(lta.add_state(r, "d{}".format(i)))
    p_correct_bot = lta.add_state(p, "c_") # state of a correct process with est = bottom
    p_faulty_bot = lta.add_state(p, "f_") # state of a faulty process with est = bottom
    # Guards
    for i in range(m):
        for j in range(m):
            e_correct_ij = lta.get_edge(r_correct[i], p_correct[j])
            e_faulty_ij = lta.get_edge(r_faulty[i], p_faulty[j])
            lta.set_guard(e_correct_ij, r_correct[j] + r_faulty[j] > nd)
            lta.set_guard(e_faulty_ij, r_correct[j] + r_faulty[j] > nd)
        # In order for processes to go to p_correct_bot or p_faulty_bot,
        # They need to receive more than k*nd messages without any
        # one message type having a bigger count than nd
        # This is possible if min(r0, nd) + ... + min(r{m-1}, nd) > k*nd
        # The following code translates this sum into 2^m linear inequalities
        conditions = []
        minima = [(r_correct[i] + r_faulty[i], nd) for i in range(m)]
        for l in itertools.product(*minima):
            conditions.append(sum(l) > k*nd)
        e_correct_ibot = lta.get_edge(r_correct[i], p_correct_bot)
        e_faulty_ibot = lta.get_edge(r_faulty[i], p_faulty_bot)
        lta.set_guard(e_correct_ibot, pylta.api.conjunction(*conditions))
        lta.set_guard(e_faulty_ibot, pylta.api.conjunction(*conditions))
    enough_received = (2*(sum(p_correct) + sum(p_faulty) + p_correct_bot + p_faulty_bot) > n)
    decision_cond = (2*(sum(p_correct) + sum(p_faulty)) > n)
    random_choice_cond = (2*(p_correct_bot + p_faulty_bot) > n)
    for j in range(m):
        for i in range(m):
            # Decision
            e_correct_di = lta.get_edge(p_correct[j], r_decide[i])
            e_faulty_di = lta.get_edge(p_faulty[j], r_decide[i])
            lta.set_guard(e_correct_di,
                          decision_cond
                          & (p_correct[i] + p_faulty[i] > 0))
            lta.set_guard(e_faulty_di,
                          decision_cond 
                          & (p_correct[i] + p_faulty[i] > 0))
            # set est
            e_correct_ji = lta.get_edge(p_correct[j], r_correct[i])
            e_faulty_ji = lta.get_edge(p_faulty[j], r_faulty[i])
            lta.set_guard(e_correct_ji,
                          enough_received
                          & (p_correct[i] + p_faulty[i] > 0)
                          & (p_correct_bot + p_faulty_bot > 0))
            lta.set_guard(e_faulty_ji,
                          enough_received
                          & (p_correct[i] + p_faulty[i] > 0)
                          & (p_correct_bot + p_faulty_bot > 0))
            # We do not put random choice here considering a process always receive at least its own value
    for i in range(m):
        # Similarly, a process in r_bot can't decide if it receives its own value
        e_correct_bot_i = lta.get_edge(p_correct_bot, r_correct[i])
        e_faulty_bot_i = lta.get_edge(p_faulty_bot, r_faulty[i])
        lta.set_guard(e_correct_bot_i,
                      (enough_received & (p_correct[i] + p_faulty[i] > 0))
                      | random_choice_cond)
        lta.set_guard(e_faulty_bot_i,
                      (enough_received & (p_correct[i] + p_faulty[i] > 0))
                      | random_choice_cond)

    checker = pylta.api.checker(lta)
    if goal == "agreement":
        #Agreement
        r_ini = checker.add_predicate(
                r,
                (sum(r_correct) == n - f) & (sum(r_faulty) == f) & (sum(r_decide) == 0),
            "ini"
        )
        r_decided = []
        for i in range(m):
            r_decided.append(
                checker.add_predicate(r, r_decide[i] > 0, "decided{}".format(i))
            )
        conflicts = []
        for decisions in itertools.combinations(r_decided, k+1):
            conflicts.append(
                pylta.api.conjunction(*(pylta.api.F(r & d) for d in decisions))
            )
        formula = pylta.api.implies(
            r_ini, ~pylta.api.disjunction(*conflicts)
        )
        checker.set_formula(formula)
    elif goal == "termination":
        # Termination under the assumption that in one round at most k values are proposed
        r_ini = checker.add_predicate(
            r,
            (sum(r_correct) == n - f) & (sum(r_faulty) == f) & (sum(r_decide) == 0),
            "ini"
        )
        r_fairness_assumptions = []
        for src in r_correct:
            leaving = lta.get_edge(src, p_correct_bot)
            for dest in p_correct:
                leaving += lta.get_edge(src, dest)
            r_fairness_assumptions.append(
                pylta.api.implies(
                    sum(r_correct) > nd, #Without messages from faulty processes
                    src == leaving
                )
            )
        r_fair = checker.add_predicate(r, pylta.api.conjunction(*r_fairness_assumptions), "fair")
        p_fairness_assumptions = []
        for src in p_correct:
            leaving = 0
            for dest in r_decide:
                leaving += lta.get_edge(src, dest)
            for dest in r_correct:
                leaving += lta.get_edge(src, dest)
            p_fairness_assumptions.append(
                pylta.api.implies(
                    2*(sum(p_correct) + p_correct_bot) > n,
                    src == leaving
                )
            )
        leaving = 0
        for dest in r_correct:
            leaving += lta.get_edge(p_correct_bot, dest)
        p_fairness_assumptions.append(
            pylta.api.implies(
                2*(sum(p_correct) + p_correct_bot) > n,
                p_correct_bot == leaving
            )
        )
        p_fair = checker.add_predicate(p, pylta.api.conjunction(*p_fairness_assumptions), "fair")
        at_most_k_values = [] #imposes that at most k values are proposed
        for values in itertools.combinations(range(m), m - k):
            term = 0
            for v in values:
                term += r_correct[v] + r_faulty[v]
            at_most_k_values.append(term == 0)
        r_not_too_many = checker.add_predicate(r, pylta.api.disjunction(*at_most_k_values), "at_most_k")
        r_decided = checker.add_predicate(r, sum(r_decide) > 0, "decided")
        formula = pylta.api.implies(
            r_ini & pylta.api.G(~r | r_fair) & pylta.api.G(~p | p_fair) & pylta.api.F(r & r_not_too_many),
            pylta.api.F(r & r_decided)
        )
        checker.set_formula(formula)
    return str(checker)


for m in range(2, 5):
    for k in range(1, m):
        with open("generated_examples/k_set_agreement_{}_{}.txt".format(m, k), mode='w') as f:
            f.write(k_set_agreement(m, k, "agreement"))
        with open("generated_examples/k_set_termination_{}_{}.txt".format(m, k), mode='w') as f:
            f.write(k_set_agreement(m, k, "termination"))


    
