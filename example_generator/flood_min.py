import itertools

import pylta.api

def flood_min(m, k, goal):
    lta = pylta.api.lta()
    # Parameters
    n = lta.add_parameter("n")
    # Layers
    l = lta.add_layer("l")
    lta.set_cycle(l)
    # States
    s = []
    s_crashing = []
    crashed = lta.add_state(l, "crashed")
    lta.set_guard(lta.get_edge(crashed, crashed), pylta.api.true())
    for i in range(m):
        s.append(lta.add_state(l, str(i)))
        s_crashing.append(lta.add_state(l, "c{}".format(i)))
    for i in range(m):
        for j in range(i+1): # define the guard between i and j
            conditions = []
            # In order for ap rocess to  move from i to j, we must have:
            # No messages smaller than j were received
            for k in range(j):
                conditions.append(s[k] == 0)
            # One message labelled j was received
            conditions.append(s[j] + s_crashing[j] > 0)
            guard = pylta.api.conjunction(*conditions)
            edge = lta.get_edge(s[i], s[j])
            edge_crashing = lta.get_edge(s[i], s_crashing[j])
            lta.set_guard(edge, guard)
            lta.set_guard(edge_crashing, guard)
            edge_crashed = lta.get_edge(s_crashing[i], crashed)
            lta.set_guard(edge_crashed, pylta.api.true())
    checker = pylta.api.checker(lta)
    # Validity
    for i in range(m):
        if goal == "validity{}".format(i):
            # Ugly, but not biggest inneficiency
            p_full = checker.add_predicate(l, sum(s) + sum(s_crashing) + crashed == n, "full")
            no_i = checker.add_predicate(l, s[i] + s_crashing[i] == 0, "no_{}".format(i))
            formula = pylta.api.implies(p_full & no_i, pylta.api.G(no_i))
            checker.set_formula(formula)
    # Termination
    if goal == "termination":
        p_full = checker.add_predicate(l, sum(s) + sum(s_crashing) + crashed == n, "full")
        clean_round = checker.add_predicate(l, sum(s_crashing) < k, "clean")
        decisions = []
        for values in itertools.combinations(range(m), k):
            sum_vals = crashed
            for v in values:
                sum_vals += s[v] + s_crashing[v]
            decisions.append(
                checker.add_predicate(l, sum_vals == n, "d"+"".join([str(v) for v in values]))
            )
        formula = pylta.api.implies(
            pylta.api.G(p_full),
            pylta.api.G(
                pylta.api.implies(
                    clean_round,
                    pylta.api.X(pylta.api.disjunction(*decisions))
                )
            )
        )
        checker.set_formula(formula)
    return str(checker)

for m in range(2, 5):
    for k in range(1, m):
        with open("generated_examples/flood_min_validity_{}_{}.txt".format(m, k), mode='w') as f:
            f.write(flood_min(m, k, "validity{}".format(m-1))) #The last validity is the most intersesting
        with open("generated_examples/flood_min_termination_{}_{}.txt".format(m, k), mode='w') as f:
            f.write(flood_min(m, k, "termination"))


    

