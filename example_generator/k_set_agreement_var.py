"""
From 'Randomized k-set agreement in crash-prone and Byzantine
asynchronous systems' published in 2017 by Achour Mostefaoui, Hamouma
Mounen and Michel Raynal.

This variant adds transition between decision states to simplify the
agreement verification.  """

import itertools

import pylta.api

def k_set_agreement(m, k, goal):
    lta = pylta.api.lta()
    # Parameters
    n = lta.add_parameter("n")
    t = lta.add_parameter("t")
    lta.add_parameter_relation(2*t < n)
    nd = lta.add_parameter("nd") # represents |n/(k+1)|
    lta.add_parameter_relation((k+1)*nd <= n) #k*n is linear because k is an integer and not a variable
    lta.add_parameter_relation((k+1)*(nd+1) > n)
    lta.add_parameter_relation(t < n - k*nd)
    # States
    r = lta.add_layer("R")
    p = lta.add_layer("P")
    lta.set_cycle(r)
    r_correct = [] # r_correct[i] is the state of a correct process with est = i
    r_crashed = lta.add_state(r, "crashed")
    p_correct = [] # same with odd layers
    p_crashed = lta.add_state(p, "crashed")
    r_decide = [] # r_decide[i] is the state of a process has decided value i
    p_decide = [] # added for remembering which process decides
    for i in range(m):
        r_correct.append(lta.add_state(r, str(i)))
        p_correct.append(lta.add_state(p, str(i)))
        r_decide.append(lta.add_state(r, "d{}".format(i)))
        p_decide.append(lta.add_state(p, "d{}".format(i)))
    p_correct_bot = lta.add_state(p, "c_") # state of a correct process with est = bottom
    # Guards
    for i in range(m):
        for j in range(m):
            e_correct_ij = lta.get_edge(r_correct[i], p_correct[j])
            lta.set_guard(e_correct_ij, r_correct[j] > nd)
        # In order for processes to go to p_correct_bot or p_faulty_bot,
        # They need to receive more than k*nd messages without any
        # one message type having a bigger count than nd
        # This is possible if min(r0, nd) + ... + min(r{m-1}, nd) > k*nd
        # The following code translates this sum into 2^m linear inequalities
        conditions = []
        minima = [(r_correct[i], nd) for i in range(m)]
        for l in itertools.product(*minima):
            conditions.append(sum(l) > k*nd)
        e_correct_ibot = lta.get_edge(r_correct[i], p_correct_bot)
        lta.set_guard(e_correct_ibot, pylta.api.conjunction(*conditions))
        # guards between decisions and crash states
        e_decision = lta.get_edge(r_decide[i], p_decide[i])
        lta.set_guard(e_decision, pylta.api.true())
        e_i_crash = lta.get_edge(r_correct[i], p_crashed)
        lta.set_guard(e_i_crash, pylta.api.true())
    e_rcrash_pcrash = lta.get_edge(r_crashed, p_crashed)
    lta.set_guard(e_rcrash_pcrash, pylta.api.true())
    
    enough_received = (2*(sum(p_correct) + p_correct_bot) > n)
    decision_cond = (2*sum(p_correct) > n)
    random_choice_cond = (2*p_correct_bot > n)
    for j in range(m):
        for i in range(m):
            # Decision
            e_correct_di = lta.get_edge(p_correct[j], r_decide[i])
            lta.set_guard(e_correct_di,
                          decision_cond
                          & (p_correct[i] > 0))
            # set est
            e_correct_ji = lta.get_edge(p_correct[j], r_correct[i])
            lta.set_guard(e_correct_ji,
                          enough_received
                          & (p_correct[i] > 0)
                          & (p_correct_bot > 0))
            # We do not put random choice here considering a process always receive at least its own value
        # guards between decisions and crash states
        e_decision = lta.get_edge(p_decide[j], r_decide[j])
        lta.set_guard(e_decision, pylta.api.true())
        e_j_crash = lta.get_edge(p_correct[j], r_crashed)
        lta.set_guard(e_j_crash, pylta.api.true())
    for i in range(m):
        # Similarly, a process in r_bot can't decide if it receives its own value
        e_correct_bot_i = lta.get_edge(p_correct_bot, r_correct[i])
        lta.set_guard(e_correct_bot_i,
                      (enough_received & (p_correct[i] > 0))
                      | random_choice_cond)
    e_bot_crash = lta.get_edge(p_correct_bot, r_crashed)
    lta.set_guard(e_bot_crash, pylta.api.true())
    e_pcrash_rcrash = lta.get_edge(r_crashed, p_crashed)
    lta.set_guard(e_pcrash_rcrash, pylta.api.true())

    checker = pylta.api.checker(lta)
    if goal == "agreement":
        #Agreement
        r_ini = checker.add_predicate(
                r,
                (sum(r_correct) + r_crashed == n) & (sum(r_decide) == 0),
            "ini"
        )
        conflicts = []
        for values in itertools.combinations(range(m), k+1):
            conflicts.append( 
                pylta.api.conjunction(*[r_decide[i] > 0 for i in values])
            )
        r_unsafe = checker.add_predicate(r, pylta.api.disjunction(*conflicts), "unsafe")
        formula = pylta.api.implies(
            r_ini, ~ pylta.api.F(r & r_unsafe)
        )
        checker.set_formula(formula)
    elif goal == "termination":
        # Termination under the assumption that in one round at most k values are proposed
        r_ini = checker.add_predicate(
                r,
                (sum(r_correct) + r_crashed == n) & (sum(r_decide) == 0),
            "ini"
        )
        r_fairness_assumptions = []
        for src in r_correct:
            leaving = lta.get_edge(src, p_correct_bot) + lta.get_edge(src, p_crashed)
            for dest in p_correct:
                leaving += lta.get_edge(src, dest)
            r_fairness_assumptions.append(
                pylta.api.implies(
                    sum(r_correct) > nd,
                    src == leaving
                )
            )
        r_crash_crash = lta.get_edge(r_crashed, p_crashed)
        r_fairness_assumptions.append(r_crashed == r_crash_crash)
        r_fair = checker.add_predicate(r, pylta.api.conjunction(*r_fairness_assumptions), "fair")
        p_fairness_assumptions = []
        for src in p_correct:
            leaving = lta.get_edge(src, r_crashed)
            for dest in r_decide:
                leaving += lta.get_edge(src, dest)
            for dest in r_correct:
                leaving += lta.get_edge(src, dest)
            p_fairness_assumptions.append(
                pylta.api.implies(
                    2*(sum(p_correct) + p_correct_bot) > n,
                    src == leaving
                )
            )
        leaving = 0
        for dest in r_correct:
            leaving += lta.get_edge(p_correct_bot, dest)
        p_fairness_assumptions.append(
            pylta.api.implies(
                2*(sum(p_correct) + p_correct_bot) > n,
                p_correct_bot == leaving
            )
        )
        p_crash_crash = lta.get_edge(p_crashed, r_crashed)
        r_fairness_assumptions.append(p_crashed == p_crash_crash)
        p_fair = checker.add_predicate(p, pylta.api.conjunction(*p_fairness_assumptions), "fair")
        at_most_k_values = [] #imposes that at most k values are proposed
        for values in itertools.combinations(range(m), m - k):
            term = 0
            for v in values:
                term += r_correct[v]
            at_most_k_values.append(term == 0)
        r_at_most_k = checker.add_predicate(r, pylta.api.disjunction(*at_most_k_values), "at_most_k")
        r_not_too_many = checker.add_predicate(r, r_crashed <= t, "not_too_many")
        p_not_too_many = checker.add_predicate(p, p_crashed <= t, "not_too_many")
        r_decided = checker.add_predicate(r, sum(r_decide) > 0, "decided")
        formula = pylta.api.implies(
            r_ini
            & pylta.api.G(~r | r_fair)
            & pylta.api.G(~p | p_fair)
            & pylta.api.G(~r | r_not_too_many)
            & pylta.api.G(~p | p_not_too_many)
            & pylta.api.F(r & r_at_most_k),
            pylta.api.F(r & r_decided)
        )
        checker.set_formula(formula)
    return str(checker)


for m in range(2, 5):
    for k in range(1, m):
        with open("generated_examples/k_set_agreement_var_{}_{}.txt".format(m, k), mode='w') as f:
            f.write(k_set_agreement(m, k, "agreement"))
        with open("generated_examples/k_set_termination_var_{}_{}.txt".format(m, k), mode='w') as f:
            f.write(k_set_agreement(m, k, "termination"))


    
