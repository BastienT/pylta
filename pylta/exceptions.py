import pylta.variables

class PyltaError(Exception):
    pass

class SymbolRedefinition(PyltaError):
    def __init__(self, name, meta=None):
        self.name = name
        self.meta = meta

    def __str__(self):
        t = "The symbol {} cannot be redefined".format(self.name)
        if self.meta is None:
            return t
        pre = "line {}: ".format(self.meta.line)
        return pre + t

class SymbolUndefined(PyltaError):
    def __init__(self, name, meta=None):
        self.name = name
        self.meta = meta

    def __str__(self):
        t = "The symbol {} is not defined".format(self.name)
        if self.meta is None:
            return t
        pre = "line {}: ".format(self.meta.line)
        return pre + t

class TypeError_(PyltaError):
    def __init__(self, variable, *expected_type, meta=None):
        self.variable = variable
        self.expected_type = expected_type
        self.meta = meta

    def __str__(self):
        str_expected = " or ".join([str(t) for t in self.expected_type])
        t = "Symbol {} is of type {} but should be {}".format(
            self.variable,
            self.variable.type_,
            str_expected
        )
        if self.meta is None:
            return t
        pre = "line {}: ".format(self.meta.line)
        return pre + t

class LayeringError(PyltaError):
    def __init__(self, item, expected_layer, meta=None):
        self.item = item
        self.expected_layer = expected_layer
        self.meta=meta

    def __str__(self):
        t = "{} is in layer {} but layer {} was expected".format(
            self.item,
            self.item.layer,
            self.expected_layer
        )
        if self.meta is None:
            return t
        pre = "line {}: ".format(self.meta.line)
        return pre + t
            
class UnexpectedTemporalOperator(PyltaError):
    def __init__(self, formula, meta=None):
        self.formula = formula
        self.meta=meta

    def __str__(self):
        t = "Formula {} cannot contain temporal operator".format(
            self.formula
        )
        if self.meta is None:
            return t
        pre = "line {}: ".format(self.meta.line)
        return pre + t
    
def check_formula_layering(lta, formula, layer, meta=None):
    """Check that `formula` only contains variable in `layer`.

    Raise an appropriate exception otherwise.

    If `layer` is `None`, then `formula` may only contain parameter
    variables.

    :param lta: A :class:`pylta.lta.LTA` object.
    :param formula: A :class:`pylta.formulas.Formula` object.
    :param layer: A :class:`pylta.variables.Layer` of `lta`
    :param meta: metadata to be provided with eventual exceptions.

    :raises pylta.exceptions.UnexpectedTemporalOperator: If the
        formula contains temporal operator.
    :raises pylta.exceptions.LayeringError: If some variables do not
        belong to `layer`.
    :raises pylta.exceptions.TypeError_: If the formula is not build
        properly (should not happen if the formula is built from
        :class:`pylta.parser.Parser`.
    """
    if formula.is_temporal():
        raise pylta.exceptions.UnexpectedTemporalOperator(
            formula,
            meta=meta
        )
    for v in formula.get_variables():
        if not lta.is_variable(v):
            raise pylta.exceptions.SymbolUndefined(v, meta=meta)
        match v:
            case pylta.variables.Parameter(_):
                pass
            case pylta.variables.State(S_layer, _):
                if S_layer != layer:
                    raise pylta.exceptions.LayeringError(
                        v,
                        layer,
                        meta=meta
                    )
            case pylta.variables.Edge(src, _):
                if src.layer != layer:
                    raise pylta.exceptions.LayeringError(
                        v,
                        layer,
                        meta=meta
                    )
            case _:
                raise pylta.exceptions.TypeError_(
                    v,
                    pylta.variables.Types.TERM,
                    meta=meta
                )
