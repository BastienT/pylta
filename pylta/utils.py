from abc import ABC, abstractmethod
import collections.abc

class Lasso:
    """A type of list that starts at a given index and may end with a
    loop.

    The structure contains three indices, start which contains the
    first index of the list, end which is the first index that loops
    back and loop_start which give the beginning of the loop.

    This means in particular that l[end] = l[loop_start].

    This class can also be used simply as a list that does not start
    at 0 by setting loop_start to None.
    """
    def __init__(self, start):
        """Create an empty lasso.

        The first element that gets added will have index start.

        :param start: an integer
        """
        self._start = start
        self.content = []
        self.loop_start = None
        
    @property
    def start(self):
        """The index of the first element of the list
        """
        return self._start

    @property
    def end(self):
        """The index after end of the list when taking the loop only
        once.
        """
        return self._start + len(self.content)

    @property
    def length(self):
        """The length of the list when counting the loop only once.
        """
        return len(self.content)

    def has_loop(self):
        """Return True if the list has a loop """
        return self.loop_start is not None
        
    def __getitem__(self, index):
        """Return the element of the list at the given index.

        If the lasso has a loop, index can be greater or equal to
        self.end, it will return the appropriate element from the
        loop.

        :param index: an integer

        :returns: the element of the lasso at the given index

        :raises IndexError: if index < self.start or if index >=
            self.end and the lasso has no loop.
        """
        if index < self.start:
            # otherwise l[start-1] would return the last element
            # which I find weird in this case
            raise IndexError(index)
        if index < self.end:
            return self.content[index - self.start]
        if self.loop_start is not None:
            i = (index - self.loop_start) % (self.end - self.loop_start)
            return self.content[self.loop_start + i]
        raise IndexError(index)

    def __setitem__(self, index, value):
        """Set the element at the given index to the given value.

        Similar functionalities than __getitem__

        :param index: an integer
        :param value: any element

        :raises IndexError: if index < self.start or if index >=
            self.end and the lasso has no loop.
        """
        if index < self.start:
            # otherwise l[start-1] would modify the last element
            # which I find weird in this case
            raise KeyError(index)
        if index < self.end:
            self.content[index - self.start] = value
        if self.loop_start is not None:
            i = (index - self.loop_start) % (self.end - self.loop_start)
            self.content[self.loop_start + i] = value
        raise IndexError(index)

    def append(self, value):
        """Adds an element at index self.end.

        :param value: any element.
        """
        self.content.append(value)

    def iter_indices(self, unroll=0, reverse=False):
        """Iterate over the indices of the lasso

        Parameter unroll can be used to append additional indices at
        the end, similar to unrolling the loop of the lasso a bit.

        Parameter reverse allows the iteration to happen in reverse
        order.

        :param unroll: an integer
        :param reverse: a boolean

        :returns: a generator of indices of the list
        """
        
        if not reverse:
            yield from range(self.start, self.end + unroll)
        else:
            yield from range(self.end + unroll-1, self.start-1, -1)

    def iter_values(self, unroll=0, reverse=False):
        """Iterate over the elements of the lasso

        Parameter unroll can be used to iterate over additional
        elements from the loop, similar to unrolling the loop of the
        lasso a bit.

        Parameter reverse allows the iteration to happen in reverse
        order.

        :param unroll: an integer
        :param reverse: a boolean

        :returns: a generator of elements of the list
        """
        for i in self.iter_indices(unroll=unroll, reverse=reverse):
            yield self.__getitem__(i)

    def items(self, unroll=0, reverse=False):
        """Iterate over both indices and elements of the lasso

        semantically equivalent to zip(self.iter_indices(unroll,
        reverse), self.iter_values(unroll, reverse)).

        :param unroll: an integer
        :param reverse: a boolean

        :returns: a generator of pairs (index, element)
        """
        
        for i in self.iter_indices(unroll=unroll, reverse=reverse):
            yield (i, self.__getitem__(i))

    def get_slice(self, new_start, new_end):
        """Return a new loop-less lasso containing the indices of self
        between new_start (included) and new_end (excluded).
        
        :param new_start: an index
        :param new_end: an index
        """
        result = type(self)(new_start)
        for index in range(new_start, new_end):
            result.append(self[index])
        return result

    
class AssociativeArray:
    """An array of hashable values with fast __contains__ and index
    support.

    This class simply synchronises a list (associating values to
    indices) and a dictionary (associating indices to values).
    """
    def __init__(self, iterator=None):
        """Initialises an AssociativeArray

        If iterator is provided, then the array will be initialised
        with these values.  Otherwise it will be empty.

        :param iterator: an generator of hashable objects
        """
        
        self.content = []
        self.indices = {}
        if iterator is not None:
            for a in iterator:
                self.append(a)

    def copy(self):
        """Performs a shallow copy

        :returns: a shallow copy of self.
        """
        
        result = type(self)()
        result.content = self.content.copy()
        result.indices = self.indices.copy()
        return result
                
    def append(self, a):
        if a in self.indices:
            raise KeyError(a)
        index = len(self.content)
        self.indices[a] = index
        self.content.append(a)

    def __len__(self):
        return len(self.content)
        
    def __getitem__(self, index):
        return self.content[index]

    def __setitem__(self, index, value):
        if value in self.indices:
            raise KeyError(value)
        self.indices[value] = index
        self.content[index] = value

    def index(self, a):
        return self.indices[a]
    
    def pop(self):
        a = self.content.pop()
        self.indices.pop(a)
        return a

    def items(self):
        return enumerate(self.content)

    def __iter__(self):
        yield from self.content

def array_to_str(lines, separation=" "):
    """Format a two dimensional array of strings with proper
    alignments.

    :param lines: A list of list of strings

    :returns: a single formatted string
    """
    column = 0
    while True:
        max_len = -1
        for i in range(len(lines)):
            if len(lines[i]) > column:
                max_len = max(max_len, len(str(lines[i][column])))
        if max_len == -1:
            # All lines are shorter than column
            break
        for i in range(len(lines)):
            #We add padding so that all cells in a column have same width
            if len(lines[i]) > column:
                lines[i][column] += " " * (max_len - len(lines[i][column]))
        column += 1

    joined_lines = [separation.join(l) for l in lines]
    return "\n".join(joined_lines)
