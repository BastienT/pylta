from dataclasses import dataclass
import collections

import pylta.utils
import pylta.variables
import pylta.predicates
import pylta.formulas

class Valuation(collections.UserDict):
    """A custom mapping for handling valuation of predicates.
    """
    def __init__(self, layer, *args):
        """
        Create a valuation of the predicates of the input layer.
        """
        super().__init__(*args)
        self.layer = layer

    def copy(self):
        return Valuation(self.layer, self)
        
    def freeze(self):
        """
        Return a hashable FrozenValuation describing the Valuation.
        """
        true_pred = []
        false_pred = []
        for pred, val in self.items():
            if val:
                true_pred.append(pred)
            else:
                false_pred.append(pred)
        return FrozenValuation(self.layer, frozenset(true_pred), frozenset(false_pred))

    def formula(self):
        """Return a :class:`pylta.formulas.Formula` that describes the
        valuation.
        """
        literals = []
        for pred, value in self.items():
            var = pylta.formulas.NamedPredicate(pred)
            if value:
                literals.append(var)
            else:
                literals.append(pylta.formulas.Not(var))
        return pylta.formulas.And(*literals)
        
@dataclass(frozen=True)
class FrozenValuation(collections.abc.Mapping):
    """A hashable version of Valuation. """
    layer: pylta.variables.Layer
    true_pred: frozenset
    false_pred: frozenset

    def __getitem__(self, pred):
        if pred in self.true_pred:
            return True
        if pred in self.false_pred:
            return False
        raise KeyError(pred)

    def __iter__(self):
        yield from self.true_pred
        yield from self.false_pred
    
    def __len__(self):
        return len(self.true_pred) + len(self.false_pred)

    def unfreeze(self):
        """ Return a non frozen copy of the valuation. """
        return Valuation(self.layer, self)


class Path(pylta.utils.Lasso):
    """A class for describing (abstract) paths on an LTA."""
    def copy(self):
        res = Path(self.start)
        for v in self.iter_values():
            res.append(v.copy())
        res.loop_start = self.loop_start
        return res
    
    def iter_all(self, unroll=0, reverse=False):
        """Iter over the (index, predicates, values) tuples of the
        path.

        In case the path is a lasso, the unroll parameter can be used
        to iterate over a few more iteration of the loops.  For
        example, if start=3, end=7 and loop_start=5, the default
        unroll=0 would iterate over indices 3, 4, 5, 6.  If unroll =
        3, it will iterate over indices 3, 4, 5, 6, 7, 8, 9 where the
        valuations given at indices 7, 8 and 9 are the same as those
        at indices 5, 6 and 5 respectively.

        If reverse is True, then the indices (not the predicates
        within a given index) will be iterated over in reverse order.

        :param unroll: an integer
        :param reverse: a boolean

        :returns: a (integer, :class:`pylta.variables.Predicate`,
                  boolean) iterator.
        """
        for index, valuation in self.items(unroll, reverse):
            for pred, value in valuation.items():
                yield (index, pred, value)
                
    def formula(self, unroll=0):
        """Return a :class:`pylta.formulas.Formula` object describing
        the path.

        The unroll parameter is used in the same way as in iter_all.
        """
        f = pylta.formulas.FormulaConstant(True)
        for index, valuation in self.items(unroll=unroll, reverse=True):
            val_f = valuation.formula()
            f = pylta.formulas.And(val_f, pylta.formulas.Next(f))
        return f
                
    def __str__(self):
        predicates = {}
        for valuation in self.iter_values():
            if valuation.layer not in predicates:
                predicates[valuation.layer] = set()
            for pred in valuation:
                predicates[valuation.layer].add(pred)
        lines = []
        layer_lines = {}
        pred_lines = {}
        for index, valuation in self.items():
            layer = valuation.layer
            if layer not in layer_lines:
                layer_lines[layer] = len(lines)
                lines.append([str(layer)+":"])
                for pred in predicates[layer]:
                    pred_lines[pred] = len(lines)
                    lines.append([str(pred.name)+":"])
                lines.append([]) #add space
            lines[layer_lines[layer]].append(str(index))
            for pred in predicates[layer]:
                if pred not in valuation:
                    char = "_"
                elif valuation[pred]:
                    char = "T"
                else:
                    char = "F"
                lines[pred_lines[pred]].append(char)
        res = pylta.utils.array_to_str(lines)
        if self.has_loop:
            res += ("\nLoop: {}".format(self.loop_start))
        return res

class ConcreteValuation:
    """
    A class used for representing valuations of the variables of a
    layer of an LTA.
    """
    def __init__(self, layer):
        """Initialise an empty concrete valuation at the given layer.
        """
        self.layer = layer
        self.state_val = {}
        self.edge_val = {}

    def states(self):
        """
        Iterate over the :class:`pylta.variables.State` of the
        valuation.
        """
        yield from self.state_val.keys()
        
    def get_state_value(self, state):
        """
        Return the (integer) values associated with the input state in
        the valuation.
        """
        return self.state_val[state]

    def set_state_value(self, state, value):
        """Set the (integer) value associated with a given state.

        Raise a ValueError if the state is not in the same Layer as
        the valuation.
        """
        if state.layer != self.layer:
            raise ValueError #Not sure what error is most appropriate here
        self.state_val[state] = value

    def state_items(self):
        """
        Iterate over (state, value) tuples.
        """
        yield from self.state_val.items()

    def edges(self):
        """
        Iterate over the :class:`pylta.variables.Edge` of the
        valuation.
        """
        yield from self.edge_val.keys()
        
    def get_edge_value(self, edge):
        """
        Return the (integer) value associated with the input edge.
        """
        return self.edge_val[edge]

    def set_edge_value(self, edge, value):
        """Set the (integer) value associated with a given edge.

        Raise a ValueError if the edge is not in the same Layer as
        the valuation.
        """
        if edge.src.layer != self.layer:
            raise ValueError #Not sure what error is most appropriate here
        self.edge_val[edge] = value

    def edge_items(self):
        """
        Iterate over (edge, value) tuples.
        """
        yield from self.edge_val.items()
        
class ConcretePath(pylta.utils.Lasso):
    """A class for describing concrete paths of an LTA. """
    def __init__(self, start):
        """
        Initialises an ampty path starting at the input index.
        """
        super().__init__(start)
        self.param_val = {}

    def get_param_value(self, param):
        """
        Return the valuation of the input parameter in the path.
        """
        return self.param_val[param]

    def set_param_value(self, param, value):
        """
        Set the valuation of the input parameter in the path.
        """
        self.param_val[param] = value

    def param_items(self):
        """
        Iterate over the (parameter, values) tuples.
        """
        yield from self.param_val.items()
        
    def __str__(self):
        states = {}
        edges = {}
        for index, val in self.items():
            if val.layer not in states:
                states[val.layer] = set()
                edges[val.layer] = set()
            for state in val.states():
                states[val.layer].add(state)
                if state not in edges:
                    edges[state] = set()
            for edge in val.edges():
                edges[edge.src].add(edge)
            #TODO: might fail if val contains an edge but not its src

        layer_lines = {}
        state_lines = {}
        edge_lines = {}
        lines = []
        for index, val in self.items():
            layer = val.layer
            if layer not in layer_lines:
                layer_lines[layer] = len(lines)
                lines.append(["{}:".format(layer)])
                for s in states[layer]:
                    state_lines[s] = len(lines)
                    lines.append(["{}:".format(s)])
                    for edge in edges[s]:
                        edge_lines[edge] = len(lines)
                        lines.append([" -> {}:".format(edge.dest)])
                lines.append([])
            lines[layer_lines[layer]].append("l = {}".format(index))
            for s in states[layer]:
                val = self[index].get_state_value(s)
                lines[state_lines[s]].append("{}".format(val))
                for edge in edges[s]:
                    val = self[index].get_edge_value(edge)
                    lines[edge_lines[edge]].append(" -> {}".format(val))
        for line in lines:
            # Lazy way of adding a final "|" at the end of lines
            if line:
                line.append("")
        res = pylta.utils.array_to_str(lines, separation = " | ")
        param_vals = ["{}: {}".format(p, v) for p, v in self.param_items()]
        param_line = ", ".join(param_vals)
        res = param_line + "\n" + res
        if self.has_loop:
            res += ("\nLoop: {}".format(self.loop_start))
        return res
    
