import collections.abc

import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.exceptions

class Wrapper:
    def __init__(self, arg):
        if isinstance(arg, Wrapper):
            self.content = arg.content
        elif isinstance(arg, int):
            self.content = pylta.terms.TermConstant(arg)
        else:
            self.content = arg

    def _parameter(self):
        match self.content.type_:
            case pylta.variables.Types.PARAMETER:
                return self.content
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.STATE
                )

    def _layer(self):
        match self.content.type_:
            case pylta.variables.Types.LAYER:
                return self.content
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.LAYER
                )
            
    def _state(self):
        match self.content.type_:
            case pylta.variables.Types.STATE:
                return self.content
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.STATE
                )

    def _edge(self):
        match self.content.type_:
            case pylta.variables.Types.EDGE:
                return self.content
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.EDGE
                )
            
    def _term(self):
        match self.content.type_:
            case pylta.variables.Types.PARAMETER:
                return pylta.terms.TermParameter(self.content)
            case pylta.variables.Types.STATE:
                return pylta.terms.TermState(self.content)
            case pylta.variables.Types.EDGE:
                return pylta.terms.TermEdge(self.content)
            case pylta.variables.Types.TERM:
                return self.content
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.TERM
                )

    def _predicate(self):
        match self.content.type_:
            case pylta.variables.Types.PREDICATE:
                return self.content
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.PREDICATE
                )

    def _formula(self):
        match self.content.type_:
            case pylta.variables.Types.LAYER:
                pred = pylta.variables.Predicate.from_layer(self.content)
                return pylta.formulas.NamedPredicate(pred)
            case pylta.variables.Types.PREDICATE:
                return pylta.formulas.NamedPredicate(self.content)
            case pylta.variables.Types.FORMULA:
                return self.content
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.FORMULA
                )

    @property
    def src(self):
        match self.content.type_:
            case pylta.variables.EDGE:
                return Wrapper(self.content.src)
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.EDGE
                )

    @property
    def dest(self):
        match self.content.type_:
            case pylta.variables.EDGE:
                return Wrapper(self.content.dest)
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.EDGE
                )

    @property
    def layer(self):
        match self.content.type_:
            case pylta.variables.STATE:
                return Wrapper(self.content.layer)
            case pylta.variables.PREDICATE:
                return Wrapper(self.content.layer)
            case _:
                raise pylta.exceptions.TypeError_(
                    self.content,
                    pylta.variables.Types.STATE,
                    pylta.variables.Types.PREDICATE,
                )
            
    def __str__(self):
        return str(self.content)
            
    def __pos__(self):
        return Wrapper(self._term())
            
    def __add__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        sum_term = pylta.terms.Sum(left, right)
        return Wrapper(sum_term)

    def __radd__(self, other):
        left = Wrapper(other)._term()
        right = self._term()
        sum_term = pylta.terms.Sum(left, right)
        return Wrapper(sum_term)

    def __neg__(self):
        term = pylta.terms.Product(
            pylta.terms.TermConstant(-1),
            self._term()
        )
        return Wrapper(term)

    def __sub__(self, other):
        left = self._term()
        right = pylta.terms.Product(
            pylta.terms.TermConstant(-1),
            Wrapper(other)._term()
        )
        sum_term = pylta.terms.Sum(left, right)
        return Wrapper(sum_term)

    def __rsub__(self, other):
        left = Wrapper(other)._term()
        right = pylta.terms.Product(
            pylta.terms.TermConstant(-1),
            self._term()
        )
        sum_term = pylta.terms.Sum(left, right)
        return Wrapper(sum_term)

    def __mul__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        product = pylta.terms.Product(left, right)
        return Wrapper(product)

    def __rmul__(self, other):
        left = Wrapper(other)._term()
        right = self._term()
        product = pylta.terms.Product(left, right)
        return Wrapper(product)

    def term_next(self):
        term = pylta.terms.NextLayer(self._term())
        return Wrapper(term)
    
    def __lt__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        formula = pylta.formulas.LowerThan(left, right)
        return Wrapper(formula)

    def __le__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        formula = pylta.formulas.LowerEqual(left, right)
        return Wrapper(formula)

    def __eq__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        formula = pylta.formulas.Equal(left, right)
        return Wrapper(formula)

    def __ne__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        formula = pylta.formulas.NotEqual(left, right)
        return Wrapper(formula)

    def __gt__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        formula = pylta.formulas.GreaterThan(left, right)
        return Wrapper(formula)

    def __ge__(self, other):
        left = self._term()
        right = Wrapper(other)._term()
        formula = pylta.formulas.GreaterEqual(left, right)
        return Wrapper(formula)

    def __invert__(self):
        formula = pylta.formulas.Not(self._formula())
        return Wrapper(formula)
    
    def __and__(self, other):
        left = self._formula()
        right = Wrapper(other)._formula()
        formula = pylta.formulas.And(left, right)
        return Wrapper(formula)

    def __rand__(self, other):
        left = Wrapper(other)._formula()
        right = self._formula()
        formula = pylta.formulas.And(left, right)
        return Wrapper(formula)

    def __or__(self, other):
        left = self._formula()
        right = Wrapper(other)._formula()
        formula = pylta.formulas.Or(left, right)
        return Wrapper(formula)

    def __ror__(self, other):
        left = Wrapper(other)._formula()
        right = self._formula()
        formula = pylta.formulas.Or(left, right)
        return Wrapper(formula)    

    def implies(self, other):
        left = self._formula()
        right = Wrapper(other)._formula()
        formula = pylta.formulas.Implies(left, right)
        return Wrapper(formula)

    def iff(self, other):
        left = self._formula()
        right = Wrapper(other)._formula()
        formula = pylta.formulas.Equiv(left, right)
        return Wrapper(formula)

    def until(self, other):
        left = self._formula()
        right = Wrapper(other)._formula()
        formula = pylta.formulas.Until(left, right)
        return Wrapper(formula)

    def X(self):
        formula = pylta.formulas.Next(self._formula())
        return Wrapper(formula)

    def F(self):
        formula = pylta.formulas.Finally(self._formula())
        return Wrapper(formula)

    def G(self):
        formula = pylta.formulas.Globally(self._formula())
        return Wrapper(formula)

def add(*args):
    children = [Wrapper(x)._term() for x in args]
    term = pylta.terms.Sum(*children)
    return Wrapper(term)

def multiply(*args):
    children = [Wrapper(x)._term() for x in args]
    term = pylta.terms.Product(*children)
    return Wrapper(term)

def opposite(arg):
    return Wrapper(arg).__neg__()

def term_next(arg):
    return Wrapper(arg).term_next()

def iff(*args):
    children = [Wrapper(x)._formula() for x in args]
    formula = pylta.formulas.Iff(*children)
    return Wrapper(formula)

def implies(*args):
    children = [Wrapper(x)._formula() for x in args]
    formula = pylta.formulas.Implies(*children)
    return Wrapper(formula)

def U(*args):
    children = [Wrapper(x)._formula() for x in args]
    formula = pylta.formulas.Implies(*children)
    return Wrapper(formula)

def disjunction(*args):
    children = [Wrapper(x)._formula() for x in args]
    formula = pylta.formulas.Or(*children)
    return Wrapper(formula)

def conjunction(*args):
    children = [Wrapper(x)._formula() for x in args]
    formula = pylta.formulas.And(*children)
    return Wrapper(formula)

def negation(arg):
    return Wrapper(arg).__invert__()

def X(arg):
    return Wrapper(arg).X()

def F(arg):
    return Wrapper(arg).F()

def G(arg):
    return Wrapper(arg).G()

def true():
    return Wrapper(pylta.formulas.FormulaConstant(True))

def false():
    return Wrapper(pylta.formulas.FormulaConstant(False))

def lt(left, right):
    formula = pylta.formulas.LowerThan(
        Wrapper(left)._formula(),
        Wrapper(right)._formula()
    )
    return Wrapper(formula)

def le(left, right):
    formula = pylta.formulas.LowerEqual(
        Wrapper(left)._formula(),
        Wrapper(right)._formula()
    )
    return Wrapper(formula)

def eq(left, right):
    formula = pylta.formulas.Equal(
        Wrapper(left)._formula(),
        Wrapper(right)._formula()
    )
    return Wrapper(formula)

def ne(left, right):
    formula = pylta.formulas.NotEqual(
        Wrapper(left)._formula(),
        Wrapper(right)._formula()
    )
    return Wrapper(formula)

def ge(left, right):
    formula = pylta.formulas.GreaterEqual(
        Wrapper(left)._formula(),
        Wrapper(right)._formula()
    )
    return Wrapper(formula)

def gt(left, right):
    formula = pylta.formulas.GreaterThan(
        Wrapper(left)._formula(),
        Wrapper(right)._formula()
    )
    return Wrapper(formula)
