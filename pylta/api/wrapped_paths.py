import collections

class WrappedValuation(collections.abc.MutableMapping):
    def __init__(self, valuation):
        self.content = valuation

    @property
    def layer(self):
        return pylta.api.wrapper.Wrapper(self.content.layer)
        
    def copy(self):
        return WrappedValuation(self.content.copy())
    
    def __getitem__(self, pred):
        pred = pred._predicate()
        if pred.layer != self.layer:
            raise pylta.exceptions.LayeringError(
                pred, self.layer)
        return self.valuation[pred]

    def __setitem__(self, pred, value):
        pred = pred._predicate()
        if pred.layer != self.layer:
            raise pylta.exceptions.LayeringError(
                pred, self.layer)
        self.content[pred] = value

    def __delitem__(self, pred):
        pred = pred._predicate()
        if pred.layer != self.content.layer:
            raise pylta.exceptions.LayeringError(
                pred, self.layer)
        del self.content[pred]

    def __iter__(self):
        for pred in self.content:
            yield pylta.api.wrapper.Wrapper(pred)

    def items(self):
        for pred, val in self.content.items():
            yield (pylta.api.wrapper.Wrapper(pred), val)
        
    def __contains__(self, pred):
        pred = pred._predicate()
        return pred in self.content
        
    def __len__(self):
        return len(self.content)

def valuation(layer):
    layer = layer._layer()
    return WrappedValutaion(pylta.paths.Valuation(layer))
    
class WrappedPath:
    #methods of pylta.utils.Lasso
    def __init__(self, content):
        self.content = content

    @property
    def start(self):
        return self.content.start

    @property
    def end(self):
        return self.content.end

    @property
    def length(self):
        return self.content.length
    
    def has_loop(self):
        return self.content.has_loop()

    def set_loop(index):
        self.content.loop_start = index
    
    def __getitem__(self, index):
        return WrappedValuation(self.content.__getitem__(index))

    def append(self, valuation):
        self.content.append(valuation.content)

    def iter_indices(self, unroll=0, reverse=False):
        yield from self.content.iter_indices(unroll, reverse)

    def iter_values(self, unroll=0, reverse=False):
        for index in self.iter_indices(unroll, reverse):
            yield self.__getitem__(index)

    def items(self, unroll=0, reverse=False):
        for index in self.iter_indices(unroll, reverse):
            yield (index, self.__getitem__(index))

    def get_slice(self, new_start, new_end):
        return WrappedPath(self.content.get_slice(new_start, new_end))

    # methods of pylat.paths.Path
    def copy(self):
        return WrappedPath(self.content.copy())

    def iter_all(self, unroll=0, reverse=False):
        for index, valuation in self.items(unroll, reverse):
            for pred, value in valuation.items():
                yield (index, pred, value)

    def __str__(self):
        return self.content.__str__()

def path(start):
    return WrappedPath(pylta.paths.Path(start))
