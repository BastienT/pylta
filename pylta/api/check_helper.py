import collections.abc

import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.predicates
import pylta.exceptions
import pylta.lta
import pylta.guard_abstraction
import pylta.guard_automaton
import pylta.checker
import pylta.api.wrapper
import pylta.api.wrapped_paths


class WrappedChecker:
    def __init__(self, lta):
        self.checker = pylta.checker.Checker(lta)
        self.predicate_numbers = {}
        
    def get_predicates(self, layer):
        layer = layer._layer()
        for pred in self.checker.predicates.get(layer):
            yield pylta.api.wrapper.Wrapper(pred)
            
    def get_predicate_formula(self, pred):
        pred = pred._predicate()
        self._check_predicate(pred)
        return pylta.api.wrapper.Wrapper(self.checker.predicates[pred])

    def add_predicate(self, layer, formula, name=None):
        layer = layer._layer()
        if not self.checker.lta.is_layer(layer):
            raise pylta.exceptions.SymbolUndefined(layer)
        formula = formula._formula()
        pylta.exceptions.check_formula_layering(self.checker.lta, formula, layer)
        if name is None:
            number = self.predicate_numbers.get(layer, 0)
            pred = pylta.variables.Predicate(layer, "A{}".format(number))
            self.predicate_numbers[layer] = number + 1
            while pred in self.checker.predicates:
                pred = pylta.variables.Predicate(layer, "A{}".format(self.predicate_numbers[layer]))
                self.predicate_numbers[layer] += 1
        else:
            pred = pylta.variables.Predicate(layer, name)
            if pred in self.checker.predicates:
                raise pylta.exceptions.SymbolRedefinition(pred)
        self.checker.add_predicate(pred, formula)
        return pylta.api.wrapper.Wrapper(pred)

    def _check_predicate(self, pred):
        if not self.checker.lta.is_layer(pred.layer):
            raise pylta.exceptions.SymbolUndefined(pred.layer)
        if not pred in self.checker.predicates:
            raise pylta.exceptions.SymbolUndefined(pred)

    def set_formula(self, ltl_formula):
        ltl_formula = ltl_formula._formula()
        for var in ltl_formula.get_variables():
            # This is the simplest way i have found to take an alement in a
            # set if it not empty
            raise pylta.exceptions.TypeError_(
                var, 
                pylta.variables.Types.PREDICATE,
            )
        for pred in ltl_formula.get_predicates():
            if pred.name and pred not in self.checker.predicates:
                raise pylta.exceptions.SymbolUndefined(
                    pred,
                )
        self.checker.set_formula(ltl_formula)
        
    def __str__(self):
        return str(self.checker)

    def verify(self):
        res = self.checker.cegar_solve()
        if res is None:
            return None
        elif res == True:
            return True
        else:
            return pylta.api.wrapped_paths.WrappedPath(res)

def checker(lta):
    return WrappedChecker(lta.lta)

