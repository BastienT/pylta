import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.exceptions
import pylta.lta
import pylta.api.wrapper

class WrappedLTA:
    def __init__(self):
        self.lta = pylta.lta.LTA()
        self.layer_number = 0
        self.param_number = 0
        self.state_numbers = {}

    def __str__(self):
        return str(self.lta)

    def layers(self):
        for layer in self.lta.get_layers():
            yield pylta.api.wrapper.Wrapper(layer)

    def add_layer(self, name=None):
        if name is None:
            layer = pylta.variables.Layer("L{}".format(self.layer_number))
            self.layer_number += 1
            while self.lta.is_layer(layer):
                layer = pylta.variables.Layer("L{}".format(self.layer_number))
                self.layer_number += 1
        else:
            layer = pylta.variables.Layer(name)
            if self.lta.is_layer(layer):
                raise pylta.exceptions.SymbolRedefinition(layer)
        self.lta.append_layer(layer)
        self.state_numbers[layer] = 0        
        return pylta.api.wrapper.Wrapper(layer)

    def get_layer(self, name):
        layer = pylta.variables.Layer(name)
        self._check_layer(layer)
        return pylta.api.wrapper.Wrapper(layer)

    def set_cycle(self, layer):
        layer = layer._layer()
        self._check_layer(layer)
        index = self.lta.get_layer_index(layer)
        self.lta.layers.set_loop(index)
    
    def first_layer(self):
        return pylta.api.wrapper.Wrapper(self.lta.get_layer_at(0))
    
    def next_layer(self, layer):
        return pylta.api.wrapper.Wrapper(self.lta.next_layer(layer._layer()))

    def _check_layer(self, layer):
        if not self.lta.is_layer(layer):
            raise pylta.exceptions.SymbolUndefined(layer)

    def parameters(self):
        for param in self.lta.get_parameters():
            yield pylta.api.wrapper.Wrapper(param)

    def add_parameter(self, name=None):
        if name is None:
            param = pylta.variables.Parameter("P{}".format(self.param_number))
            self.param_number += 1
            while self.lta.is_parameter(param):
                param = pylta.variables.Parameter("P{}".format(self.param_number))
                self.param_number += 1
        else:
            param = pylta.variables.Parameter(name)
            if self.lta.is_parameter(param):
                raise pylta.exceptions.SymbolRedefinition(param)
        self.lta.add_parameter(param)
        return pylta.api.wrapper.Wrapper(param)

    def get_parameter(self, name):
        param = pylta.variables.Parameter(name)
        self._check_parameter(param)
        return pylta.api.wrapper.Wrapper(param)

    def add_parameter_relation(self, formula):
        formula = formula._formula()
        pylta.exceptions.check_formula_layering(self.lta, formula, None)
        for pred in formula.get_predicates():
            raise 
        self.lta.add_parameter_relation(formula)

    def get_parameter_relations(self):
        for relation in self.lta.get_parameter_relations():
            yield pylta.api.wrapper.Wrapper(relation)
        
    def _check_parameter(self, param):
        if not self.lta.is_parameter(param):
            raise pylta.exceptions.SymbolUndefined(param)
    
    def states(self, layer):
        unwrapped = layer._layer()
        for state in self.lta.get_states(unwrapped):
            yield pylta.api.wrapper.Wrapper(state)

    def add_state(self, layer, name=None):
        layer = layer._layer()
        if name is None:
            state = pylta.variables.State(layer, "S{}".format(self.state_numbers[layer]))
            self.state_numbers[layer] += 1
            while self.lta.is_state(state):
                state = pylta.variables.State(layer, "S{}".format(self.state_numbers[layer]))
                self.state_numbers[layer] += 1
        else:
            state = pylta.variables.State(layer, name)
            if self.lta.is_state(state):
                raise pylta.exceptions.SymbolRedefinition(state)
        self.lta.add_state(state)
        return pylta.api.wrapper.Wrapper(state)
            
    def get_state(self, layer, name):
        layer = layer._layer()
        state = pylta.variables.State(layer, name)
        self._check_state(state)
        return pylta.api.wrapper.Wrapper(state)

    def _check_state(self, state):
        self._check_layer(state.layer)
        if not self.lta.is_state(state):
            raise pylta.exceptions.SymbolUndefined(state)

    def get_edge(self, src, dest):
        src = src._state()
        dest = dest._state()
        edge = pylta.variables.Edge(src, dest)
        self._check_edge(edge)
        return pylta.api.wrapper.Wrapper(edge)
            
    def out_edges(self, src):
        src = src._state()
        for dest in self.lta.get_successors(src):
            edge = pylta.variables.Edge(src, dest)
            yield pylta.api.wrapper.Wrapper(edge)

    def _check_edge(self, edge):
        self._check_state(edge.src)
        self._check_state(edge.dest)
        dest_layer = self.lta.next_layer(edge.src.layer)
        if dest_layer != edge.dest.layer:
            raise pylta.exceptions.LayeringError(
                edge.dest, dest_layer)
        
    def set_guard(self, edge, formula):
        edge = edge._edge()
        self._check_edge(edge)
        formula = formula._formula()
        pylta.exceptions.check_formula_layering(self.lta, formula, edge.src.layer)
        self.lta.set_guard(edge.src, edge.dest, formula)

    def get_guard(self, edge):
        edge = edge._edge()
        return pylta.api.wrapper.Wrapper(self.lta.get_guard(edge.src, edge.dest))

def lta():
    return WrappedLTA()
