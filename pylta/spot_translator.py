import spot

import pylta.variables
import pylta.formulas

class TemporalToSpot(pylta.formulas.FormulaVisitor):
    """
    This visitor is used by the to_spot function.
    """
    def translate(self, formula):
        return formula.visit(self)
    
    def on_formula(self, formula):
        raise NotImplementedError

    def on_formula_constant(self, formula):
        if formula.value:
            return spot.formula.tt()
        return spot.formula.ff()
    
    def on_iff(self, formula):
        visited_children = [child.visit(self) for child in formula.get_children()]
        left = visited_children[0]
        for right in visited_children[1:]:
            left = spot.formula.Equiv(left, right)
        return left

    def on_implies(self, formula):
        visited_children = [
            child.visit(self) for child in formula.get_children()
        ]
        #  a -> b -> c means a -> (b -> c)
        right = visited_children[-1]
        for left in visited_children[-2::-1]:
            # This loop from the next to last element to the first in reverse order
            right = spot.formula.Implies(left, right)
        return right
    
    def on_until(self, formula):
        visited_children = [
            child.visit(self) for child in formula.get_children()
        ]
        # Our convention: a U b U c means a U (b U c)
        right = visited_children[-1]
        for left in visited_children[-2::-1]:
            # This loop from the next to last element to the first in reverse order
            right = spot.formula.U(left, right)
        return right
    
    def on_or(self, formula):
        visited_children = [
            child.visit(self) for child in formula.get_children()
        ]
        return spot.formula.Or(visited_children)
    
    def on_and(self, formula):
        visited_children = [
            child.visit(self) for child in formula.get_children()
        ]
        return spot.formula.And(visited_children)
    
    def on_not(self, formula):
        visited_child = formula.child.visit(self)
        return spot.formula.Not(visited_child)
    
    def on_next(self, formula):
        visited_child = formula.child.visit(self)
        return spot.formula.X(visited_child)
    
    def on_finally(self, formula):
        visited_child = formula.child.visit(self)
        return spot.formula.F(visited_child)
    
    def on_globally(self, formula):
        visited_child = formula.child.visit(self)
        return spot.formula.G(visited_child)
    
    def on_formula_identifier(self, formula):
        raise NotImplementedError

    def on_named_predicate(self, formula):
        return spot.formula.ap(str(formula.predicate))

_TO_SPOT = TemporalToSpot()

def to_spot(formula):
    """Translate the input into a spot formula.

    Raise NotImplementedError if it encounters some uninterpreted
    terms or some arithmetic operations.

    :param formula: A :class:`pylta.formulas.Formula` object.

    :returns: A spot formula.
    """
    
    return formula.visit(_TO_SPOT)
