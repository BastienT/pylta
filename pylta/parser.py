import importlib.resources
import enum
import lark
import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.commands

@lark.visitors.v_args(meta=True)
class PyltaTransformer(lark.Transformer):
    """Transform the AST into a list of commands.
    """
    def start(self, meta, commands):
        return pylta.commands.Program(commands)

    def declare_parameters(self, meta, args):
        assert len(args) == 1
        parameter_names = args[0]
        parameters = [pylta.variables.Parameter(name) for name in parameter_names]
        command = pylta.commands.DeclareParameter(*parameters)
        command.meta = meta
        return command

    def declare_parameter_relation(self, meta, args):
        assert len(args) == 1
        relation = args[0]
        command = pylta.commands.DeclareParameterRelation(relation)
        command.meta = meta
        return command

    def declare_layers(self, meta, args):
        assert len(args) == 1
        layer_names = args[0]
        layers = [pylta.variables.Layer(name) for name in layer_names]
        command = pylta.commands.DeclareLayers(*layers)
        command.meta = meta
        return command

    def declare_states(self, meta, args):
        assert len(args) == 1
        states = args[0]
        command = pylta.commands.DeclareStates(*states)
        command.meta = meta
        return command

    def rule(self, meta, args):
        assert len(args) >= 1
        src = args[0]
        successors = {dest: guard for guard, dest in args[1:]}
        command = pylta.commands.Rule(src, successors)
        command.meta = meta
        return command

    def bind_term(self, meta, args):
        assert len(args) == 2
        name = args[0]
        term = args[1]
        command = pylta.commands.BindTerm(name, term)
        command.meta = meta
        return command
    
    def bind_formula(self, meta, args):
        assert len(args) == 2
        name = args[0]
        formula = args[1]
        command = pylta.commands.BindFormula(name, formula)
        command.meta = meta
        return command

    def verify(self, meta, args):
        assert len(args) > 0
        formula = args.pop()
        pred_defs = {pred: form for pred, form in args}
        command = pylta.commands.Verify(formula, pred_defs)
        command.meta = meta
        return command
        
    
    def case(self, meta, args):
        assert len(args) == 2
        guard = args[0]
        dest = args[1]
        return (guard, dest)

    def predicate_def(self, meta, args):
        assert len(args) == 2
        predicate = args[0]
        formula = args[1]
        return (predicate, formula)


    def formula(self, meta, args):
        assert len(args) >= 2
        formula = pylta.formulas.Iff(*args)
        formula.meta = meta
        return formula

    def formula_implies(self, meta, args):
        assert len(args) >= 2
        formula = pylta.formulas.Implies(*args)
        formula.meta = meta
        return formula

    def formula_until(self, meta, args):
        assert len(args) >= 2
        formula = pylta.formulas.Until(*args)
        formula.meta = meta
        return formula
        
    def formula_or(self, meta, args):
        assert len(args) >= 2
        formula = pylta.formulas.Or(*args)
        formula.meta = meta
        return formula
        
    def formula_and(self, meta, args):
        assert len(args) >= 2
        formula = pylta.formulas.And(*args)
        formula.meta = meta
        return formula
    
    def formula_not(self, meta, args):
        assert len(args) == 1
        child = args[0]
        formula = pylta.formulas.Not(child)
        formula.meta = meta
        return formula
    
    def formula_next(self, meta, args):
        assert len(args) == 1
        child = args[0]
        formula = pylta.formulas.Next(child)
        formula.meta = meta
        return formula
    
    def formula_finally(self, meta, args):
        assert len(args) == 1
        child = args[0]
        formula = pylta.formulas.Finally(child)
        formula.meta = meta
        return formula

    def formula_globally(self, meta, args):
        assert len(args) == 1
        child = args[0]
        formula = pylta.formulas.Globally(child)
        formula.meta = meta
        return formula

    def lower_than(self, meta, args):
        assert len(args) == 2
        left_term = args[0]
        right_term = args[1]
        formula = pylta.formulas.LowerThan(left_term, right_term)
        formula.meta = meta
        return formula

    def lower_equal(self, meta, args):
        assert len(args) == 2
        left_term = args[0]
        right_term = args[1]
        formula = pylta.formulas.LowerEqual(left_term, right_term)
        formula.meta = meta
        return formula

    def equal(self, meta, args):
        assert len(args) == 2
        left_term = args[0]
        right_term = args[1]
        formula = pylta.formulas.Equal(left_term, right_term)
        formula.meta = meta
        return formula

    def not_equal(self, meta, args):
        assert len(args) == 2
        left_term = args[0]
        right_term = args[1]
        return pylta.formulas.NotEqual(left_term, right_term)

    def greater_equal(self, meta, args):
        assert len(args) == 2
        left_term = args[0]
        right_term = args[1]
        formula = pylta.formulas.GreaterEqual(left_term, right_term)
        formula.meta = meta
        return formula

    def greater_than(self, meta, args):
        assert len(args) == 2
        left_term = args[0]
        right_term = args[1]
        formula = pylta.formulas.GreaterThan(left_term, right_term)
        formula.meta = meta
        return formula

    def named_predicate(self, meta, args):
        assert len(args) == 1
        predicate = args[0]
        formula = pylta.formulas.NamedPredicate(predicate)
        formula.meta = meta
        return formula

    def formula_true(self, meta, args):
        assert not args
        formula = pylta.formulas.FormulaConstant(value=True)
        formula.meta = meta
        return formula

    def formula_false(self, meta, args):
        assert not args
        formula = pylta.formulas.FormulaConstant(value=False)
        formula.meta = meta
        return formula

    def formula_identifier(self, meta, args):
        assert len(args) == 1
        ident = args[0]
        formula = pylta.formulas.FormulaIdentifier(ident)
        formula.meta = meta
        return formula

    def term(self, meta, args):
        assert len(args)%2 == 1
        result = args[0]
        children = [result]
        for i in range(1, len(args), 2):
            operator = args[i]
            next_term = args[i+1]
            if operator == "-":
                next_term = pylta.terms.Product(
                    pylta.terms.TermConstant(-1),
                    next_term
                )
            else:
                assert operator == "+"
            children.append(next_term)
        result = pylta.terms.Sum(*children)
        result.meta = meta
        return result
    
    def term_mult(self, meta, args):
        result = pylta.terms.Product(*args)
        result.meta = meta
        return result

    def term_negated(self, meta, args):
        assert len(args) >= 2
        assert args[0] == "-"
        child = args[1]
        result = pylta.terms.Product(
            pylta.terms.TermConstant(-1),
            child
        )
        result.meta = meta
        return result

    def term_edge(self, meta, args):
        assert len(args) == 1
        child = args[0]
        result = pylta.terms.TermEdge(child)
        result.meta = meta
        return result

    def term_state(self, meta, args):
        assert len(args) == 1
        child = args[0]
        result = pylta.terms.TermState(child)
        result.meta = meta
        return result

    def term_identifier(self, meta, args):
        assert len(args) == 1
        child = args[0]
        result = pylta.terms.TermIdentifier(child)
        result.meta = meta
        return result

    def term_constant(self, meta, args):
        assert len(args) == 1
        child = args[0]
        result = pylta.terms.TermConstant(int(child))
        result.meta = meta
        return result

    def predicate(self, meta, args):
        assert len(args) == 2
        layer_name = args[0]
        layer = pylta.variables.Layer(name=layer_name)
        predicate_name = args[1]
        return pylta.variables.Predicate(layer=layer, name=predicate_name)
        
    def edge(self, meta, args):
        assert len(args) == 2
        src = args[0]
        dest = args[1]
        return pylta.variables.Edge(src, dest)
    
    def list_of_states(self, meta, args):
        return args
    
    def state(self, meta, args):
        assert len(args) == 2
        layer_name = args[0]
        layer = pylta.variables.Layer(name=layer_name)
        state_name = args[1]
        return pylta.variables.State(layer=layer, name=state_name)
        
    def list_of_identifiers(self, meta, args):
        return args

    def identifier(self, meta, arg):
        assert len(arg) == 1
        return pylta.variables.Identifier(str(arg[0]))

class Parser:
    """The parser for pylta input files.
    """
    raw_parser = lark.Lark(
        importlib.resources.read_text(pylta, "grammar.lark"),
        start='start',
        propagate_positions=True
    )
    transformer = PyltaTransformer()
        
    def parse(self, input_str):
        """Generates a pylta program from the input string.

        A pylta program is a list of :class:`pylta.commands.Command`
        object

        :param input_str: a python string that follows the syntax of
            pylta.

        :returns: A :class:`pylta.commands.Program`.
        """
        
        ast = self.raw_parser.parse(input_str)
        return self.transformer.transform(ast)

_PARSER = Parser()

def parse(input_str):
    return _PARSER.parse(input_str)
