import re

import pysmt.shortcuts as smt
import pysmt.walkers
from pysmt.typing import INT as SMT_INT
from pysmt.typing import BOOL as SMT_BOOL

import pylta.variables
import pylta.terms
import pylta.formulas


class FormulaToSMT(pylta.terms.TermVisitor,
                   pylta.formulas.FormulaVisitor):
    """Used for converting pylta formulas to pysmt ones.
    """
    
    def on_formula(self, formula, index):
        # This case should not be called
        raise NotImplementedError

    def on_iff(self, formula, index):
        visited_children = [
            child.visit(self, index=index)
            for child in formula.children
        ]
        left = visited_children[0]
        for right in visited_children[1:]:
            left = smt.Iff(left, right)
        return left
    
    def on_implies(self, formula, index):
        visited_children = [
            child.visit(self, index=index)
            for child in formula.children
        ]
        #Implies is right associative
        right = visited_children[-1]
        for left in visited_children[-2::-1]:
            #This loop goes from the next to last element to the first
            #one (in reverse order)
            right = smt.Implies(left, right)
        return right
    
    def on_or(self, formula, index):
        visited_children = [
            child.visit(self, index=index)
            for child in formula.children
        ]
        return smt.Or(*visited_children)
    
    def on_and(self, formula, index):
        visited_children = [
            child.visit(self, index=index)
            for child in formula.children
        ]
        return smt.And(*visited_children)
    
    def on_not(self, formula, index):
        visited_child = formula.child.visit(self, index=index)
        return smt.Not(visited_child)

    def on_formula_constant(self, formula, index):
        if formula.value:
            return smt.TRUE()
        return smt.FALSE()

    def on_next(self, formula, index):
        #The only temporal operator that can be encoded in a finite formula
        return formula.child.visit(self, index=index+1)

    def on_lower_than(self, formula, index):
        visited_left = formula.left_term.visit(self, index=index)
        visited_right = formula.right_term.visit(self, index=index)
        return smt.LT(visited_left, visited_right)
    
    def on_lower_equal(self, formula, index):
        visited_left = formula.left_term.visit(self, index=index)
        visited_right = formula.right_term.visit(self, index=index)
        return smt.LE(visited_left, visited_right)
    
    def on_equal(self, formula, index):
        visited_left = formula.left_term.visit(self, index=index)
        visited_right = formula.right_term.visit(self, index=index)
        return smt.Equals(visited_left, visited_right)
    
    def on_not_equal(self, formula, index):
        visited_left = formula.left_term.visit(self, index=index)
        visited_right = formula.right_term.visit(self, index=index)
        return smt.NotEquals(visited_left, visited_right)
    
    def on_greater_equal(self, formula, index):
        visited_left = formula.left_term.visit(self, index=index)
        visited_right = formula.right_term.visit(self, index=index)
        return smt.GE(visited_left, visited_right)

    def on_greater_than(self, formula, index):
        visited_left = formula.left_term.visit(self, index=index)
        visited_right = formula.right_term.visit(self, index=index)
        return smt.GT(visited_left, visited_right)

    def on_named_predicate(self, formula, index):
        var_name = "AP{}({})".format(index, formula.predicate)
        return smt.Symbol(var_name, SMT_BOOL)

    def on_global_var(self, formula, index):
        var_name = "BOOL({})".format(formula.name)
        return smt.Symbol(var_name, SMT_BOOL)
    
    def on_term(self, term, index):
        # This case should not be called
        raise NotImplementedError

    def on_sum(self, term, index):
        visited_children = [child.visit(self, index=index) for child in term.children]
        if visited_children:
            # pysmt cannot create a sum of zero term
            return smt.Plus(*visited_children)
        return smt.Int(0)
    
    def on_product(self, term, index):
        visited_children = [child.visit(self, index=index) for child in term.children]
        if visited_children:
            return smt.Times(*visited_children)
        return smt.Int(1)

    def on_next_layer(self, term, index):
        """Raise index by one for every subterms.

        :param term: a pylta.terms.NextLayer object
        :param index: a non-negative integer

        :returns: a pysmt term hhere the non-parameter variables are
                  labelled with index+1
        """
        return term.child.visit(self, index=index + term.offset)

    def on_term_constant(self, term, index):
        value = term.value
        return smt.Int(value)

    def on_term_variable(self, term, index):
        var = term.content
        var_name = var.indexed_name(index)
        return smt.Symbol(var_name, SMT_INT)

        
class SMTToFormula(pysmt.walkers.DagWalker):
    """
    Used for converting pysmt formulas into pylta ones.
    """

    def _get_key(self, formula, expected_index):
        return (formula, expected_index)
    
    def walk_iff(self, formula, args, expected_index):
        children = []
        for child in args:
            if isinstance(child, pylta.formulas.Iff):
                children += child.children
            else:
                children.append(child)
        return pylta.formulas.Iff(*children)
    
    def walk_implies(self, formula, args, expected_index):
        left, right = args
        if isinstance(right, pylta.formulas.Implies):
            children = [left] + right.children
        else:
            children = [left, right]
        return pylta.formulas.Implies(*children)
    
    def walk_or(self, formula, args, expected_index):
        children = []
        for child in args:
            if isinstance(child, pylta.formulas.Or):
                children += child.children
            else:
                children.append(child)
        return pylta.formulas.Or(*children)
    
    def walk_and(self, formula, args, expected_index):
        children = []
        for child in args:
            if isinstance(child, pylta.formulas.And):
                children += child.children
            else:
                children.append(child)
        return pylta.formulas.And(*children)
    
    def walk_not(self, formula, args, expected_index):
        return pylta.formulas.Not(args[0])

    def walk_bool_constant(self, formula, args, expected_index):
        if formula.constant_value():
            return pylta.formulas.FormulaConstant(True)
        return pylta.formulas.FormulaConstant(False)
    
    def walk_equals(self, formula, args, expected_index):
        left, right = args
        return pylta.formulas.Equal(left, right)
    
    def walk_le(self, formula, args, expected_index):
        left, right = args
        return pylta.formulas.LowerEqual(left, right)
    
    def walk_lt(self, formula, args, expected_index):
        left, right = args
        return pylta.formulas.LowerThan(left, right)
    
    def walk_plus(self, formula, args, expected_index):
        children = []
        for child in args:
            if isinstance(child, pylta.terms.Sum):
                children += child.children
            else:
                children.append(child)
        return pylta.terms.Sum(*children)
    
    def walk_minus(self, formula, args, expected_index):
        left, right = args
        if isinstance(left, pylta.terms.Sum):
            children = left.children
        else:
            children = [left]
        children.append(
            pylta.terms.Product(
                pylta.terms.TermConstant(-1),
                right
            )
        )
        return pylta.terms.Sum(*children)
    
    def walk_times(self, formula, args, expected_index):
        children = []
        for child in args:
            if isinstance(child, pylta.terms.Product):
                children += child.children
            else:
                children.append(child)
        return pylta.terms.Product(*children)
    
    def walk_int_constant(self, formula, args, expected_index):
        assert formula.is_int_constant()
        value = formula.constant_value()
        return pylta.terms.TermConstant(value)
    
    def walk_symbol(self, formula, args, expected_index):
        name = formula.symbol_name()
        var, index = pylta.variables.from_indexed_name(name)
        match var.type_:
            case pylta.variables.Types.PARAMETER:
                assert index is None
                return pylta.terms.TermParameter(var)
            case pylta.variables.Types.STATE:
                term = pylta.terms.TermState(var)
                if expected_index != index:
                    # The test is not strictly necessary, but make things prettier
                    term = pylta.terms.NextLayer(term, offset=expected_index-index)
                return term
            case pylta.variables.Types.EDGE:
                term = pylta.terms.TermEdge(var)
                if expected_index != index:
                    # The test is not strictly necessary, but make things prettier
                    term = pylta.terms.NextLayer(term, offset=expected_index-index)
                return term
            case pylta.variables.Types.PREDICATE:
                formula = pylta.formulas.NamedPredicate(var)
                for _ in range(expected_index, index):
                    formula = pylta.formulas.Next(formula)
                return formula
            case pylta.variables.Types.BOOLEAN_VAR:
                assert index is None
                formula = pylta.formulas.GlobalVar(var)
                return formula
            case _:
                raise TypeError

_TO_SMT = FormulaToSMT()
_FROM_SMT = SMTToFormula()

def to_smt(formula, index):
    """
    Convert a :class:`pylta.formulas.Formula` object into a pysmt one.

    The variables that belong to layer are instanciated at the given
    index.  The fomula operator Next and term operator NextLayer can
    modify this index in part of the formula in order to have a
    formula that spans several layers.
    """
    return formula.visit(_TO_SMT, index=index)

def from_smt(smt_formula, expected_index):
    """
    Convert a pysmt formula into a :class:`pylta.formulas.Formula`
    object.  If the index of the variables does not match
    expected_index, the appropriate NextLayer operators will be added
    to the result.
    """
    return _FROM_SMT.walk(smt_formula, expected_index=expected_index)
