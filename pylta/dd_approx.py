from dataclasses import dataclass

import pylta.translator

import repycudd
import pysmt.solvers.bdd
import pysmt.shortcuts as smt

import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.paths
import pylta.translator

DD_MANAGER = repycudd.DdManager()
SMT_TO_DD = pysmt.solvers.bdd.BddConverter(smt.get_env(), DD_MANAGER)

class DdApproximation:
    def __init__(self, predicates, start, end):
        self.predicates = predicates # Just a pointer, should be read only
        self.start = start
        self.end = end
        self.lower = DD_MANAGER.Zero()
        self.upper = DD_MANAGER.One()
        for pred in self.predicates:
            self.register_pred(pred)

    def register_pred(self, predicate):
        assert predicate in self.predicates
        f = pylta.formulas.NamedPredicate(predicate)
        for index in range(self.start, self.end):
            # Could be optimised, but probably not worth it
            if predicate.layer == self.predicates.layers[index]:
                self.invalidate_lower()
                smt_f = pylta.translator.to_smt(f, index)
                SMT_TO_DD.declare_variable(smt_f)

    def invalidate_lower(self):
        self.lower = DD_MANAGER.Zero()

    def add_path(self, path):
        f = path.formula()
        smt_f = pylta.translator.to_smt(f, self.start)
        dd_f = SMT_TO_DD.convert(smt_f)
        self.lower = DD_MANAGER.Or(self.lower, dd_f)

    def remove_path(self, path):
        f = pylta.formulas.Not(path.formula())
        smt_f = pylta.translator.to_smt(f, self.start)
        dd_f = SMT_TO_DD.convert(smt_f)
        self.upper = DD_MANAGER.And(self.upper, dd_f)

    def set_cube_path(self, cube, path):
        for index in path.iter_indices():
            layer = path[index].layer
            for pred in self.predicates.get(layer):
                f = pylta.formulas.NamedPredicate(pred)
                smt_f = pylta.translator.to_smt(f, index)
                if smt_f in SMT_TO_DD.var2node:
                    # Otherwise, it means the pred does not matter
                    dd_f = SMT_TO_DD.var2node[smt_f]
                    i = dd_f.NodeReadIndex()
                    if cube[i] == 0:
                        path[index][pred] = False
                    elif cube[i] == 1:
                        path[index][pred] = True
        
    def unknown_path(self, path):
        f = path.formula()
        smt_f = pylta.translator.to_smt(f, self.start)
        dd_f = SMT_TO_DD.convert(smt_f)
        unknown_dd = DD_MANAGER.And(self.upper, DD_MANAGER.Not(self.lower))
        dd = DD_MANAGER.And(unknown_dd, dd_f)
        if dd == DD_MANAGER.Zero():
            return False
        # This variable is required for LargestCube to work
        a = repycudd.IntArray(1)
        # Not entirely sure why these steps are required
        c = DD_MANAGER.LargestCube(dd, a)
        cube = next(repycudd.ForeachCubeIterator(DD_MANAGER, c))
        self.set_cube_path(cube, path)
        return True

    def all_extensions(self, path):
        unset_preds = []
        for index in path.iter_indices():
            layer = path[index].layer
            for pred in self.predicates.get(layer):
                if pred not in path[index]:
                    unset_preds.append((index, pred))
        if not unset_preds:
            yield path.copy()
            return
        n = len(unset_preds)
        value_preds = [0 for _ in range(n)]
        finished = False
        def next_value_preds():
            remainder = 1
            for i in range(n):
                new_value = (value_preds[i] + remainder) % 2
                remainder = (value_preds[i] + remainder) // 2
                value_preds[i] = new_value
            return remainder
        def apply_value_preds():
            p2 = path.copy()
            for i, (index, pred) in enumerate(unset_preds):
                p2[index][pred] = bool(value_preds[i])
            return p2
        while not finished:
            yield apply_value_preds()
            finished = bool(next_value_preds())
                    
    def enum_lower(self, path):
        f = path.formula()
        smt_f = pylta.translator.to_smt(f, self.start)
        dd_f = SMT_TO_DD.convert(smt_f)
        dd = DD_MANAGER.And(self.lower, dd_f)
        for cube in repycudd.ForeachCubeIterator(DD_MANAGER, dd):
            res = self.predicates.abstract_path(self.start, self.end)
            self.set_cube_path(cube, res)
            #We still need to set all the combinations for predicates
            yield from self.all_extensions(res)
