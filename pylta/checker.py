import spot
import buddy

import pylta.utils
import pylta.variables
import pylta.formulas
import pylta.guard_automaton
import pylta.spot_translator
    
class Product:
    def __init__(self, guard_aut, spot_aut):
        self.guard_aut = guard_aut
        self.spot_aut = spot_aut

        # Used to record the predicate of each ap number.
        self.ap_to_pred = {}

        for ap in self.spot_aut.ap():
            ap_num = self.spot_aut.get_dict().varnum(ap)
            if "." in ap.ap_name():
                layer_name, pred_name = ap.ap_name().split(".")
                layer = pylta.variables.Layer(layer_name)
                pred = pylta.variables.Predicate(layer, pred_name)
            else:
                layer = pylta.variables.Layer(ap.ap_name())
                pred = pylta.variables.Predicate.from_layer(layer)
            self.ap_to_pred[ap_num] = pred
            
    def get_sat_assignments(self, bdd_formula, assignment=None):
        """Return all partial assignments of predicates that satisfy
        `bdd_formula`.

        :param bdd_formula: A buddy bdd
        :param assignment: A partial assignment of predicate used for
            recursivity.

        :returns: A dictionary assigning pylta.variables.Predicate of
                  the lta with booleans.
        """
        assignment = assignment or {}
        if bdd_formula == buddy.bddtrue:
            yield assignment
        elif bdd_formula == buddy.bddfalse:
            pass
        else:
            pred = self.ap_to_pred[buddy.bdd_var(bdd_formula)]
            for val in [False, True]:
                new_assignment = assignment.copy()
                if pred in self.guard_aut.predicates:
                    new_assignment[pred] = val
                if val:
                    new_bdd = buddy.bdd_high(bdd_formula)
                else:
                    new_bdd = buddy.bdd_low(bdd_formula)
                yield from self.get_sat_assignments(new_bdd, new_assignment)
                    
    def val_to_bdd(self, valuation):
        res = buddy.bddtrue
        for pred, value in valuation.items():
            ap = self.spot_aut.register_ap(str(pred))
            if value:
                res &= buddy.bdd_ithvar(ap)
            else:
                res &= buddy.bdd_nithvar(ap)
        return res

    def initial_state(self):
        return (None, self.spot_aut.get_init_state_number())

    def out_edges(self, pair):
        return self.spot_aut.out(pair[1])

    def apply_edge(self, pair, spot_edge):
        g_state = pair[0]
        for partial_val in self.get_sat_assignments(spot_edge.cond):
            if g_state is None:
                layer = self.guard_aut.ga.lta.get_layer_at(0)
                guard_dests = self.guard_aut.get_states(layer, partial_val)
            else:
                guard_dests = self.guard_aut.get_successors(g_state, partial_val)
            yield from [(g_dst, spot_edge.dst) for g_dst in guard_dests]
    
    def successors(self, pair):
        for edge in self.out_edges(pair):
            yield from self.apply_edge(pair, edge)

    def get_valuation(self, pair):
        res = {pred: value for pred, value in pair[0].items()}
        for layer in self.guard_aut.lta.get_layers():
            pred_layer = pylta.variables.Predicate.from_layer(layer)
            res[pred_layer] = (layer == pair[0].layer)
        return res

    def to_spot(self):
        bdict = self.spot_aut.get_dict()
        aut = spot.make_twa_graph(bdict)
        for layer in self.guard_aut.ga.lta.get_layers():
            for pred in self.guard_aut.predicates.get(layer):
                aut.register_ap(str(pred))
        aut.set_generalized_buchi(self.spot_aut.num_sets())
                
        pair0 = self.initial_state()
        s0 = aut.new_state()
        aut.set_init_state(s0)
        stack = [pair0]
        pair_to_state = {pair0: s0}
        while stack:
            pair = stack.pop()
            s = pair_to_state[pair]
            for spot_edge in self.out_edges(pair):
                for pair_dst in self.apply_edge(pair, spot_edge):
                    if pair_dst in pair_to_state:
                        s2 = pair_to_state[pair_dst]
                    else:
                        s2 = aut.new_state()
                        pair_to_state[pair_dst] = s2
                        stack.append(pair_dst)
                    aut.new_edge(
                        s,
                        s2,
                        self.val_to_bdd(
                            self.get_valuation(pair_dst)),
                        spot_edge.acc)
        return aut

class Checker:
    def __init__(self, lta):
        k = len(lta.get_layers())
        self.path_checker = pylta.guard_abstraction.PathInterpolator(lta, 0, k+1)
        self.guard_aut = pylta.guard_automaton.GuardAutomaton(lta, self.path_checker)
        self.ltl_formula = None #The formula can only be added after the predicates

    def add_predicate(self, pred, formula):
        self.guard_aut.add_predicate(pred, formula)

    @property
    def predicates(self):
        return self.guard_aut.predicates

    @property
    def lta(self):
        return self.guard_aut.lta

    def set_formula(self, ltl_formula):
        self.ltl_formula = ltl_formula

    def make_product(self):
        if self.ltl_formula is None:
            raise RuntimeError("Must set ltl formula before solving")
        spot_formula = pylta.spot_translator.to_spot(
            pylta.formulas.And(
                self.lta.temporal_constraints(),
                pylta.formulas.Not(self.ltl_formula)
            )
        )
        spot_aut = spot_formula.translate()
        return Product(self.guard_aut, spot_aut)
    
    def _counter_example(self, product):
        spot_product = product.to_spot()
        if spot_product.is_empty():
            return None
        else:
            product_path = pylta.utils.AssociativeArray()
            accepting_word = spot_product.accepting_word()
            spot_path = accepting_word.as_automaton()
            restriction = Product(self.guard_aut, spot_path)
            s = restriction.initial_state()
            while s not in product_path:
                product_path.append(s)
                s = list(restriction.successors(s))
                assert len(s) == 1, s
                s = s[0]
            loop_start = product_path.index(s)

            state_path = self.guard_aut.predicates.abstract_path(0, 0)
            for g_state, _ in product_path[1:]: #The first element is None
                state_path.append(g_state)
            state_path.loop_start = loop_start - 1 #first element confusion
            return state_path
    
    def counter_example(self):
        product = self.make_product()
        return self._counter_example(product)

    def process_interpolants(self, path, interpolants):
        r_interpolants = pylta.utils.Lasso(interpolants.start)
        for itp in interpolants.iter_values():
            r_interpolants.append(_remove_act_var(itp))
        useful_interpolants = self.guard_aut.ga.extract_useful_interpolants(
            path, r_interpolants)
        for index, itp in useful_interpolants.items():
            layer = self.lta.get_layer_at(index)
            pred = self.predicates.get_unused_pred(layer)
            self.add_predicate(pred, itp)
    
    def cegar_solve(self):
        product = self.make_product()
        while True:
            c_ex = self._counter_example(product)
            if c_ex is None:
                return True
            solver_res = self.path_checker.interpolate(c_ex)
            if solver_res is None:
                return None
            sat, res = solver_res
            if sat:
                return res
            else:
                self.process_interpolants(c_ex, res)

    def __str__(self):
        res = str(self.lta)
        res += "WITH\n"
        for pred, form in self.predicates.items():
            res += "  {}: {}\n".format(pred, form)
        res += "VERIFY: {}\n".format(self.ltl_formula)
        return res
        

class VerboseChecker(Checker):
    def add_predicate(self, pred, formula):
        print("Adding predicate {}: {}".format(pred, formula))
        Checker.add_predicate(self, pred, formula)

    def cegar_solve(self):
        product = self.make_product()
        while True:
            c_ex = self._counter_example(product)
            if c_ex is None:
                print("There is no abstract counter example")
                return True
            print("Found an abstract counter example:")
            print(c_ex)
            solver_res = self.path_checker.interpolate(c_ex)
            if solver_res is None:
                print("Prefix concretisation succeeded but loop failed")
                return None
            sat, res = solver_res
            if sat:
                print("The counter example was concretised")
                return res
            else:
                print("The counter example could not be concretised, starting interpolation")
                self.process_interpolants(c_ex, res)
              
                
class _ActVarRemover(pylta.formulas.FormulaVisitor):
    def on_formula(self, formula):
        return formula
    def on_sequence_operation(self, formula):
        visited_children = [child.visit(self) for child in formula.children]
        return type(formula)(*visited_children)
    def on_and(self, formula):
        visited_children = [child.visit(self) for child in formula.children]
        new_children = []
        for child in visited_children:
            if isinstance(child, pylta.formulas.FormulaConstant):
                if not child.value:
                    return pylta.formulas.FormulaConstant(False)
            else:
                new_children.append(child)
        if len(new_children) == 0:
            return pylta.formulas.FormulaConstant(True)
        if len(new_children) == 1:
            return new_children[0]
        return type(formula)(*new_children)

    def on_or(self, formula):
        visited_children = [child.visit(self) for child in formula.children]
        new_children = []
        for child in visited_children:
            if isinstance(child, pylta.formulas.FormulaConstant):
                if child.value:
                    return pylta.formulas.FormulaConstant(True)
            else:
                new_children.append(child)
        if len(new_children) == 0:
            return pylta.formulas.FormulaConstant(False)
        if len(new_children) == 1:
            return new_children[0]
        return type(formula)(*new_children)

    def on_unary_operation(self, formula):
        visited_child = formula.child.visit(self)
        return type(formula)(visited_child)

    def on_not(self, formula):
        visited_child = formula.child.visit(self)
        if isinstance(visited_child, pylta.formulas.Not):
            return visited_child.child
        if isinstance(visited_child, pylta.formulas.FormulaConstant):
            return pylta.formulas.FormulaConstant(not visited_child.value)
        return type(formula)(visited_child)

    def on_global_var(self, formula):
        return pylta.formulas.FormulaConstant(True)

_ACT_VAR_REMOVER = _ActVarRemover()
def _remove_act_var(formula):
    return formula.visit(_ACT_VAR_REMOVER)
