""" This scipt is a work in progress """

import sys
import time
from optparse import OptionParser

import pylta.parser
import pylta.interpreter

usage = "usage: python3 -m pylta [options] file..."
opt_parse = OptionParser(usage)
opt_parse.add_option("-v", "--verbose",
                     action="store_true", dest="verbose", default=False)

opt_parse.add_option("-t", "--time",
                     action="store_true", dest="time", default=False)

(options, args) = opt_parse.parse_args()

if len(args) < 1:
    opt_parse.error("must specify at least one input file")

text = ""
for file_name in args:
    with open(file_name, 'r') as f:
        text += f.read()

start_time = time.perf_counter()
program = pylta.parser.parse(text)
interpreter = pylta.interpreter.Interpreter(verbose=options.verbose)
parsing_time = time.perf_counter()
interpreter.run(program)
end_time = time.perf_counter()
if options.time:
    print("Parse Time       : {} seconds".format(parsing_time - start_time))
    print("Verification Time: {} seconds".format(end_time - parsing_time))
