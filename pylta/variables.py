"""
This module defines the basic objects that are used in pylta.terms,
pylta.formulas and pylta.lta.
"""

import enum
from dataclasses import dataclass

import lark

import pylta.utils

class Types(enum.Enum):
    """
    This enumeration is used to label the different classes of pylta.

    It can be used to generate error messages in case of type errors
    in pyLTA code.
    """
    IDENTIFIER = 0
    PARAMETER = 1
    LAYER = 2
    STATE = 3
    EDGE = 4
    TERM = 5
    BOOLEAN_VAR = 6
    PREDICATE = 7
    FORMULA = 8

class Identifier(str):
    """An alias for strings used to name variables.
    """
    type_ = Types.IDENTIFIER

@dataclass(frozen=True)
class Parameter:
    """Encode parameters of :class:`pylta.lta.LTA`.  Cannot be used
    directly in terms and formulas, look for
    :class:`pylta.terms.TermParameter` instead.
    """
    type_ = Types.PARAMETER  # Class variable
    name: Identifier
    
    def __str__(self):
        return self.name

    def indexed_name(self, index=None):
        """
        Generates a string that uniquely describes the parameter.

        This function is used to name variables.

        As the value of the parameters remain constant accros layers
        of an LTA, the argument index is ignored for parameters, but
        is used by eponymous functions for other variables.
        """
        return "P({})".format(self.name)

@dataclass(frozen=True)
class Layer:
    """Encodes the name of a layer in :class:`pylta.lta.LTA`.  Does
    not contain the states or edges of the automaton, these are
    handled by :class:`pylta.lta.LTA` directly.
    """
    type_ = Types.LAYER  # Class variable
    name: Identifier
    
    def __str__(self):
        return self.name

EMPTY_LAYER = Layer("")
    
class LayerList(pylta.utils.AssociativeArray):
    """Encodes a Lasso-shaped infinite list of layers.

    Use the constructor to create an empty list, then append to add
    layers in order.  Finally, set_loop(index) sets the loop index to
    the given value.  If set_loop is never called, a loop on a lyer
    with an empty name is present by default.
    """
    #We need both the functionalities of pylta.utils.AssociativeArray
    #and pylta.utils.Lasso here, but dual inheritance would not work
    #with the code I have. So some functionalities from Lasso are
    #reimplemented
    def __init__(self):
        """Create an empty LayerList.

        Use apppend(layer) to add other layers and set_loop to set the
        index of the start of the loop.
        """
        pylta.utils.AssociativeArray.__init__(self)
        self.loop_start = 0

    def set_loop(self, loop_start):
        """Set the begining of the loop at the input index. """
        self.loop_start = loop_start
    
    def __getitem__(self, index):
        if index < 0:
            raise KeyError(index)
        if index < self.loop_start:
            return super().__getitem__(index)
        i = (index - self.loop_start) % (len(self) - self.loop_start)
        return super().__getitem__(self.loop_start + i)

@dataclass(frozen=True)
class State:
    """Encode a state :class:`pylta.lta.LTA`.  Also indicates the
    :class:`pylta.variables.Layer` it belongs to.  Cannot be used
    directly in terms and formulas, look for
    :class:`pylta.terms.TermState` instead.
    """
    type_ = Types.STATE  # Class variable
    layer: Layer
    name: Identifier
    
    def __str__(self):
        return "{}.{}".format(self.layer, self.name)

    def indexed_name(self, index):
        """
        Generates a string that uniquely describes the state.

        Used for naming SMT variables.  The argument index is used to
        specify different states in case a formula spans several copy
        of a layer.
        """
        return "S{}({})".format(index, self)

@dataclass(frozen=True)
class Edge:
    """Encode an edge between two :class:`pylta.variables.State` of an
    :class:`pylta.lta.LTA`.  Cannot be used directly in terms and
    formulas, look for :class:`pylta.terms.TermEdge` instead.
    """
    type_ = Types.EDGE  # Class variable
    src: State
    dest: State
    
    def __str__(self):
        return "EDGE({}, {})".format(self.src, self.dest)

    def indexed_name(self, index):
        """
        Generates a string that uniquely describes the edge.

        Used for naming SMT variables.  The argument index is used to
        specify different edges in case a formula spans several copy
        of a layer.
        """
        return "E{}({}, {})".format(index, self.src, self.dest)

@dataclass(frozen=True)
class BooleanVar:
    """Encode a constant boolean variable.

    Similarly to Parameters, it ignores the index parameter in the
    indexed_name method.  In this way, BooleanVar differs from
    Predicates.
    """
    type_ = Types.BOOLEAN_VAR
    name: Identifier

    def __str__(self):
        return self.name

    def indexed_name(self, index=None):
        """
        Generates a string that uniquely describes the variable.

        This function is used to name variables in SMT formulas.

        The argument index is ignored for boolean variables.  If a
        copy of the variables are needed at different layers, one can
        use the Predicate class instead.
        """
        return "BOOL({})".format(self)

    
@dataclass(frozen=True)
class Predicate:
    """Encode a boolean predicate of :class:`pylta.lta.LTA`.  Only
    contains the name of the predicate and the
    :class:`pylta.variables.Layer` it belongs to.  The class
    :class:`pylta.lta.LTA` handles the formula it is associated to.
    Cannot be used directly in formulas, look for
    :class:`pylta.formulas.NamdePredicate` instead.
    """
    type_ = Types.PREDICATE
    layer: Layer
    name: Identifier

    def __str__(self):
        if self.name:
            return "{}.{}".format(self.layer, self.name)
        return str(self.layer)

    @classmethod
    def from_layer(cls, layer):
        """Return a predicate that only depends on the input layer.

        Such predicates are used to identify the layer in a state of a
        buchi automaton (from spot for example).

        :param layer: :class:`pylta.variables.Layer`

        :returns: A Predicate
        """
        return cls(layer, "")

    def indexed_name(self, index):
        """
        Generates a string that uniquely describes the predicate.

        Used for naming SMT variables.  The argument index is used to
        specify different edges in case a formula spans several copy
        of a layer.
        """
        return "AP{}({})".format(index, self)

#This parser is used by from_indexed_name to generates variables from the output of an indexed_name method.
_LARK_PARSER = lark.Lark(
    ("?start: param | state | edge | predicate | global_var\n"
     "param: \"P(\" ident \")\"\n"
     "state: \"S\" integer \"(\" ident \".\" tolerant_ident \")\"\n"
     "edge: \"E\" integer \"(\" ident \".\" tolerant_ident \", \" ident \".\" tolerant_ident \")\"\n"
     "predicate: \"AP\" integer \"(\" ident \".\" tolerant_ident \")\" | \"AP\" integer \"(\" ident \")\"\n"
     "global_var: \"BOOL(\" ident \")\"\n"
     "ident: CNAME\n"
     "integer: INT\n"
     "tolerant_ident: CNAME | INT\n"
     "%import common.INT\n"
     "%import common.CNAME\n"),
    start='start'
)

class VarTransformer(lark.Transformer):
    """
    Used by the from_indexed_name function
    """
    def param(self, args):
        assert len(args) == 1
        param = Parameter(args[0])
        return (param, None)

    def state(self, args):
        assert len(args) == 3
        index = args[0]
        layer = Layer(args[1])
        state = State(layer, args[2])
        return (state, index)

    def edge(self, args):
        assert len(args) == 5
        index = args[0]
        src_layer = Layer(args[1])
        src = State(src_layer, args[2])
        dest_layer = Layer(args[3])
        dest = State(dest_layer, args[4])
        edge = Edge(src, dest)
        return (edge, index)

    def predicate(self, args):
        assert len(args) == 2 or len(args) == 3
        index = args[0]
        layer = Layer(args[1])
        if len(args) == 3:
            pred = Predicate(layer, args[2])
        else:
            pred = Predicate.from_layer(layer)
        return (pred, index)

    def global_var(self, args):
        assert len(args) == 1
        var = BooleanVar(args[0])
        return (var, None)

    def ident(self, args):
        assert len(args) == 1
        return Identifier(args[0])

    def integer(self, args):
        assert len(args) == 1
        return int(args[0])

    def tolerant_ident(self, args):
        assert len(args) == 1
        return Identifier(args[0])

_VAR_TRANSFORMER = VarTransformer()

def from_indexed_name(indexed_name):
    """Generate the appropriate variable and index from an indexed
    name.

    Opposite of the indexed_name method, return a tuple with the
    variable and either the index or None if it is absent (Parameter
    or BooleanVar).
    """
    ast = _LARK_PARSER.parse(indexed_name)
    return _VAR_TRANSFORMER.transform(ast)
