import pylta.variables
import pylta.predicates
import pylta.terms
import pylta.formulas
import pylta.commands
import pylta.lta
import pylta.guard_abstraction
import pylta.guard_automaton
import pylta.spot_translator
import pylta.checker
import pylta.exceptions

import spot

# forbiden names TODO: implement properly

class Interpreter(pylta.commands.CommandVisitor,
                  pylta.formulas.FormulaVisitor,
                  pylta.terms.TermVisitor):
    """TODO: describe class
    """
    def __init__(self, verbose=False):
        self.lta = pylta.lta.LTA()
        self.symbol_table = {}
        self.verbose=verbose
    
    def run(self, program):
        """TODO describe function

        :param program: 
        :returns: 

        """
        for command in program:
            command.visit(self)

    def on_command(self, command):
        raise NotImplementedError
    
    def on_declare_parameter(self, command):
        for param in command.parameters:
            if param.name in self.symbol_table:
                raise pylta.exceptions.SymbolRedefinition(
                    param,
                    meta=command.meta
                )
            self.lta.add_parameter(param)
            self.symbol_table[param.name] = param
        
    def on_declare_parameter_relation(self, command):
        visited_relation = command.relation.visit(self)
        pylta.exceptions.check_formula_layering(
            self.lta, visited_relation, None, command.meta)
        self.lta.add_parameter_relation(visited_relation)

    def on_declare_layers(self, command):
        for layer in command.layers[:-1]:
            if layer.name in self.symbol_table:
                raise pylta.exceptions.SymbolRedefinition(
                    layer,
                    meta=command.meta
                )
            self.lta.append_layer(layer)
            self.symbol_table[layer.name] = layer
        layer = command.layers[-1]
        if self.lta.is_layer(layer):
            index = self.lta.get_layer_index(layer)
            self.lta.layers.set_loop(index)
        else:
            if layer.name in self.symbol_table:
                raise pylta.exceptions.SymbolRedefinition(
                    layer,
                    meta=command.meta
                )
            self.lta.append_layer(layer)
            self.symbol_table[layer.name] = layer
            self.lta.append_layer(pylta.variables.EMPTY_LAYER)
            index = self.lta.get_layer_index(pylta.variables.EMPTY_LAYER)
            self.lta.layers.set_loop(index)
            

    def on_declare_states(self, command):
        for state in command.states:
            if not self.lta.is_layer(state.layer):
                raise pylta.exceptions.SymbolUndefined(
                    state.layer,
                    meta=command.meta
                )
            if self.lta.is_state(state):
                raise pylta.exceptions.SymbolRedefinition(
                    state,
                    meta=command.meta
                )
            self.lta.add_state(state)

    def on_rule(self, command):
        src = command.src
        if not self.lta.is_state(src):
            raise pylta.exceptions.SymbolUndefined(
                src,
                meta=command.meta
            )
        expected_layer = self.lta.next_layer(src.layer)
        for dest, guard in command.successors.items():
            if not dest.layer == expected_layer:
                raise pylta.exceptions.LayeringError(
                    dest,
                    expected_layer,
                    meta=command.meta
                )
            if not self.lta.is_state(dest):
                raise pylta.exceptions.SymbolUndefined(
                dest,
                meta=command.meta
            )
            visited_guard = guard.visit(self)
            pylta.exceptions.check_formula_layering(
                self.lta, visited_guard, src.layer, command.meta)
            visited_guard = pylta.formulas.Or(visited_guard,
                                              self.lta.get_guard(src, dest))
            self.lta.set_guard(src, dest, visited_guard)

    def on_bind_term(self, command):
        if command.name in self.symbol_table:
            raise pylta.exceptions.SymbolRedefinition(
                command.name,
                meta=command.meta
            )
        interpreted_term = command.term.visit(self)
        self.symbol_table[command.name] = interpreted_term
        
    def on_bind_formula(self, command):
        if command.name in self.symbol_table:
            raise pylta.exceptions.SymbolRedefinition(
                command.name,
                meta=command.meta
            )
        interpreted_formula = command.formula.visit(self)
        self.symbol_table[command.name] = interpreted_formula

    def on_verify(self, command):
        print()
        print("VERIFYING {}".format(command.formula))
        if self.verbose:
            checker = pylta.checker.VerboseChecker(self.lta)
        else:
            checker = pylta.checker.Checker(self.lta)
        for pred, uninterpreted_form in command.predicates.items():
            interpreted_form = uninterpreted_form.visit(self)
            pylta.exceptions.check_formula_layering(
                self.lta, interpreted_form, pred.layer, meta=command.meta)
            checker.add_predicate(pred, interpreted_form)

        ltl_formula = command.formula.visit(self)
        for var in ltl_formula.get_variables():
            # This is the simplest way i have found to take an alement in a
            # set if it not empty
            raise pylta.exceptions.TypeError_(
                var, 
                pylta.variables.Types.PREDICATE,
                meta=command.meta
            )
        for pred in ltl_formula.get_predicates():
            if pred.name and pred not in checker.predicates:
                raise pylta.exceptions.SymbolUndefined(
                    pred,
                    meta = command.meta
                )
        checker.set_formula(ltl_formula)
        res = checker.cegar_solve()
        if res is None:
            print("Cannot verify formula")
        elif res == True:
            print("Formula is Valid")
        else:
            print("Formula is False, here is a counter-example:")
            print(str(res))

    def on_formula(self, formula):
        visited_children = [
            child.visit(self)
            for child in formula.get_children()
        ]
        result = type(formula)(*visited_children)
        result.meta = formula.meta
        return result

    def on_formula_constant(self, formula):
        return formula
    
    def on_formula_identifier(self, formula):
        if formula.ident not in self.symbol_table:
            raise pylta.exceptions.SymbolUndefined(
                formula.ident,
                meta=formula.meta
            )
        ident_value = self.symbol_table[formula.ident]
        match ident_value.type_:
            case pylta.variables.Types.FORMULA:
                return ident_value
            case pylta.variables.Types.LAYER:
                pred = pylta.variables.Predicate.from_layer(ident_value)
                return pylta.formulas.NamedPredicate(pred)
            case _:
                raise pylta.exceptions.TypeError_(
                ident_value,
                pylta.variables.Types.FORMULA,
                meta=formula.meta
                )

    def on_predicate_operation(self, formula):
        visited_left = formula.left_term.visit(self)
        visited_right = formula.right_term.visit(self)
        result = type(formula)(visited_left, visited_right)
        result.meta = formula.meta
        return result

    def on_named_predicate(self, formula):
        result = pylta.formulas.NamedPredicate(formula.predicate)
        result.meta = formula.meta
        return result
    
    def on_term(self, term):
        visited_children = [
            child.visit(self)
            for child in term.get_children()
        ]
        result = type(term)(*visited_children)
        result.meta = term.meta
        return result

    def on_term_constant(self, term):
        return term
    
    def on_next_layer(self, term):
        raise NotImplementedError
    
    def on_term_edge(self, term):
        """This type of terms are left unchanged, the interpreter only
        verifies that the edge is well formed (the source and
        destination are existing states belonging to successive
        layers).

        :param term: a pylta.terms.TermEdge object

        :returns: the term unchanged

        :raises pylta.exceptions.SymbolUndefined: if a State that
            appear in the edge is not in the lta.
        :raises pylta.exceptions.LayeringError: if the layer of the
            source state does not precede the layer of the
            destination.
        """
        src = term.content.src
        dest = term.content.dest
        if self.lta.next_layer(src.layer) != dest.layer:
            raise pylta.exceptions.LayeringError(
                dest,
                dest_layer,
                meta=term.meta
            )
        if not self.lta.is_state(src):
            raise pylta.exceptions.SymbolUndefined(
                src,
                meta=term.meta
            )
        if not self.lta.is_state(dest):
            raise pylta.exceptions.SymbolUndefined(
                dest,
                meta=term.meta
            )
        return term

    def on_term_state(self, term):
        """This type of terms are left unchanged, the interpreter only
        checks that the layer of the state exists and that the state
        itself has been defined.

        :param term: a pylta.terms.TermState object

        :returns: the term unchanged

        :raises pylta.exceptions.SymbolUndefined: if the Layer or the
            state have not been defined earlier.
        """
        if not self.lta.is_state(term.content):
            raise pylta.exceptions.SymbolUndefined(
                term.content,
                meta=term.meta
            )
        return term
    
    def on_term_identifier(self, term):
        identifier = term.content
        if identifier not in self.symbol_table:
            raise pylta.exceptions.SymbolUndefined(
                identifier,
                meta=term.meta
            )
        replacing_term = self.symbol_table[identifier]
        match replacing_term.type_:
            case pylta.variables.Types.PARAMETER:
                # parameters are added to lta and symbol_table at the same time.
                assert self.lta.is_parameter(replacing_term)
                result = pylta.terms.TermParameter(replacing_term)
                result.meta = term.meta
                return result
            case pylta.variables.Types.TERM:
                return replacing_term
            case _:
                # no other types will work
                raise pylta.exceptions.TypeError_(
                    identifier,
                    pylta.variables.Types.TERM,
                    meta=term.meta
                )

    def on_term_parameter(self, term):
        raise NotImplementedError
