import pylta.utils
import pylta.variables
import pylta.terms
import pylta.formulas

class LTA:
    """
    A class for representing Layered Threshold Automata.

    Instances of this class are meant to be built dinamically, the
    constructor instanciate an empty LTA, then the following methods
    are used to populate it:

        - :meth:`add_parameter`

        - :meth:`add_prameter_relation`

        - :meth:`append_layer`

        - :meth:`add_state`

        - :meth:`set_guard`

    Then methods:

        - :meth:`parameter_constraints`

        - :meth:`state_constraints`

        - :meth:`edge_constraints`

        - :meth:`guard_constraints`

        - :meth:`out_constraints`

        - :meth:`in_constraints`

    Are used to obtain :class:`pylta.formulas.Formula` that enforces
    the semantic of the LTA.

    Note that no tests are done to guarantee that the LTA follow the
    layering hypothesis.  This is curently done by
    :class:`pylta.interpreter.Interpreter`, and by the API whent it
    will be implemented.
    """
    def __init__(self):
        """Create an empty LTA.
        """
        self.parameters = set()
        self.layers = pylta.variables.LayerList()
        self.guards = {}
        self.parameter_relations = []

    def copy(self):
        result = type(self)()
        result.parameters = self.parameters.copy()
        result.layers = self.layers.copy()
        for layer, gaurd_layer in self.guards.items():
            result.guards[layer] = {}
            for src, guard_dict in guard_layer.copy():
                result.guards[layer][src] = guard_dict.copy()
        result.parameter_relations = self.parameter_relations.copy()
        return result

    def is_variable(self, var):
        """Check if the input is a variable defined in the LTA.

        :param var: A :class:`pylta.variables.Parameter` or a
            :class:`pylta.variables.State` or a
            :class:`pylta.variables.Edge` are expected.  Will always
            return False otherwise.

        :returns: True if `var` is defined in the LTA.
        """
        match var.type_:
            case pylta.variables.Types.PARAMETER:
                return self.is_parameter(var)
            case pylta.variables.Types.STATE:
                return self.is_state(var)
            case pylta.variables.Types.EDGE:
                return self.is_edge(var)
            case _:
                return False
            
    def append_layer(self, layer):
        """Adds a new layer type

        :param layer: A :class:`pylta.variables.Layer` object.
        """
        self.layers.append(layer)
        self.guards[layer] = {}
            
    def get_layers(self):
        """Return the list of layers of the LTA.

        :returns: A list of :class:`pylta.variables.Layer`.

        """
        return self.layers

    def get_layer_index(self, layer):
        """Return the index of `layer` in the LTA.

        :param layer: a :class:`pylta.variables.Layer` of the LTA.

        :returns: the index of `layer` in the LTA.
        """
        return self.layers.index(layer)
    
    def get_layer_at(self, index):
        """Return the layer of the LTA at `index`

        The layers of the LTA forms a repeating cycle meaning that
        `lta.get_layer_at(index + len(lta.get_layers)) ==
        `lta.get_layer_at(index)`

        :param index: an integer.

        :returns: a :class:`pylta.variables.Layer` of the LTA.
        """
        return self.layers[index]

    def is_layer(self, layer):
        """Test if `layer` is a layer of the LTA.

        :param layer: a :class:`pylta.variables.Layer` object.

        :returns: `True` if `layer` is in the LTA.
        """
        return layer in self.layers

    def next_layer(self, layer, k=1):
        """Return the layer that follows the input.

        Wraps back to the first layer if necessary.

        If another k is given, will instead return the k-th layer
        after the specified one.  For example, if k is -1, it will
        output the preceding layer.

        :param layer: a :class:`pylta.variables.Layer` of the LTA.

        :param k: an integer

        :returns: the :class:`pylta.variables.Layer` after the input
                  in the lta
        """
        index = self.get_layer_index(layer)
        return self.get_layer_at(index + k)

    def add_parameter(self, parameter):
        """Add `parameter` to the LTA.

        :param parameter: A :class:`pylta.variables.Parameter` object.
        """
        assert parameter not in self.parameters  #TODO: proper exception
        self.parameters.add(parameter)
    
    def get_parameters(self):
        """Return the set of parameters of the LTA.

        :returns: a set of :class:`pylta.variables.Parameter` objects.
        """
        
        return self.parameters

    def is_parameter(self, param):
        """Test if `param` is a parameter of the LTA.

        :param param: a :class:`pylta.variables.Parameter` object.
        :returns: `True` if `param` is in the LTA.

        """
        return param in self.parameters

    def add_state(self, state):
        """Add `state` to the LTA.

        The layer of `state` should be in the LTA.

        :param state: A :class:`pylta.variables.state` object.
        """
        assert state.layer in self.layers  #TODO: proper exception
        assert state not in self.guards[state.layer]  #TODO: proper exception
        self.guards[state.layer][state] = {}
    
    def get_states(self, layer):
        """Return the states of the LTA at `layer`.

        :param layer: A :class:`pylta.variables.Layer` of the LTA.

        :returns: Some :class:`pylta.variables.State`
        """
        return self.guards[layer].keys()

    def is_state(self, state):
        """Test if `state` is a state of the LTA.

        :param param: a :class:`pylta.variables.State` object.

        :returns: `True` if `state` is in the LTA.
        """
        layer = state.layer
        return state in self.guards[layer]
    
    def get_successors(self, state):
        """Return the successors of `state` in the LTA.

        :param state: A :class:`pylta.variables.State` of the
            automaton.

        :returns: Some :class:`pylta.variables.State`.x
        """
        return self.guards[state.layer][state].keys()

    def get_edges(self, layer):
        """Yield the out edges of `layer`.

        :param layer: A :class:`pylta.variable.Layer` of the
            automaton.

        :returns: A list of :class:`pylta.variables.Edge`.
        """
        for src in self.get_states(layer):
            for dest in self.get_successors(src):
                yield pylta.variables.Edge(src, dest)
    
    def is_edge(self, edge):
        """Test if `edge' is in the LTA.

        :param edge: A :class:`pylta.variables.Edge` object.

        :returns: `True` if `edge` is an edge in the LTA (meaning its
                  guard has been set).
        """
        if not self.is_state(edge.src):
            return False
        return edge.dest in self.guards[edge.src.layer][edge.src]
    
    def set_guard(self, src, dest, formula):
        """Set the guard between `src` and `dest` to `formula`.

        :param src: A :class:`pylta.variables.State` object.
        :param dest: A :class:`pylta.variables.State` object.
        :param formula: A :class:`pylta.formulas.Formula` object.
        """
        
        # TODO: safeguards to ensure proper layering
        self.guards[src.layer][src][dest] = formula
    
    def get_guard(self, src, dest):
        """Return the guard between `src` and `dest`.

        :param src: A :class:`pylta.variables.State` of the automaton.
        :param dest: A :class:`pylta.variables.State` of the
            automaton.

        :returns: A :class:`pylta.formulas.Formula` object
        """
        try:
            res = self.guards[src.layer][src][dest]
        except KeyError:
            res = pylta.formulas.FormulaConstant(False)
        return res

    def add_parameter_relation(self, relation):
        """Add the parameter relation `relation` to the LTA

        `relation` should only contain parameter variables.

        :param relation: A :class:`pylta.formulas.Formula` object
        """
        
        self.parameter_relations.append(relation)
    
    def get_parameter_relations(self):
        """Return the parameter relations of the LTA.

        :returns: A list of :class:`pylta.formulas.Formula` objects
                  over the :class:`pylta.variables.Parameter` of the
                  automaton.
        """
        
        return self.parameter_relations

    def __str__(self):
        """Return a string representation of the LTA.

        When given to :class:`pylta.parser.Parser`, outputs a program
        that can be interpreted by
        :class:`pylta.interpreter.Interpreter` to recreate the LTA.
        """
        res = "PARAMETERS: "
        res += ", ".join([str(p) for p in self.get_parameters()])
        res += "\n"
        for relation in self.get_parameter_relations():
            res += "PARAMETER_RELATION: " + str(relation) + "\n"
        if self.get_layers()[len(self.get_layers())-1].name:
            #The LTA is infinite with a loop:
            layer_names = [
                str(self.get_layers()[i])
                for i in range(len(self.get_layers())+1)
            ]
            # +1 to add the loop
        else:
            #The lta is finite with an empty layer at the end
            layer_names = [
                str(self.get_layers()[i])
                for i in range(len(self.get_layers())-1)
            ]
            # -1 to omit the final empty layer
        res += "LAYERS: "
        res += ", ".join(layer_names)
        res += "\n"
        for layer in self.get_layers():
            res += "STATES: "
            res += ", ".join([str(s) for s in self.get_states(layer)])
            res += "\n"
        for layer in self.get_layers():
            for src in self.get_states(layer):
                res += "CASE " + str(src) + ":\n"
                for dest in self.get_successors(src):
                    guard = self.get_guard(src, dest)
                    res += "  IF {} THEN {}\n".format(guard, dest)
        return res

    def concrete_valuation(self, index):
        layer = self.get_layer_at(index)
        return pylta.paths.ConcreteValuation(layer)
    
    def concrete_path(self, start, end, loop_start=None):
        result = pylta.paths.ConcretePath(start)
        for index in range(start, end):
            result.append(self.concrete_valuation(index))
        result.loop_start = loop_start
        return result
    
    def parameter_constraints(self):
        """Return a list of constraints over parameters of the LTA.

        These include their non-negativity as well as the specified
        parameter_relations.

        :returns: A list of :class:`pylta.formulas.Formula` objects.
        """
        for parameter in self.get_parameters():
            term_param = pylta.terms.TermParameter(parameter)
            term_0 = pylta.terms.TermConstant(0)
            yield pylta.formulas.GreaterEqual(term_param, term_0)
        for relation in self.get_parameter_relations():
            yield relation
        
    def state_constraints(self, layer):
        """Return a list of constraints over states of `layer`.

        These constraints are the non-negativity of these variables.

        :param layer: A :class:`pylta.variables.Layer` of the LTA.

        :returns: A list of :class:`pylta.formulas.Formula` objects.
        """
        for state in self.get_states(layer):
            term_state = pylta.terms.TermState(state)
            term_0 = pylta.terms.TermConstant(0)
            yield pylta.formulas.GreaterEqual(term_state, term_0)

    def edge_constraints(self, layer):
        """Return a list of constraints over edges of `layer`

        Namely these constraints impose the non-negativity of these
        variables.

        :param layer: A :class:`pylta.variables.Layer` of the LTA.

        :returns: A list of :class:`pylta.formulas.Formula` objects.
        """
        for src in self.get_states(layer):
            for dest in self.get_successors(src):
                edge = pylta.variables.Edge(src=src, dest=dest)
                term_edge = pylta.terms.TermEdge(edge)
                term_0 = pylta.terms.TermConstant(0)
                yield pylta.formulas.GreaterEqual(term_edge, term_0)

    def guard_constraints(self, layer):
        """Return a list of constraints relating to guards at `layer`.

        :param layer: A :class:`pylta.variables.Layer` of the LTA.

        :returns: A list of :class:`pylta.formulas.Formula` objects.
        """
        for src in self.get_states(layer):
            for dest in self.get_successors(src):
                edge = pylta.variables.Edge(src=src, dest=dest)
                term_edge = pylta.terms.TermEdge(edge)
                term_0 = pylta.terms.TermConstant(0)
                precond = pylta.formulas.GreaterThan(term_edge, term_0)
                guard = self.get_guard(src, dest)
                yield pylta.formulas.Implies(precond, guard)
            
    def out_constraints(self, layer):
        """Build a list of constraints over states and edges of
        `layer`.

        These conditions encode that no more process must leave a
        state that were in it.

        :param layer: A :class:`pylta.variables.Layer` of the LTA.

        :returns: A list of :class:`pylta.formulas.Formula` objects.
        """
        
        for src in self.get_states(layer):
            left_term = pylta.terms.TermState(src)
            edges = [
                pylta.variables.Edge(src, dest)
                for dest in self.get_successors(src)
            ]
            right_term = pylta.terms.Sum(*[pylta.terms.TermEdge(edge) for edge in edges])
            yield pylta.formulas.GreaterEqual(left_term, right_term)

    def in_constraints(self, layer):
        """Build a list of constraints over edges of `layer` and
        states of the next layer.

        These conditions encode that process that every process that
        went through a state must have come from an edge.  It cannot
        be enforced at the very beginning of the algorithm.

        This make use of :class:`pylta.terms.NextLayer` objects to
        allow states from the next layer together with edges of the
        current layer.

        :param layer: A :class:`pylta.variables.Layer` of the LTA.

        :returns: A list of :class:`pylta.formulas.Formula` objects.
        """
        # We compute the sum of entering edges for every states in the
        # next layer
        next_layer = self.next_layer(layer)
        in_edges = {dest: [] for dest in self.get_states(next_layer)}
        for src in self.get_states(layer):
            for dest in self.get_successors(src):
                edge = pylta.variables.Edge(src=src, dest=dest)
                in_edges[dest].append(edge)
        for dest, edges in in_edges.items():
            left_term = pylta.terms.Sum(*[pylta.terms.TermEdge(edge) for edge in edges])
            # Normaly, edges variables can only cohabit in terms with their src variables
            right_term = pylta.terms.NextLayer(
                pylta.terms.TermState(dest),
                1
            )
            yield pylta.formulas.Equal(left_term, right_term)
            
    def all_but_in_constraints(self, layer):
        yield from self.state_constraints(layer)
        yield from self.edge_constraints(layer)
        yield from self.guard_constraints(layer)
        yield from self.out_constraints(layer)
            
    def temporal_constraints(self):
        """Return a temporal formula imposing the correct alternance
        of layer predicates.

        If the layer of the LTA are L0, L1, L2, then the resulting
        formula will be:

        L0 & G (L0 -> X L1) & G (L1 -> X L2) & G (L2 -> X L0) &
        G (! L0 | ! L1) & G (! L0 | ! L2) & G (! L1 | ! L2)

        :returns: A :class:`pylta.formulas.Formula` object.
        """
        res = []
        res.append(pylta.formulas.NamedPredicate(
            pylta.variables.Predicate.from_layer(self.get_layer_at(0))))
        layers = list(self.get_layers())
        for i, layer in enumerate(layers):
            pred_layer = pylta.formulas.NamedPredicate(
                pylta.variables.Predicate.from_layer(layer))
            pred_next = pylta.formulas.NamedPredicate(
                pylta.variables.Predicate.from_layer(self.next_layer(layer)))
            res.append(
                pylta.formulas.Globally(
                    pylta.formulas.Implies(
                        pred_layer,
                        pylta.formulas.Next(pred_next)
                    )
                )
            )
            for other_layer in layers[(i+1):]:
                pred_other = pylta.formulas.NamedPredicate(
                    pylta.variables.Predicate.from_layer(other_layer))
                res.append(
                    pylta.formulas.Globally(
                        pylta.formulas.Or(
                            pylta.formulas.Not(pred_layer),
                            pylta.formulas.Not(pred_other)
                        )
                    )
                )
        return pylta.formulas.And(*res)
