"""
This module defines the different commands that can appear in a pyLTA
file.

These commands would typically be created by pylta.parser.Parser from
an input file.
"""

import pylta.variables
import pylta.terms
import pylta.formulas

class Program(list):
    """A pyLTA program consists simply of a list of commands.
    """
    def __str__(self):
        """Return a string representation of the program.

        The output can be parsed by :class:`pylta.parser.Parser()`.
        """
        str_list = [str(command) for command in self]
        return "".join(str_list)

class Command:
    """An abstract base class for representing commands.

    Each commands should overload three methods: the constructor, the
    `__str__` method and the `visit` method.
    """
    def __init__(self):
        raise NotImplementedError

    def visit(self, visitor):
        """Return the output of the appropriate method from `visitor`.

        :param visitor: a :class:`pylta.commands.CommandVisitor`
            instance.

        :returns: the output of the appropiate method of `visitor`.
        """
        return visitor.on_command(self)

    def __str__(self):
        """Return the string representation of the command.

        This string representation can be parsed by
        :class:`pylta.parser.Parser` to recreate the command.
        """
        raise NotImplementedError

class DeclareParameter(Command):
    """Encode the `PARAMETER: ...` instruction.
    """
    def __init__(self, *parameters):
        """Create a command that declares the input parameters.

        :param parameters: Some :class:`pylta.variables.Parameter`
            objects.
        """
        
        self.parameters = parameters

    def visit(self, visitor):
        return visitor.on_declare_parameter(self)

    def __str__(self):
        str_parameters = [str(parameter) for parameter in self.parameters]
        return "PARAMETERS: {}\n".format(", ".join(str_parameters))

class DeclareParameterRelation(Command):
    """Encode the `PARAMETER_RELATION: ...` instruction.
    """
    def __init__(self, relation):
        """Create a command that impose the input parameter relation.

        :param relation: a :class:`pylta.formulas.Formula` object.
        """
        
        self.relation = relation

    def visit(self, visitor):
        return visitor.on_declare_parameter_relation(self)

    def __str__(self):
        return "PARAMETER_RELATION: {}\n".format(self.relation)

class DeclareLayers(Command):
    """Encode the `LAYERS: ...` instruction.
    """
    def __init__(self, *layers):
        """Create a command representing the layer list.

        :param layers: Some :class:`pylta.variables.Layer`.
        """
        self.layers = layers

    def visit(self, visitor):
        return visitor.on_declare_layers(self)

    def __str__(self):
        str_layers = [str(layer) for layer in self.layers]
        return "LAYERS: {}\n".format(", ".join(str_layers))

class DeclareStates(Command):
    """Encode the `STATES: ...` instruction.
    """
    def __init__(self, *states):
        """Build a command decaring some states

        :param states: some :class:`pylta.variables.State` objects
        """
        self.states = states

    def visit(self, visitor):
        return visitor.on_declare_states(self)

    def __str__(self):
        str_states = [str(state) for state in self.states]
        return "STATES: {}\n".format(", ".join(str_states))
    
class Rule(Command):
    """Encode the `CASE _: IF _ THEN _ ...` instruction.
    """
    def __init__(self, src, successors):
        """Build a command representing successors rules.

        The command `Rule(src, {dest1: guard1, dest2: guard2})` encode
        the instruction:

        CASE src: 
          IF guard1 THEN dest1
          IF guard2 THEN dest2

        :param src: A :class:`pylta.variables.State` object
        :param successors: A dictionary with
            :class:`pylta.variables.State` as keys and
            :class:`pylta.formulas.Formula` as values
        """
        self.src = src
        self.successors = successors

    def visit(self, visitor):
        return visitor.on_rule(self)

    def __str__(self):
        str_successors = [
            "  IF {} THEN {}".format(formula, state)
            for state, formula in self.successors.items()
        ]
        joined_successors = "\n".join(str_successors)
        return "CASE {}:\n{}\n".format(self.src, joined_successors)
    
class BindTerm(Command):
    """Encode the `TERM _: ...` instruction.
    """
    def __init__(self, name, term):
        """Create a command that binds a term to an identifier.

        :param name: A :class:`pylta.variables.Identifier` object.
        :param term: A :class:`pylta.terms.Term` object
        """
        
        self.name = name
        self.term = term

    def visit(self, visitor):
        return visitor.on_bind_term(self)

    def __str__(self):
        return "TERM {}: {}\n".format(self.name, self.term)
    
class BindFormula(Command):
    """Encode the `FORMULA _: ...` instruction.
    """
    def __init__(self, name, formula):
        """Create a command that binds `formula` to `name`.

        :param name: A :class:`pylta.variables.Identifier` object.
        :param formula: A :class:`pylta.formulas.Formula` object.
        :returns: 

        """
        
        self.name = name
        self.formula = formula

    def visit(self, visitor):
        return visitor.on_bind_formula(self)

    def __str__(self):
        return "FORMULA {}: {}\n".format(self.name, self.formula)
    
class Verify(Command):
    def __init__(self, formula, predicates):
        self.formula = formula
        self.predicates = predicates

    def visit(self, visitor):
        return visitor.on_verify(self)

    def __str__(self):
        pred_defs = "".join(["  {}: {}\n".format(pred, formula)
                             for pred, formula in self.predicates.items()])
        
        return "WITH\n" + pred_defs + "VERIFY: " + str(self.formula) + "\n"
        
class CommandVisitor:
    """The visitor for Command objects.

    Instances of this class have one method for each type of commands
    that can be called with their :meth:`Command.visit()` method.
    This gives a nice way of performing pattern matching.

    See :class:`pylta.interpreter.Interpreter` for an example of
    usage.
    """
    def on_command(self, command):
        raise NotImplementedError

    def on_declare_parameter(self, command):
        return self.on_command(command)

    def on_declare_parameter_relation(self, command):
        return self.on_command(command)

    def on_declare_layers(self, command):
        return self.on_command(command)

    def on_declare_states(self, command):
        return self.on_command(command)

    def on_rule(self, command):
        return self.on_command(command)

    def on_bind_term(self, command):
        return self.on_command(command)
    
    def on_bind_formula(self, command):
        return self.on_command(command)

    def on_verify(self, command):
        return self.on_command(command)
