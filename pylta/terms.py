"""
This module define the terms that appear in formulas of LTAs.

The terms corresponds to integer arithmetic formulas over variables
that can be parameters, states or edges of an LTA.
"""

import enum
import gmpy2

import pylta.variables

class TermPrecedence(enum.IntEnum):
    """
    Used for placing parenthesis only when necessary when printing out
    terms.
    """
    ATOM = 0
    NEGATION = 1
    MULTIPLICATION = 2
    ADDITION = 3

class Term:
    """An abstract class for terms of formulas.
    """
    
    precedence = None
    type_ = pylta.variables.Types.TERM

    def __init__(self):
        """Term is an abstract class and should not be instanciated
        directly.  Look for the children classes in :mod:`pylta.terms`
        instead.
        """
        # The meta field should be set by the pylta.parser.Parser and
        # contains information such as the line where the term appears.
        self.meta = None
        
    def visit(self, visitor, **kwargs):
        """Return the output of the appropriate method from `visitor`.

        Look for the class :class:`pylta.terms.TermVisitor` for more
        details.

        :param visitor: A :class:`pylta.terms.TermVisitor` instance.

        :returns: The output of the appropriate method of `visitor`
        """
        return visitor.on_term(self, **kwargs)

    def get_children(self):
        """Return the child terms of the object.

        :returns: A list of :class:`pylta.terms.Term` objects.
        """
        # This should be overriden in subclasses
        raise NotImplementedError
    
    def __str__(self):
        """Return a string representation of the terms.

        The output can be parsed by :class:`pylta.parser.Parser`.
        """
        raise NotImplementedError

    def get_variables(self):
        """Return the set of variables that appear in a term.

        Will fail if the term contains uninterpreted
        :class:`pylta.terms.TermIdentifier` subterms.

        The output can contain :class:`pylta.variables.Parameter`,
        :class:`pylta.variables.State` or
        :class:`pylta.variables.Edge`.

        :returns: The set of variables that appear in the term.
        """
        raise NotImplementedError

class ArithmeticOperation(Term):
    """
    An abstract class for encoding Sum and Products of arbitrarily
    many terms.
    """
    def __init__(self, *children):
        """This is an abstract class and the constructor should not be
        called directly, look for :class:`pylta.terms.Sum` and
        :class:`pylta.terms.Product` instead.
        """
        super().__init__()
        self.children = children

    def get_children(self):
        return self.children
        
    def visit(self, visitor, **kwargs):
        return visitor.on_arithmetic_operation(self, **kwargs)

    def get_variables(self):
        child_vars = [child.get_variables() for child in self.get_children()]
        if child_vars:
            # set.union() requires at least one argument
            return set.union(*child_vars)
        return set()
    
class Sum(ArithmeticOperation):
    """
    A term encoding the sum of arbitrarily many other terms.
    """
    
    precedence = TermPrecedence.ADDITION

    def __init__(self, *children):
        """Create a term representing the sum of the input.

        By convention, a sum with no children represents the constant
        0.

        :param children: some :class:`pylta.terms.Term` objects
        """
        super().__init__(*children)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_sum(self, **kwargs)

    def __str__(self):
        if not self.children:
            return "0"
        
        result = str(self.children[0])
        for child in self.children[1:]:
            # a + (-1 * b) can be printed as a - b
            if (isinstance(child, Product)
                and len(child.children) == 2
                and isinstance(child.children[0], TermConstant)
                and child.children[0].value < 0):
                mult = child.children[0].value
                if mult == -1:
                    if child.children[1].precedence >= self.precedence:
                        # Parenthesis are needed around a - (b + c)
                        result = "{} - ({})".format(result, child.children[1])
                    else:
                        # Parenthesis can be omitted
                        result = "{} - {}".format(result, child.children[1])
                else:
                    if child.children[1].precedence >= self.precedence:
                        # Parenthesis are needed around a - 2 * (b + c)
                        result = "{} - {} * ({})".format(result,
                                                         mult,
                                                         child.children[1])
                    else:
                        # Parenthesis can be omitted
                        result = "{} - {} * {}".format(result, mult, child.children[1])
            else:
                # Parenthesis are never necessary since addition is associative
                # and has the highest precedence
                result = "{} + {}".format(result, child)
        return result

class Product(ArithmeticOperation):
    """
    A term encoding the multiplication of arbitrarily many other terms.
    """
    
    precedence = TermPrecedence.MULTIPLICATION

    def __init__(self, *children):
        """Create a term representing the product of the input.

        By convention, a product with no children represents the
        constant 1.

        :param children: some :class:`pylta.terms.Term` objects
        """
        super().__init__(*children)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_product(self, **kwargs)

    def __str__(self):
        if not self.children:
            return "1"
        # Depending if the first child is -1 or not, the first child may
        # be printed or not. This index is used to remember this.
        next_index = 0
        result = ""
        if (len(self.children) >= 2
            and isinstance(self.children[0], TermConstant)
            and self.children[0].value == -1):
            # we write - b * c instead of (-1) * b * c
            if self.children[1].precedence > self.precedence:
                # Parenthesis are needed
                result = "- ({})".format(self.children[1])
            else:
                result = "- {}".format(self.children[1])
            next_index = 2
        else:
            if self.children[0].precedence > self.precedence:
                # Parenthesis are needed
                result = "({})".format(self.children[0])
            else:
                result = str(self.children[0])
            next_index = 1
        # next_index represents the first index that has not been printed
        for child in self.children[next_index:]:
            if child.precedence > self.precedence:
                # Parenthesis are needed
                result = "{} * ({})".format(result, child)
            else:
                result = "{} * {}".format(result, child)
        return result

class NextLayer(Term):
    """
    Reference terms from the next layer.

    Typically, `a.0 + b.1` would not be a valid terms if `a` and `b`
    are successive layers.  When such a term is necessary, one may
    write `a.0 + NEXT(b.1)`.  This `NEXT` operation is encoded by this
    class.

    These instructions need to be explicit because of the cyclic
    nature of the layers of an LTA.  for example, if an LTA has a
    single repeating layer, then there is no way of knowing wether the
    term a.0 refers to the current layer or to the next one.

    For an example of usage of these terms, look for
    :meth:`pylta.lta.LTA.in_constraints`.

    This class is currently not supported in CHECK instructions.
    """

    precedence = TermPrecedence.NEGATION
    
    def __init__(self, child, offset=1):
        """Create a term encoding NEXT(child).

        Instead of writing Next(Next(...(Next(child))...)), one can
        write Next(child, offset=n).  This might even work with
        negative values (untested).

        :param child: A :class:`pylta.terms.Term` object
        :param offset: An integer
        """
        super().__init__()
        self.child = child
        self.offset = offset

    def visit(self, visitor, **kwargs):
        return visitor.on_next_layer(self, **kwargs)

    def get_children(self):
        return [self.child]

    def get_variables(self):
        return self.child.get_variables()
    
    def __str__(self):
        if self.offset == 1:
            return "NEXT({})".format(self.child)
        else:
            return "NEXT^{}({})".format(self.offset, self.child)


class TermConstant(Term):
    """A term encoding an integer constant.
    """
    
    precedence = TermPrecedence.ATOM
    
    def __init__(self, value):
        """Create a term encoding the input integer constant.

        :param value: either an `int` or a :class:`gmpy2.mpz`
            multiprecision integer.
        """
        
        super().__init__()
        # multiprecision integers
        self.value = gmpy2.mpz(value)

    def visit(self, visitor, **kwargs):
        return visitor.on_term_constant(self, **kwargs)

    def get_children(self):
        return []
    
    def get_variables(self):
        return set()
    
    def __str__(self):
        if self.value < 0:
            return "- {}".format(abs(self.value))
        return str(self.value)

    
class TermVariable(Term):
    """
    An abstract class encoding variables as they appear in terms.
    """
    
    precedence = TermPrecedence.ATOM
    
    def __init__(self, arg):
        """This is an abstract class and should not be instanciated
        directly.

        :param arg: The variable that needs to appear in a
            :class:`pylta.terms.Term`.
        """
        super().__init__()
        self.content = arg

    def visit(self, visitor, **kwargs):
        return visitor.on_term_variable(self, **kwargs)

    def get_children(self):
        return []

    def get_variables(self):
        return set([self.content])
    
    def __str__(self):
        return str(self.content)


class TermEdge(TermVariable):
    """
    A term that corresponds to an edge of an LTA.
    """

    def __init__(self, edge):
        """
        Create an edge variable.

        :param edge: a :class:`pylta.variables.Edge` object.
        """
        super().__init__(edge)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_term_edge(self, **kwargs)

class TermState(TermVariable):
    """
    A term that corresponds to a state of an LTA
    """

    def __init__(self, state):
        """
        Create a state variable.

        :param state: a :class:`pylta.variables.State` object.
        """
        super().__init__(state)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_term_state(self, **kwargs)
    
class TermIdentifier(TermVariable):
    """A term that references another term.

    These are replaced by their corresponding terms during
    interpretation.
    """

    def __init__(self, ident):
        """
        Create a term with the given identifier.

        :param ident: a :class:`pylta.variables.Identifier` object
        """
        super().__init__(ident)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_term_identifier(self, **kwargs)


class TermParameter(TermVariable):
    """
    A term that corresponds to a parameter of an LTA.
    """

    def __init__(self, parameter):
        """
        Create a parameter variable.

        :param parameter: a :class:`pylta.variables.Parameter` object
        """
        super().__init__(parameter)

    def visit(self, visitor, **kwargs):
        return visitor.on_term_parameter(self, **kwargs)
    
class TermVisitor:
    """The visitor for terms object.

    Instances of this class have one method for each type of terms
    that can be called with their :meth:`Term.visit()` method.  This
    gives a nice way of performing pattern matching.

    The default methods defined here are meant to be overloaded.  By
    default, methods for a subclass call the method of the parent
    class, meaning for example that if all
    :class:`pylta.terms.ArithmeticOperations` (meaning both
    :class:`pylta.terms.Sum` and :class:`pylta.terms.Product`) should
    be handled the same, it should be enough to overload
    :meth:`pylta.terms.TermVisitor.on_arithmetic_operation()` and
    leave :meth:`pylta.terms.TermVisitor.on_sum()` and
    :meth:`pylta.terms.TermVisitor.on_product()` unchanged.

    For some examples of usage, see
    :class:`pylta.interpreter.Interpreter` or
    :class:`pylta.translator.FormulaToSMT`.
    """
    def on_term(self, term, **kwargs):
        raise NotImplementedError

    def on_arithmetic_operation(self, term, **kwargs):
        return self.on_term(term, **kwargs)

    def on_sum(self, term, **kwargs):
        return self.on_arithmetic_operation(term, **kwargs)

    def on_product(self, term, **kwargs):
        return self.on_arithmetic_operation(term, **kwargs)

    def on_negation(self, term, **kwargs):
        return self.on_term(term, **kwargs)

    def on_next_layer(self, term, **kwargs):
        return self.on_term(term, **kwargs)
    
    def on_term_constant(self, term, **kwargs):
        return self.on_term(term, **kwargs)

    def on_term_variable(self, term, **kwargs):
        return self.on_term(term, **kwargs)

    def on_term_edge(self, term, **kwargs):
        return self.on_term_variable(term, **kwargs)

    def on_term_state(self, term, **kwargs):
        return self.on_term_variable(term, **kwargs)

    def on_term_identifier(self, term, **kwargs):
        return self.on_term_variable(term, **kwargs)

    def on_term_parameter(self, term, **kwargs):
        return self.on_term_variable(term, **kwargs)
