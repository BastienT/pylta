"""
This module defines formulas that appear in LTAs.

This includes both boolean algebra operators as well as LTL ones.
"""

import enum

import pylta.variables
import pylta.terms

class FormulaPrecedence(enum.IntEnum):
    """
    Used for placing parenthesis only when necessary when printing out
    formulas.
    """
    ATOM = 4 # Level 0 to 3 are used by terms
    UNARY = 5
    AND = 6
    OR = 7
    UNTIL = 8
    IMPLIES = 9
    EQUIV = 10

class Formula:
    """
    An abstract class for representing formulas.
    """
    precedence = None
    type_ = pylta.variables.Types.FORMULA

    def __init__(self):
        """
        Formulas is an abstract class and should not be instanciated
        directly.  Look for the children classes in
        :mod:`pylta.formulas` instead.
        """
        # The meta field should be set by the pylta.parser.Parser and
        # contains information such as the line where the term appears.
        self.meta = None
        
    def visit(self, visitor, **kwargs):
        """Return the output of the appropriate method from `visitor`.

        :param visitor: A :class:`pylta.formulas.FormulaVisitor`
            instance.

        :returns: the output of the appropiate method of `visitor`.
        """
        return visitor.on_formula(self, **kwargs)

    def get_children(self):
        """Return the child formulas of the object.

        The output only contains formulas, not terms.  This means that
        the children formulas of "x <= y" is empty.

        :returns: a list of :class:`pylta.formulas.Formula` objects.
        """
        # Should be overriden in subclasses
        raise NotImplementedError

    def __str__(self):
        """Return a string representation of the formula.

        The output can be parsed by :class:`pylta.parser.Parser`.
        """
        raise NotImplementedError

    def get_predicates(self):
        """Return the set of predicates that appear in the formula.

        :returns: A set of :class:`pylta.variables.Predicate` objects.
        """
        children_predicates = [child.get_predicates() for child in self.get_children()]
        if children_predicates:
            # set.union() requires at least one argument
            return set.union(*children_predicates)
        return set()
    
    def get_variables(self):
        """Return the set of variables that appear in the formula.

        Will fail if the term contains uninterpreted
        :class:`pylta.terms.TermIdentifier` subterms or
        :class:`pylta.formulas.FormulaIdentifier` subformulas.

        Subformulas of type :class:`pylta.formulas.NamedPredicate` are
        considered to have no variables.

        The output can contain :class:`pylta.variables.Parameter`,
        :class:`pylta.variables.State` or
        :class:`pylta.variables.Edge`.

        :returns: The set of variables that appear in the formula.
        """
        children_vars = [child.get_variables() for child in self.get_children()]
        if children_vars:
            # set.union() requires at least one argument
            return set.union(*children_vars)
        return set()
    
    def is_temporal(self):
        """
        Return `True` if the formula contains some temporal operator

        :returns: A boolean
        """
        return any([child.is_temporal() for child in self.get_children()])

class SequenceOperation(Formula):
    """
    An abstract class for representing formulas with several children
    such as And or Or.
    """
    
    def __init__(self, *children):
        """
        This is an abstract class and should not be instaciated
        directly.

        :param children: some :class:`pylta.formulas.Formula` objects
        """
        super().__init__()
        self.children = list(children)

    def get_children(self):
        return self.children
        
    def visit(self, visitor, **kwargs):
        return visitor.on_sequence_operation(self, **kwargs)

class UnaryOperation(Formula):
    """
    An abstract class for representing formulas with a single
    subformulas such as Next or Not.
    """

    precedence = FormulaPrecedence.UNARY
    
    def __init__(self, child):
        """This is an abstract class and should not be instanciated
        directly.

        :param child: a :class:`pylta.formulas.Formula` object
        """
        super().__init__()
        self.child = child

    def get_children(self):
        return [self.child]
    
    def visit(self, visitor, **kwargs):
        return visitor.on_unary_operation(self, **kwargs)

class FormulaConstant(Formula):
    """
    A formula encoding a constant formula.

    `FormulaConstant(True)` encodes the constant True while
    `FormulaConstant(False)` encodes the constant False.
    """
    precedence = FormulaPrecedence.ATOM
    
    def __init__(self, value):
        """Create a constant formula with the input boolean value

        :param value: a boolean
        """
        
        super().__init__()
        self.value = value

    def get_children(self):
        return []
        
    def visit(self, visitor, **kwargs):
        return visitor.on_formula_constant(self, **kwargs)

    def is_temporal(self):
        return False

    def __str__(self):
        if self.value:
            return "TRUE"
        else:
            return "FALSE"

class PredicateOperation(Formula):
    """
    An abstract class encoding the comparisons between two terms.
    """

    precedence = FormulaPrecedence.ATOM
    
    def __init__(self, left_term, right_term):
        """Create a formulas that compares the two input terms.

        :param left_term: a :class:`pylta.terms.Term` instance
        :param right_term: a :class:`pylta.terms.Term` instance
        """
        super().__init__()
        self.left_term = left_term
        self.right_term = right_term

    def get_children(self):
        return []
    
    def visit(self, visitor, **kwargs):
        return visitor.on_predicate_operation(self, **kwargs)

    def get_variables(self):
        return (self.left_term.get_variables()
                | self.right_term.get_variables())

    def is_temporal(self):
        return False
    
class Iff(SequenceOperation):
    """
    Encode the equivalence of arbitrarily many sub formulas
    """

    precedence = FormulaPrecedence.EQUIV

    def __init__(self, *children):
        """Create a formula encoding the equivalence of subformulas.

        `Iff(a, b, c)` encodes `a <-> b <-> c`.

        `Iff()` has no meaning and raises :exc:`ValueError`.

        :param children: some :class:`pylta.formulas.Formula` instances.

        :raises ValueError: if no arguments are provided.
        """
        if not children:
            raise IndexError("Cannot initialise with no sub-formulas")
        super().__init__(*children)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_iff(self, **kwargs)

    def __str__(self):
        str_children = []
        for i, child in enumerate(self.children):
            str_child = str(child)
            if child.precedence > self.precedence:
                # Parenthesis are needed
                str_child = "({})".format(str_child)
            str_children.append(str_child)
        return " <-> ".join(str_children)

class Implies(SequenceOperation):
    """
    Encode the implication of arbitrarily many subformulas
    """

    precedence = FormulaPrecedence.IMPLIES

    def __init__(self, *children):
        """Create a formula encoding the implications of subformulas.

        `Implies(a, b, c)` encodes `a -> (b -> c)` with right
        associativity.

        :param children: some :class:`pylta.formulas.Formula` objects.

        :raises ValueError: if no arguments are provided.
        """
        if not children:
            raise IndexError("Cannot initialise with no sub-formulas")
        super().__init__(*children)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_implies(self, **kwargs)

    def __str__(self):
        str_children = []
        for i, child in enumerate(self.children):
            str_child = str(child)
            if child.precedence >= self.precedence:
                # weaker condition because (a -> b) -> c is not
                # the same as a -> b -> c
                str_child = "({})".format(str_child)
            str_children.append(str_child)
        return " -> ".join(str_children)

    
class Until(SequenceOperation):
    """
    A class for encoding the `U` temporal operator.
    """

    precedence = FormulaPrecedence.UNTIL

    def __init__(self, *children):
        """Create a formula encoding some until of subformulas.

        `Until(a, b, c)` encodes `a U (b U c)` with right
        associativity.

        `Until()` has no meaning and raises :exc:`ValueError`.

        :param children: some :class:`pylta.formulas.Formula` objects.

        :raises ValueError: if no arguments are provided.
        """
        if not children:
            raise IndexError("Cannot initialise with no sub-formulas")
        super().__init__(*children)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_until(self, **kwargs)

    def __str__(self):
        str_children = []
        for i, child in enumerate(self.children):
            str_child = str(child)
            if child.precedence >= self.precedence:
                # We use the same conventions as implication
                str_child = "({})".format(str_child)
            str_children.append(str_child)
        return " U ".join(str_children)

    def is_temporal(self):
        return True
    
class Or(SequenceOperation):
    """
    A class for encoding disjunction of formulas.
    """
    
    precedence = FormulaPrecedence.OR

    def __init__(self, *children):
        """Create a formula encoding the disjunction of subformulas.

        `Or(a, b, c)` encodes `a | b | c`.

        `Or()` has the same semantic as `FormulaConstant(False)`

        :param children: some :class:`pylta.formulas.Formula` instances.
        """
        super().__init__(*children)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_or(self, **kwargs)

    def __str__(self):
        str_children = []
        for i, child in enumerate(self.children):
            str_child = str(child)
            if child.precedence > self.precedence:
                # Parenthesis are needed
                str_child = "({})".format(str_child)
            str_children.append(str_child)
        return " | ".join(str_children)

class And(SequenceOperation):
    """
    A class for encoding conjunction of formulas.
    """

    precedence = FormulaPrecedence.AND

    def __init__(self, *children):
        """Create a formula encoding the conjunction of subformulas.

        `And(a, b, c)` encodes `a & b & c`.

        `And()` has the same semantic as `FormulaConstant(True)`

        :param children: some :class:`pylta.formulas.Formula` instances.
        """
        super().__init__(*children)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_and(self, **kwargs)

    def __str__(self):
        str_children = []
        for i, child in enumerate(self.children):
            str_child = str(child)
            if child.precedence > self.precedence:
                # Parenthesis are needed
                str_child = "({})".format(str_child)
            str_children.append(str_child)
        return " & ".join(str_children)
    
class Not(UnaryOperation):
    """
    A class for encoding the negation of a subformula.
    """

    def __init__(self, child):
        """Encodes the formula `! child`.

        :param child: a :class:`pylta.formulas.Formula` object.
        """
        super().__init__(child)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_not(self, **kwargs)

    def __str__(self):
        str_child = str(self.child)
        if self.child.precedence > self.precedence:
            # Parenthesis are needed
            str_child = "({})".format(str_child)
        return "! {}".format(str_child)

class Next(UnaryOperation):
    """
    A class for encoding the `X` temporal operator.
    """

    def __init__(self, child):
        """Encodes the formula `X child`.

        :param child: a :class:`pylta.formulas.Formula` object.
        """
        super().__init__(child)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_next(self, **kwargs)

    def __str__(self):
        str_child = str(self.child)
        if self.child.precedence > self.precedence:
            # Parenthesis are needed
            str_child = "({})".format(str_child)
        return "X {}".format(str_child)

    def is_temporal(self):
        return True

class Finally(UnaryOperation):
    """
    A class for encoding the `F` temporal operator.
    """

    def __init__(self, child):
        """Encodes the formula `F child`.

        :param child: a :class:`pylta.formulas.Formula` object.
        """
        super().__init__(child)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_finally(self, **kwargs)

    def __str__(self):
        str_child = str(self.child)
        if self.child.precedence > self.precedence:
            # Parenthesis are needed
            str_child = "({})".format(str_child)
        return "F {}".format(str_child)

    def is_temporal(self):
        return True


class Globally(UnaryOperation):
    """
    A class for encoding the `G` temporal operator.
    """

    def __init__(self, child):
        """Encodes the formula `G child`.

        :param child: a :class:`pylta.formulas.Formula` object.
        """
        super().__init__(child)
    
    def visit(self, visitor, **kwargs):
        return visitor.on_globally(self, **kwargs)

    def __str__(self):
        str_child = str(self.child)
        if self.child.precedence > self.precedence:
            # Parenthesis are needed
            str_child = "({})".format(str_child)
        return "G {}".format(str_child)

    def is_temporal(self):
        return True

class LowerThan(PredicateOperation):
    """
    A class for encoding `term < term`.
    """
    def visit(self, visitor, **kwargs):
        return visitor.on_lower_than(self, **kwargs)

    def __str__(self):
        return "{} < {}".format(self.left_term, self.right_term)

class LowerEqual(PredicateOperation):
    """
    A class for encoding `term <= term`.
    """
    
    def visit(self, visitor, **kwargs):
        return visitor.on_lower_equal(self, **kwargs)

    def __str__(self):
        return "{} <= {}".format(self.left_term, self.right_term)

class Equal(PredicateOperation):
    """
    A class for encoding `term == term`.
    """
    
    def visit(self, visitor, **kwargs):
        return visitor.on_equal(self, **kwargs)

    def __str__(self):
        return "{} == {}".format(self.left_term, self.right_term)

class NotEqual(PredicateOperation):
    """
    A class for encoding `term != term`.
    """
    
    def visit(self, visitor, **kwargs):
        return visitor.on_not_equal(self, **kwargs)

    def __str__(self):
        return "{} != {}".format(self.left_term, self.right_term)

class GreaterEqual(PredicateOperation):
    """
    A class for encoding `term >= term`.
    """
    
    def visit(self, visitor, **kwargs):
        return visitor.on_greater_equal(self, **kwargs)

    def __str__(self):
        return "{} >= {}".format(self.left_term, self.right_term)

class GreaterThan(PredicateOperation):
    """
    A class for encoding `term > term`.
    """
    
    def visit(self, visitor, **kwargs):
        return visitor.on_greater_than(self, **kwargs)

    def __str__(self):
        return "{} > {}".format(self.left_term, self.right_term)

class FormulaIdentifier(Formula):
    """
    Encode an identifier for another formula.

    Theses are replaced by their corresponding formulas during
    interpretation.

    Some methods such as get_variables or is_temporal fail on these
    formulas.
    """

    precedence = FormulaPrecedence.ATOM
    
    def __init__(self, ident):
        """Create a formula with the given identifier

        :param ident: a :class:`pylta.variables.Identifier` object
        """
        super().__init__()
        self.ident = ident

    def visit(self, visitor, **kwargs):
        return visitor.on_formula_identifier(self, **kwargs)
    
    def __str__(self):
        return str(self.ident)

    def get_children(self):
        return []

    def get_predicates(self):
        raise NotImplementedError
    
    def get_variables(self):
        raise NotImplementedError
    
    def is_temporal(self):
        raise NotImplementedError

class NamedPredicate(Formula):
    """
    A formula encoding the value of a defined predicate.

    These formulas differ from FormulaIdentifier because they are not
    replaced at interpretation time.  but rather the valuation of the
    predicate is linked with the valuation of the corresponding
    formula when it is defined.
    """

    precedence = FormulaPrecedence.ATOM
    
    def __init__(self, predicate):
        """Create a formula representing the given predicate

        :param predicate: a :class:`pylta.variables.Predicate` object
        """
        super().__init__()
        self.predicate = predicate

    def visit(self, visitor, **kwargs):
        return visitor.on_named_predicate(self, **kwargs)
    
    def __str__(self):
        return str(self.predicate)

    def get_children(self):
        return []

    def get_predicates(self):
        return set([self.predicate])
    
    def get_variables(self):
        return set()
    
    def is_temporal(self):
        return False

class GlobalVar(Formula):
    precedence = FormulaPrecedence.ATOM
    
    def __init__(self, name):
        """Create a formula representing a global boolean variable.

        :param name: a :class:`pylta.variables.BooleanVar` object
        """
        super().__init__()
        self.name = name

    def visit(self, visitor, **kwargs):
        return visitor.on_global_var(self, **kwargs)
    
    def __str__(self):
        return str(self.name)

    def get_children(self):
        return []

    def get_predicates(self):
        return set()
    
    def get_variables(self):
        return set()
    
    def is_temporal(self):
        return False

    
class FormulaVisitor:
    """The visitor for Formula object.

    Instances of this class have one method for each type of formulas
    that can be called with their :meth:`Formula.visit()` method.
    This gives a nice way of performing pattern matching.

    The default methods defined here are meant to be overloaded.  By
    default, methods for a subclass call the method of the parent
    class, meaning for example that if all
    :class:`pylta.formulas.SequenceOperations` should be handled the
    same, it should be enough to overload
    :meth:`pylta.formulas.FormulaVisitor.on_sequence_operation()` and
    leave :meth:`pylta.formulas.FormulaVisitor.on_iff()`,
    :meth:`pylta.formulas.FormulaVisitor.on_implies()`...  unchanged.

    See :class:`pylta.interpreter.Interpreter` or
    :class:`pylta.translator.FormulaToSMT` for some examples of usage.
    """
    def on_formula(self, formula, **kwargs):
        raise NotImplementedError

    def on_sequence_operation(self, formula, **kwargs):
        return self.on_formula(formula, **kwargs)

    def on_unary_operation(self, formula, **kwargs):
        return self.on_formula(formula, **kwargs)

    def on_formula_constant(self, formula, **kwargs):
        return self.on_formula(formula, **kwargs)

    def on_predicate_operation(self, formula, **kwargs):
        return self.on_formula(formula, **kwargs)

    def on_iff(self, formula, **kwargs):
        return self.on_sequence_operation(formula, **kwargs)

    def on_implies(self, formula, **kwargs):
        return self.on_sequence_operation(formula, **kwargs)

    def on_until(self, formula, **kwargs):
        return self.on_sequence_operation(formula, **kwargs)

    def on_or(self, formula, **kwargs):
        return self.on_sequence_operation(formula, **kwargs)

    def on_and(self, formula, **kwargs):
        return self.on_sequence_operation(formula, **kwargs)

    def on_not(self, formula, **kwargs):
        return self.on_unary_operation(formula, **kwargs)

    def on_next(self, formula, **kwargs):
        return self.on_unary_operation(formula, **kwargs)

    def on_finally(self, formula, **kwargs):
        return self.on_unary_operation(formula, **kwargs)

    def on_globally(self, formula, **kwargs):
        return self.on_unary_operation(formula, **kwargs)

    def on_lower_than(self, formula, **kwargs):
        return self.on_predicate_operation(formula, **kwargs)

    def on_lower_equal(self, formula, **kwargs):
        return self.on_predicate_operation(formula, **kwargs)

    def on_equal(self, formula, **kwargs):
        return self.on_predicate_operation(formula, **kwargs)

    def on_not_equal(self, formula, **kwargs):
        return self.on_predicate_operation(formula, **kwargs)

    def on_greater_equal(self, formula, **kwargs):
        return self.on_predicate_operation(formula, **kwargs)

    def on_greater_than(self, formula, **kwargs):
        return self.on_predicate_operation(formula, **kwargs)

    def on_formula_identifier(self, formula, **kwargs):
        return self.on_formula(formula, **kwargs)

    def on_global_var(self, formula, **kwargs):
        return self.on_formula(formula, **kwargs)
    
    def on_named_predicate(self, formula, **kwargs):
        return self.on_formula(formula, **kwargs)
