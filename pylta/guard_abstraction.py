import mathsat
import pysmt.shortcuts as smt

import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.translator
import pylta.paths

class GuardAbstraction:
    def __init__(self, lta, start, end):
        self.lta = lta
        self.predicates = pylta.predicates.PredicateManager(lta)
        self.initialize_solver()
        self.start = start
        self.end = start
        self.ensure_end_at_least(end)

    @property
    def length(self):
        return self.end - self.start

    def initialize_solver(self):
        self.solver = smt.UnsatCoreSolver(name="z3")
        
    def activation_var(self, index):
        """Return a pylta.formulas.GlobalVar that when set to True
        activates all the constraints of that layer

        :param index: An index of the guard abstraction

        :returns: a pylta.formulas.GlobalVar
        """
        name = "_A{}".format(index)
        bool_var = pylta.variables.BooleanVar(name)
        return pylta.formulas.GlobalVar(bool_var)

    def add_assertion(self, formula, index=None):
        smt_formula = pylta.translator.to_smt(formula, index)
        self.solver.add_assertion(smt_formula)
    
    def add_predicate(self, predicate, formula):
        layer = predicate.layer
        self.predicates[predicate] = formula
        layer_index = self.lta.get_layer_index(layer)
        for index in range(layer_index, self.end):
            if layer == self.lta.get_layer_at(index):
                self.setup_predicate(index, predicate)

    def push(self):
        self.solver.push()

    def pop(self):
        self.solver.pop()
            
    def setup_predicate(self, index, predicate):
        act_var = self.activation_var(index)
        constraint = pylta.formulas.Implies(
            act_var,
            pylta.formulas.Iff(
                pylta.formulas.NamedPredicate(predicate),
                self.predicates[predicate]
            )
        )
        self.add_assertion(constraint, index)
        
    def setup_parameters(self):
        for constraint in self.lta.parameter_constraints():
            self.add_assertion(constraint, None)

    def setup_index(self, index):
        layer = self.lta.get_layer_at(index)
        act_var = self.activation_var(index)
        for constraint in self.lta.all_but_in_constraints(layer):
            new_constr = pylta.formulas.Implies(act_var, constraint)
            self.add_assertion(new_constr, index)
        for pred in self.predicates.get(layer):
            self.setup_predicate(index, pred)

    def setup_link(self, index):
        src_layer = self.lta.get_layer_at(index)
        src_act_var = self.activation_var(index)
        dest_act_var = self.activation_var(index+1)
        for constraint in self.lta.in_constraints(src_layer):
            new_constr = pylta.formulas.Implies(src_act_var, dest_act_var, constraint)
            self.add_assertion(new_constr, index)
        
    def setup_next(self):
        if self.length == 0:
            self.setup_parameters()
            self.setup_index(self.start)
        else:
            index = self.end
            self.setup_index(index)
            self.setup_link(index - 1)
        self.end += 1

    def ensure_end_at_least(self, new_end):
        while self.end < new_end:
            self.setup_next()

    def setup_path(self, path, unroll=0):
        assert self.start <= path.start
        # We should not use ensure_length_at_least here because it
        # will get messed up with push and pop
        assert path.end + unroll <= self.end
        for index in path.iter_indices(unroll):
            act_var = self.activation_var(index)
            self.add_assertion(act_var, None)
        for index, pred, value in path.iter_all(unroll):
            formula = pylta.formulas.NamedPredicate(pred)
            if not value:
                formula = pylta.formulas.Not(formula)
            self.add_assertion(formula, index)

    def extract_concrete_path(self, start, end, loop_start=None):
        c_path = self.lta.concrete_path(start, end, loop_start)
        smt_model = self.solver.get_model()
        for p in self.lta.get_parameters():
            formula_p = pylta.terms.TermParameter(p)
            smt_p = pylta.translator.to_smt(formula_p, None)
            val_p = smt_model.get_py_value(smt_p)
            c_path.set_param_value(p, val_p)
        for index in c_path.iter_indices():
            layer = c_path[index].layer
            for s in self.lta.get_states(layer):
                formula_s = pylta.terms.TermState(s)
                smt_s = pylta.translator.to_smt(formula_s, index)
                val_s = smt_model.get_py_value(smt_s)
                c_path[index].set_state_value(s, val_s)
            for e in self.lta.get_edges(layer):
                formula_e = pylta.terms.TermEdge(e)
                smt_e = pylta.translator.to_smt(formula_e, index)
                val_e = smt_model.get_py_value(smt_e)
                c_path[index].set_edge_value(e, val_e)
        return c_path

    def extract_abstract_path(self, start, end, loop_start=None):
        path = self.predicates.abstract_path(start, end, loop_start)
        model = self.solver.get_model()
        for index in range(start, end):
            layer = self.lta.get_layer_at(index)
            for pred in self.predicates.get(layer):
                formula_pred = pylta.formulas.NamedPredicate(pred)
                smt_pred = pylta.translator.to_smt(formula_pred, index)
                path[index][pred] = model.get_py_value(smt_pred)
        return path

    def extract_unsat_path(self, start, end):
        path = self.predicates.abstract_path(start, end)
        core = self.solver.get_unsat_core()
        for index in path.iter_indices():
            layer = path[index].layer
            for pred in self.predicates.get(layer):
                formula_pred = pylta.formulas.NamedPredicate(pred)
                smt_pred = pylta.translator.to_smt(formula_pred, index)
                if smt_pred in core:
                    path[index][pred] = True
                else:
                    not_pred = pylta.formulas.Not(formula_pred)
                    smt_not_pred = pylta.translator.to_smt(not_pred, index)
                    if smt_not_pred in core:
                            path[index][pred] = False
        return path

    def complete_valuation(self, path):
        # TODO: modify path instead of making a copy in order to uniformise code
        self.push()
        self.setup_path(path)
        sat = self.solver.solve()
        if sat:
            res_path = self.extract_abstract_path(path.start, path.end)
        else:
            res_path = self.extract_unsat_path(path.start, path.end)
        self.pop()
        return (sat, res_path)

    def concretise(self, path):
        self.push()
        self.setup_path(path)
        if not self.solver.solve():
            return None
        res = self.extract_concrete_path(path.start, path.end, path.loop_start)
        self.pop()
        return res

class PathInterpolator(GuardAbstraction):
    def __init__(self, *args, **kwargs):
        self.itp_groups = []
        super().__init__(*args, **kwargs)
    
    def initialize_solver(self):
        self.solver = smt.UnsatCoreSolver(
            name="msat",
            solver_options={"interpolation": "true"}
        )

    def setup_next(self):
        env = self.solver.msat_env()
        self.itp_groups.append(mathsat.msat_create_itp_group(env))
        super().setup_next()
        
    def add_assertion(self, formula, index):
        if index is None:
            index = self.start
        group = self.itp_groups[index]
        env = self.solver.msat_env()
        _ = mathsat.msat_set_itp_group(env, group)
        super().add_assertion(formula, index)

    def interpolate(self, path):
        unroll = 0
        if path.has_loop():
            unroll = 1
        self.ensure_end_at_least(path.end + unroll)
        self.push()
        self.setup_path(path, unroll)
        
        if self.solver.solve():
            # A concrete counter example was found, but
            # without the concrete equality at the loop
            if path.has_loop():
                layer = path[path.loop_start].layer
                for state in self.lta.get_states(layer):
                    term_state = pylta.terms.TermState(state)
                    term_next_state = pylta.terms.NextLayer(
                        term_state,
                        offset=(path.end - path.loop_start)
                    )
                    formula = pylta.formulas.Equal(term_state, term_next_state)
                    self.add_assertion(formula, path.loop_start)
                if not self.solver.solve():
                    # The loop could not be concretised
                    # TODO: try to find longer prefixes
                    self.pop()
                    return None
            res = (True, self.extract_concrete_path(path.start,
                                                    path.length,
                                                    path.loop_start))
            self.pop()
            return res
        #No concrete counter examples could be found, interpolation begins
        interpolants = pylta.utils.Lasso(path.start + 1)
        env = self.solver.msat_env()
        for index in path.iter_indices(unroll-1):
            groupA = self.itp_groups[path.start:index+1]
            msat_itp = mathsat.msat_get_interpolant(env, self.itp_groups[path.start:index+1])
            smt_itp = self.solver.converter.back(msat_itp)
            itp = pylta.translator.from_smt(smt_itp, index+1)
            interpolants.append(itp)
        self.pop()
        return (False, interpolants)
            
    def extract_useful_interpolants(self, path, interpolants):
        #We remove the predicates in the beginning that cannot be set to False
        start = None
        index = interpolants.start
        while start is None:
            itp = interpolants[index]
            new_path = path.get_slice(index, index+1)
            self.push()
            self.setup_path(new_path)
            self.add_assertion(pylta.formulas.Not(itp), index)
            sat = self.solver.solve()
            self.pop()
            if sat:
                start = index
            index += 1
        #We only keep predicates that can be set to True
        end = None
        while end is None:
            itp = interpolants[index]
            new_path = path.get_slice(index, index+1)
            self.push()
            self.setup_path(new_path)
            self.add_assertion(itp, index)
            sat = self.solver.solve()
            self.pop()
            if not sat:
                end = index
            index += 1
        return interpolants.get_slice(start, end)
            
