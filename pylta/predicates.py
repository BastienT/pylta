from dataclasses import dataclass
import collections

import pylta.variables

class PredicateManager:
    """A class for managing :class:`pylta.variables.Predicate`
    objects.

    It handles the link between the predicates and their formulas, as
    well as organise them in layers and generate valuations.
    """
    def __init__(self, lta):
        """Generate an empty set of predicate for the input LTA.

        Will not work properly if Layers are added to the LTA after
        the PredicateManager is initialised.

        :param lta: a :class:`pylta.lta.LTA` object.
        """
        self.layers = lta.layers
        self.content = {}    #A [layer -> [predicate -> formula]] mapping
        self.next_auto_pred = {}    #A [layer -> integer] mapping for generating new predicate names

    def __iter__(self):
        """
        Iterate over all the :class:`pylta.variables.Predicate` in the
        object.
        """
        for pred_layer in self.content.values():
            yield from pred_layer.keys()
        
    def __contains__(self, pred):
        if pred.layer not in self.content:
            return False
        return pred in self.content[pred.layer]

    def get(self, layer):
        """Iterate over the :class:`pylta.variables.Predicate` in
        given layer.

        :param layer: A :class:`pylta.variables.Layer` object

        :returns: a :class:`pylta.variables.Predicate` iterator
        """
        if layer not in self.content:
            return []
        yield from self.content[layer].keys()

    def items(self):
        """Iterate over the (predicate, formula) tuples.
        """
        for pred_layer in self.content.values():
            yield from pred_layer.items()
        
    def __getitem__(self, pred):
        return self.content[pred.layer][pred]

    def __setitem__(self, pred, formula):
        if pred.layer not in self.content:
            self.content[pred.layer] = {}
        self.content[pred.layer][pred] = formula

    def get_unused_pred(self, layer):
        """Return an unused Predicate at the input layer.

        Useful for abstraction refinement.

        :param layer: A :class:`pylta.variables.Layer` object

        :returns: A :class:`pylta.variables.Predicate` object not
                  belonging to the layer.
        """
        number = self.next_auto_pred.get(layer, 0)
        pred = pylta.variables.Predicate(layer, "A{}".format(number))
        number += 1
        self.next_auto_pred[layer] = number
        while pred in self:
            pred = pylta.variables.Predicate(layer, "A{}".format(number))
            number += 1
            self.next_auto_pred[layer] = number
        return pred

    def abstract_valuation(self, index):
        """Return a valuation of the predicates of the layer at the
        input index.

        :param index: An integer

        :returns: A :class:`pylta.paths.Valuation` object
        """
        layer = self.layers[index]
        return pylta.paths.Valuation(layer)

    def abstract_path(self, start, end, loop_start=None):
        """Return a path starting at index start and ending before
        end.

        Lasso paths can be built by specifying a loop_start.  We must
        have start <= end and if loop_start is not None, start <=
        loop_start < end.

        :param start: An integer
        :param end: An integer
        :param loop_start: Either None or an integer.

        :returns: A :class:`pylta.paths.Path` object.
        """
        
        result = pylta.paths.Path(start)
        for index in range(start, end):
            result.append(self.abstract_valuation(index))
        result.loop_start = loop_start
        return result

    def __str__(self):
        lines = ["PREDICATES:"]
        for pred, formula in self.items():
            res = " {}: {}".format(pred, formula)
            lines.append(res)
        return "\n".join(lines)

