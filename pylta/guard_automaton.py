from dataclasses import dataclass

import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.translator
import pylta.predicates
import pylta.lta
import pylta.paths
import pylta.dd_approx
import pylta.guard_abstraction


class GuardAutomaton:
    def __init__(self, lta, guard_abstraction=None):
        n = len(lta.get_layers())
        if guard_abstraction is None:
            self.ga = pylta.guard_abstraction.GuardAbstraction(lta, 0, n+1)
        else:
            self.ga = guard_abstraction
        self.approx_states = [
            pylta.dd_approx.DdApproximation(self.predicates, i, i+1)
            for i in range(n)
        ]
        self.approx_edges = [
            pylta.dd_approx.DdApproximation(self.predicates, i, i+2)
            for i in range(n)
        ]

    @property
    def predicates(self):
        return self.ga.predicates
        
    @property
    def lta(self):
        return self.ga.lta
    
    def add_predicate(self, predicate, formula):
        self.ga.add_predicate(predicate, formula)
        for dd_state in self.approx_states:
            dd_state.register_pred(predicate)
        for dd_edge in self.approx_edges:
            dd_edge.register_pred(predicate)
            
    def _complete_states(self, partial_path):
        index = partial_path.start
        assert partial_path.length == 1
        new_path = partial_path.copy()
        while self.approx_states[index].unknown_path(new_path):
            sat, completed_path = self.ga.complete_valuation(new_path)
            if sat:
                self.approx_states[index].add_path(completed_path)
            else:
                self.approx_states[index].remove_path(completed_path)
            new_path = partial_path.copy() #Gettting back to a clean path
            
    def _complete_edges(self, partial_path):
        index = partial_path.start
        assert partial_path.length == 2
        new_path = partial_path.copy()
        while self.approx_edges[index].unknown_path(new_path):
            sat, completed_path = self.ga.complete_valuation(new_path)
            if sat:
                self.approx_edges[index].add_path(completed_path)
            else:
                self.approx_edges[index].remove_path(completed_path)
            new_path = partial_path.copy() #Getting back to a clean path
            
    def get_states(self, layer, partial_val=None):
        if partial_val is None:
            partial_val = {}
        index = self.lta.get_layer_index(layer)
        path = self.predicates.abstract_path(index, index+1)
        for pred, val in partial_val.items():
            path[index][pred] = val
        self._complete_states(path)
        for res_path in self.approx_states[index].enum_lower(path):
            yield res_path[index].freeze()

    def initial_states(self, partial_val=None):
        layer = self.lta.get_layer_at(0)
        yield from self.get_states(layer, partial_val)

    def get_successors(self, state, partial_val=None):
        index = self.lta.get_layer_index(state.layer)
        next_layer = self.lta.next_layer(state.layer)
        if partial_val is None:
            partial_val = {}
        path = self.predicates.abstract_path(index, index+2)
        for pred, val in state.items():
            path[index][pred] = val
        for pred, val in partial_val.items():
            path[index+1][pred] = val
        self._complete_edges(path)
        for res_path in self.approx_edges[index].enum_lower(path):
            yield res_path[index+1].freeze()


