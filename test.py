import pysmt
import pysmt.shortcuts as smt
from pysmt.typing import REAL as INT #UGLY

r0 = smt.Symbol("r0", INT)
r1 = smt.Symbol("r1", INT)
n = smt.Symbol("n", INT)
t = smt.Symbol("t", INT)
f = smt.Symbol("f", INT)
rec0 = smt.Symbol("r'0", INT)
rec1 = smt.Symbol("r'1", INT)
f0 = smt.Symbol("f0", INT)
f1 = smt.Symbol("f1", INT)

formulas = []
formulas.append(r0 >= 0)
formulas.append(r1 >= 0)
formulas.append(n >= 0)
formulas.append(t >= 0)
formulas.append(f >= 0)
formulas.append(rec0 >= 0)
formulas.append(rec1 >= 0)
formulas.append(f0 >= 0)
formulas.append(f1 >= 0)
formulas.append(rec0 <= r0)
formulas.append(rec1 <= r1)
formulas.append(f0 + f1 <= f)
formulas.append(rec0 + rec1 + f0 + f1 >= n - t)
formulas.append(2*(rec0 + f0) < n + t)
formulas.append(2*(rec1 + f1) < n + t)

form = smt.Exists([rec0, rec1, f0, f1], smt.And(*formulas))
form_elim = smt.simplify(smt.qelim(form))
print(str(form_elim))
