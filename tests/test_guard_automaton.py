import pytest

import pylta.exceptions
import pylta.parser
import pylta.commands
import pylta.interpreter
import pylta.guard_abstraction
import pylta.guard_automaton

def test_rel_broadcast():
    input_str = ("PARAMETERS: n, t, f\n"
                 "PARAMETER_RELATION: 2*t < n & f <= t & t > 0\n"
                 "LAYERS: init, inter, acc\n"
                 "STATES: init.0, init.1, inter.s, acc.s\n"
                 "CASE init.0:\n"
                 "  IF init.1 + f > t THEN inter.s\n"
                 "CASE init.1:\n"
                 "  IF TRUE THEN inter.s\n"
                 "CASE inter.s:\n"
                 "  IF inter.s + f >= n - t THEN acc.s\n")
    program = pylta.parser.parse(input_str)
    interp = pylta.interpreter.Interpreter()
    interp.run(program)

    n = pylta.variables.Parameter("n")
    assert interp.lta.is_parameter(n)
    t = pylta.variables.Parameter("t")
    assert interp.lta.is_parameter(t)
    f = pylta.variables.Parameter("f")
    assert interp.lta.is_parameter(f)
    
    init = pylta.variables.Layer("init")
    assert interp.lta.is_layer(init)
    init0 = pylta.variables.State(init, "0")
    assert interp.lta.is_state(init0)
    init1 = pylta.variables.State(init, "1")
    assert interp.lta.is_state(init1)

    inter = pylta.variables.Layer("inter")
    assert interp.lta.is_layer(inter)
    inter_s = pylta.variables.State(inter, "s")
    assert interp.lta.is_state(inter_s)
    
    acc = pylta.variables.Layer("acc")
    assert interp.lta.is_layer(acc)
    acc_s = pylta.variables.State(acc, "s")
    assert interp.lta.is_state(acc_s)

    ga = pylta.guard_automaton.GuardAutomaton(interp.lta)
    
    p_init = pylta.variables.Predicate(init, "initial")
    f_init = pylta.formulas.Equal(
        pylta.terms.Sum(
            pylta.terms.TermState(init0),
            pylta.terms.TermState(init1),
            pylta.terms.TermParameter(f)
        ),
        pylta.terms.TermParameter(n)
        )
    ga.add_predicate(p_init, f_init)

    p_one1 = pylta.variables.Predicate(init, "one1")
    f_one1 = pylta.formulas.GreaterThan(
        pylta.terms.TermState(init1),
        pylta.terms.TermConstant(0)
    )
    ga.add_predicate(p_one1, f_one1)

    p_inter = pylta.variables.Predicate(inter, "enough")
    f_inter = pylta.formulas.GreaterEqual(
        pylta.terms.Sum(
            pylta.terms.TermState(inter_s),
            pylta.terms.TermParameter(f)
        ),
        pylta.terms.Sum(
            pylta.terms.TermParameter(n),
            pylta.terms.Product(
                pylta.terms.TermConstant(-1),
                pylta.terms.TermParameter(t)
            )
        )
    )
    ga.add_predicate(p_inter, f_inter)

    p_acc = pylta.variables.Predicate(acc, "one")
    f_acc = pylta.formulas.GreaterThan(
        pylta.terms.TermState(acc_s),
        pylta.terms.TermConstant(0)
    )
    ga.add_predicate(p_acc, f_acc)

    assert p_init in ga.predicates
    assert p_one1 in ga.predicates
    assert p_inter in ga.predicates
    assert p_acc in ga.predicates

    sanity_check = False
    for state in ga.initial_states({p_init: True}):
        assert state.layer == init
        assert len(state) == 2
        assert p_init in state
        assert p_one1 in state
        assert state[p_init]
        assert p_one1 in state
        for successor in ga.get_successors(state):
            assert successor.layer == inter
            assert len(successor) == 1
            assert p_inter in successor
            assert state[p_one1] or not successor[p_inter]
            for last in ga.get_successors(successor):
                assert last.layer == acc
                assert len(last) == 1
                assert p_acc in last
                path = pylta.paths.Path(0)
                path.append(state)
                path.append(successor)
                path.append(last)
                print(path)
                assert state[p_one1] or not last[p_acc]
                sanity_check = sanity_check or last[p_acc]
    assert sanity_check
