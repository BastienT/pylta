import pytest
import pylta.variables

def test_parameter():
    param = pylta.variables.Parameter("name")
    index = 42
    indexed_name = param.indexed_name(index)
    param2, index2 = pylta.variables.from_indexed_name(indexed_name)
    assert param == param2
    assert index2 is None

def test_state():
    layer = pylta.variables.Layer("a")
    state = pylta.variables.State(layer, "2")
    index = 42
    indexed_name = state.indexed_name(index)
    state2, index2 = pylta.variables.from_indexed_name(indexed_name)
    assert state == state2
    assert index == index2

def test_edge():
    src_layer = pylta.variables.Layer("a")
    src = pylta.variables.State(src_layer, "2")
    dest_layer = pylta.variables.Layer("b28")
    dest = pylta.variables.State(dest_layer, "x")
    edge = pylta.variables.Edge(src, dest)
    index = 27
    indexed_name = edge.indexed_name(index)
    edge2, index2 = pylta.variables.from_indexed_name(indexed_name)
    assert edge == edge2
    assert index == index2

def test_predicate():
    layer = pylta.variables.Layer("a")
    pred = pylta.variables.Predicate(layer, "2")
    index = 42
    indexed_name = pred.indexed_name(index)
    pred2, index2 = pylta.variables.from_indexed_name(indexed_name)
    assert pred == pred2
    assert index == index2

def test_layer_predicate():
    layer = pylta.variables.Layer("a")
    pred = pylta.variables.Predicate.from_layer(layer)
    index = 42
    indexed_name = pred.indexed_name(index)
    pred2, index2 = pylta.variables.from_indexed_name(indexed_name)
    assert pred == pred2
    assert index == index2

def test_global_var():
    bool_var = pylta.variables.BooleanVar("_A2")
    index = 13
    indexed_name = bool_var.indexed_name(index)
    bool_var2, index2 = pylta.variables.from_indexed_name(indexed_name)
    assert bool_var == bool_var2
    assert index2 is None
