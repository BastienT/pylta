import pytest

import pylta.exceptions
import pylta.parser
import pylta.commands
import pylta.interpreter

def test_declare_parameter():
    input_str = "PARAMETERS: n, t, f00\n"
    commands = pylta.parser.parse(input_str)
    interp = pylta.interpreter.Interpreter()
    interp.run(commands)
    assert interp.symbol_table["n"] == pylta.variables.Parameter(name="n")
    assert interp.symbol_table["t"] == pylta.variables.Parameter(name="t")
    assert interp.symbol_table["f00"] == pylta.variables.Parameter(name="f00")

def test_declare_parameter2():
    input_str = "PARAMETERS: n, n, f00\n"
    commands = pylta.parser.parse(input_str)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolRedefinition, match=r".* n .*"):
        interp.run(commands)
    
def test_declare_parameter_relation():
    decl_param = "PARAMETERS: n, t, f00\n"
    relation = "PARAMETER_RELATION: 2 * t < n & t > 0 & f00 <= t\n"
    commands = pylta.parser.parse(decl_param + relation)
    interp = pylta.interpreter.Interpreter()
    interp.run(commands)
    assert len(interp.lta.parameter_relations) == 1

def test_declare_parameter_relation2():
    decl_param = "PARAMETERS: n, t, f00\n"
    relation = "PARAMETER_RELATION: 2 * typo < n & t > 0 & f00 <= t\n"
    commands = pylta.parser.parse(decl_param + relation)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolUndefined, match=r".* typo .*"):
        interp.run(commands)

def test_declare_parameter_relation3():
    decl_param = "PARAMETERS: n, t, f00\n"
    relation = "PARAMETER_RELATION: 2 * t < n & F t > 0 & f00 <= t\n"
    commands = pylta.parser.parse(decl_param + relation)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.UnexpectedTemporalOperator):
        interp.run(commands)

def test_declare_layers():
    decl_layers = "LAYERS: a, b, c\n"
    commands = pylta.parser.parse(decl_layers)
    interp = pylta.interpreter.Interpreter()
    interp.run(commands)
    assert len(interp.lta.layers) == 3
    a = pylta.variables.Layer("a")
    b = pylta.variables.Layer("b")
    c = pylta.variables.Layer("c")
    assert interp.lta.is_layer(a)
    assert interp.lta.is_layer(b)
    assert interp.lta.is_layer(c)
    assert interp.lta.next_layer(a) == b
    assert interp.lta.next_layer(b) == c
    assert interp.lta.next_layer(c) == a

def test_declare_layers2():
    decl_param = "PARAMETERS: param_and_layer, t, f00\n"
    decl_layers = "LAYERS: a, param_and_layer, c\n"
    commands = pylta.parser.parse(decl_param + decl_layers)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolRedefinition, match=".* param_and_layer .*"):
        interp.run(commands)

def test_declare_states():
    decl_layers = "LAYERS: a, b, c\n"
    decl_states = "STATES: a.x, a.y, b.x\n"
    commands = pylta.parser.parse(decl_layers + decl_states)
    interp = pylta.interpreter.Interpreter()
    interp.run(commands)
    a = pylta.variables.Layer("a")
    b = pylta.variables.Layer("b")
    c = pylta.variables.Layer("c")
    assert len(interp.lta.guards[a]) == 2
    assert len(interp.lta.guards[b]) == 1
    assert len(interp.lta.guards[c]) == 0
    ax = pylta.variables.State(layer=a, name="x")
    ay = pylta.variables.State(layer=a, name="y")
    bx = pylta.variables.State(layer=b, name="x")
    assert interp.lta.is_state(ax)
    assert interp.lta.is_state(ay)
    assert interp.lta.is_state(bx)

def test_declare_states2():
    decl_layers = "LAYERS: a\n"
    decl_states = "STATES: a.x, a.y, non_existing_layer.x\n"
    commands = pylta.parser.parse(decl_layers + decl_states)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolUndefined, match=".* non_existing_layer .*"):
        interp.run(commands)

def test_declare_states3():
    decl_layers = "LAYERS: a\n"
    decl_states = "STATES: a.same_name, a.same_name\n"
    commands = pylta.parser.parse(decl_layers + decl_states)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolRedefinition, match=".* a.same_name .*"):
        interp.run(commands)

def test_rule():
    decl_param = "PARAMETERS: n, t, f00\n"
    decl_layers = "LAYERS: a, b, c\n"
    decl_states = "STATES: a.x, a.y, b.x, b.y, c.x\n"
    decl_rules = (
        "CASE a.x:\n"
        "  IF TRUE THEN b.x\n"
        "CASE a.y:\n"
        "  IF a.x + n >= 2 * (t - f00) THEN b.y\n"
        "CASE b.x:\n"
        "  IF 2 * b.y == n THEN c.x\n"
        "CASE c.x:\n"
        "  IF c.x + 2 <= n THEN a.x\n"
        "  IF 3 * c.x - 4 > 6 - t THEN a.y\n"
    )
    commands = pylta.parser.parse(decl_param + decl_layers + decl_states + decl_rules)
    interp = pylta.interpreter.Interpreter()
    interp.run(commands)
    a = pylta.variables.Layer("a")
    b = pylta.variables.Layer("b")
    c = pylta.variables.Layer("c")
    ax = pylta.variables.State(layer=a, name="x")
    ay = pylta.variables.State(layer=a, name="y")
    bx = pylta.variables.State(layer=b, name="x")
    by = pylta.variables.State(layer=b, name="y")
    cx = pylta.variables.State(layer=c, name="x")
    assert str(interp.lta.guards[a][ax][bx]) == "TRUE"
    assert str(interp.lta.guards[a][ay][by]) == "a.x + n >= 2 * (t - f00)"
    assert str(interp.lta.guards[b][bx][cx]) == "2 * b.y == n"
    assert str(interp.lta.guards[c][cx][ax]) == "c.x + 2 <= n"
    assert str(interp.lta.guards[c][cx][ay]) == "3 * c.x - 4 > 6 - t"

def test_rule2():
    decl_layers = "LAYERS: a, b\n"
    decl_states = "STATES: a.x, b.x, b.y\n"
    decl_rules = (
        "CASE a.y:\n"
        "  IF TRUE THEN b.y\n"
    )
    commands = pylta.parser.parse(decl_layers + decl_states + decl_rules)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolUndefined, match=r".* a.y .*"):
        interp.run(commands)

def test_rule3():
    decl_layers = "LAYERS: a, b\n"
    decl_states = "STATES: a.x, b.x, b.y\n"
    decl_rules = (
        "CASE a.x:\n"
        "  IF TRUE THEN a.x\n"
    )
    commands = pylta.parser.parse(decl_layers + decl_states + decl_rules)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.LayeringError, match=r".* a.x .* b .*"):
        interp.run(commands)

def test_rule4():
    decl_layers = "LAYERS: a, b\n"
    decl_states = "STATES: a.x, b.x, b.y\n"
    decl_rules = (
        "CASE b.x:\n"
        "  IF a.x > 2 THEN a.x\n"
    )
    commands = pylta.parser.parse(decl_layers + decl_states + decl_rules)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.LayeringError, match=r".* a.x .* b .*"):
        interp.run(commands)
        
def test_bind_term():
    decl_param = "PARAMETERS: n, t, f00\n"
    decl_term = "TERM test: n - 2 * t\n"
    commands = pylta.parser.parse(decl_param + decl_term)
    interp = pylta.interpreter.Interpreter()
    interp.run(commands)
    assert interp.symbol_table["test"].type_ is pylta.variables.Types.TERM
    assert str(interp.symbol_table["test"]) == "n - 2 * t"

def test_bind_term2():
    decl_param = "PARAMETERS: n, t, f00\n"
    decl_term = "TERM n: n - 2 * t\n"
    commands = pylta.parser.parse(decl_param + decl_term)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolRedefinition, match=r".* n .*"):
        interp.run(commands)
        
def test_bind_formula():
    decl_param = "PARAMETERS: n, t, f00\n"
    decl_term = "TERM test: n - 2 * t\n"
    decl_formula = "FORMULA test2: X test != f00\n"
    commands = pylta.parser.parse(decl_param + decl_term + decl_formula)
    interp = pylta.interpreter.Interpreter()
    interp.run(commands)
    assert interp.symbol_table["test2"].type_ is pylta.variables.Types.FORMULA
    assert str(interp.symbol_table["test2"]) == "X n - 2 * t != f00"

def test_bind_formula2():
    decl_param = "PARAMETERS: n, t, f00\n"
    decl_term = "TERM test: n - 2 * t\n"
    decl_formula = "FORMULA test: X test != f00\n"
    commands = pylta.parser.parse(decl_param + decl_term + decl_formula)
    interp = pylta.interpreter.Interpreter()
    with pytest.raises(pylta.exceptions.SymbolRedefinition, match=r".* test .*"):
        interp.run(commands)

