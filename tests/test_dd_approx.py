import pylta.variables
import pylta.formulas
import pylta.lta
import pylta.predicates
import pylta.paths
import pylta.dd_approx

def test_dd():
    #setup
    ttrue = pylta.formulas.FormulaConstant(True) #default formula for predicate
    lta = pylta.lta.LTA()
    a = pylta.variables.Layer('a')
    lta.append_layer(a)
    pred_manager = pylta.predicates.PredicateManager(lta)
    p0 = pred_manager.get_unused_pred(a)
    pred_manager[p0] = ttrue
    p1 = pred_manager.get_unused_pred(a)
    pred_manager[p1] = ttrue
    
    #tests
    dd_approx = pylta.dd_approx.DdApproximation(pred_manager, 10, 13)
    empty_path = pred_manager.abstract_path(10, 13)
    assert not list(dd_approx.enum_lower(empty_path))

    path = pred_manager.abstract_path(10, 13)
    dd_approx.unknown_path(path)
    for i, val in path.items():
        assert not val

    path[10][p0] = True
    b = dd_approx.unknown_path(path)
    assert b
    assert path[10][p0]
    assert len(path[10]) == 1
    for i in range(11, 13):
        assert not path[i]

    path[10][p1] = False
    path[11][p0] = True
    path[12][p0] = True
    dd_approx.add_path(path)
    l = list(dd_approx.enum_lower(empty_path))
    for i, val in empty_path.items():
        #Check that empty_path remains empty
        assert not val
    assert len(l) == 4
    value_pairs = set() #tests that all combinaison of other values appear
    for path in l:
        assert path[10][p0]
        assert not path[10][p1]
        assert path[11][p0]
        assert path[12][p0]
        value_pairs.add((path[11][p1], path[12][p1]))
    assert len(value_pairs) == 4
    
    path2 = pred_manager.abstract_path(10, 13)
    path2[11][p0] = True
    b = dd_approx.unknown_path(path2)
    assert b
    assert len(path2[10]) == 1
    assert not path2[10][p0]
    assert len(path2[11]) == 1
    assert path2[11][p0]
    assert not path2[12]
    dd_approx.remove_path(path2)


    while dd_approx.unknown_path(path):
        assert path[10][p0] or not path[11][p0]
        dd_approx.add_path(path)
        path = pred_manager.abstract_path(10, 13)
    
