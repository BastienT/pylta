import pytest
import spot
import pylta.formulas
import pylta.spot_translator

def test_identifier():
    layer = pylta.variables.Layer("a")
    ident = "predicate"
    pred = pylta.variables.Predicate(layer, ident)
    f = pylta.formulas.NamedPredicate(pred)
    f_spot = pylta.spot_translator.to_spot(f)
    assert str(f) == str(f_spot)

def test_identifier2():
    layer = pylta.variables.Layer("a")
    pred = pylta.variables.Predicate.from_layer(layer)
    f = pylta.formulas.NamedPredicate(pred)
    f_spot = pylta.spot_translator.to_spot(f)
    assert str(f) == str(f_spot)
    
def test_true():
    f = pylta.formulas.FormulaConstant(True)
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_tt

def test_false():
    f = pylta.formulas.FormulaConstant(False)
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_ff

def test_globally():
    layer = pylta.variables.Layer("a")
    ident = "predicate"
    pred = pylta.variables.Predicate(layer, ident)
    f = pylta.formulas.Globally(
        pylta.formulas.NamedPredicate(pred)
    )
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_G
    assert str(f_spot[0]) == str(pred)

def test_finally():
    layer = pylta.variables.Layer("a")
    ident = "predicate"
    pred = pylta.variables.Predicate(layer, ident)
    f = pylta.formulas.Finally(
        pylta.formulas.NamedPredicate(pred)
    )
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_F
    assert str(f_spot[0]) == str(pred)

def test_next():
    layer = pylta.variables.Layer("a")
    ident = "predicate"
    pred = pylta.variables.Predicate(layer, ident)
    f = pylta.formulas.Next(
        pylta.formulas.NamedPredicate(pred)
    )
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_X
    assert str(f_spot[0]) == str(pred)

def test_not():
    layer = pylta.variables.Layer("a")
    ident = "predicate"
    pred = pylta.variables.Predicate(layer, ident)
    f = pylta.formulas.Not(
        pylta.formulas.NamedPredicate(pred)
    )
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_Not
    assert str(f_spot[0]) == str(pred)

def test_and():
    layer = pylta.variables.Layer("a")
    id0 = "0"
    id1 = "1"
    id2 = "2"
    pred0 = pylta.variables.Predicate(layer, id0)
    pred1 = pylta.variables.Predicate(layer, id1)
    pred2 = pylta.variables.Predicate(layer, id2)
    f0 = pylta.formulas.NamedPredicate(pred0)
    f1 = pylta.formulas.NamedPredicate(pred1)
    f2 = pylta.formulas.NamedPredicate(pred2)
    f = pylta.formulas.And(f0, f1, f2)
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_And
    assert str(f_spot[0]) == str(f0)
    assert str(f_spot[1]) == str(f1)
    assert str(f_spot[2]) == str(f2)

def test_and2():
    f = pylta.formulas.And()
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_tt
    
def test_or():
    layer_a = pylta.variables.Layer("a")
    layer_b = pylta.variables.Layer("b")
    id0 = "0"
    id1 = "1"
    pred0 = pylta.variables.Predicate(layer_a, id0)
    pred1 = pylta.variables.Predicate(layer_b, id1)
    f0 = pylta.formulas.NamedPredicate(pred0)
    f1 = pylta.formulas.NamedPredicate(pred1)
    f = pylta.formulas.Or(f0, f1)
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_Or
    assert str(f_spot[0]) == str(f0)
    assert str(f_spot[1]) == str(f1)

def test_or2():
    f = pylta.formulas.Or()
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_ff

def test_until():
    layer = pylta.variables.Layer("a")
    id0 = "0"
    id1 = "1"
    id2 = "2"
    pred0 = pylta.variables.Predicate(layer, id0)
    pred1 = pylta.variables.Predicate(layer, id1)
    pred2 = pylta.variables.Predicate(layer, id2)
    f0 = pylta.formulas.NamedPredicate(pred0)
    f1 = pylta.formulas.NamedPredicate(pred1)
    f2 = pylta.formulas.NamedPredicate(pred2)
    f = pylta.formulas.Until(f0, f1, f2)
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_U
    assert f_spot[0].kind() == spot.op_ap
    assert f_spot[0].ap_name() == str(f0)
    assert f_spot[1].kind() == spot.op_U
    assert f_spot[1][0].ap_name() == str(f1)
    assert f_spot[1][1].ap_name() == str(f2)

def test_implies():
    layer_a = pylta.variables.Layer("a")
    layer_b = pylta.variables.Layer("b")
    id0 = "0"
    id1 = "1"
    pred0 = pylta.variables.Predicate(layer_a, id0)
    pred1 = pylta.variables.Predicate(layer_b, id1)
    f0 = pylta.formulas.NamedPredicate(pred0)
    f1 = pylta.formulas.NamedPredicate(pred1)
    f = pylta.formulas.Implies(f0, f1)
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_Implies
    assert f_spot[0].kind() == spot.op_ap
    assert f_spot[0].ap_name() == str(f0)
    assert f_spot[1].kind() == spot.op_ap
    assert f_spot[1].ap_name() == str(f1)

def test_iff():
    layer = pylta.variables.Layer("a")
    id0 = "0"
    id1 = "1"
    id2 = "2"
    pred0 = pylta.variables.Predicate(layer, id0)
    pred1 = pylta.variables.Predicate(layer, id1)
    pred2 = pylta.variables.Predicate(layer, id2)
    f0 = pylta.formulas.NamedPredicate(pred0)
    f1 = pylta.formulas.NamedPredicate(pred1)
    f2 = pylta.formulas.NamedPredicate(pred2)
    f = pylta.formulas.Iff(f0, f1, f2)
    f_spot = pylta.spot_translator.to_spot(f)
    assert f_spot.kind() == spot.op_Equiv
    # For some reason, spot reorders the operands making the
    # following tests a bit weird
    assert f_spot[1].kind() == spot.op_Equiv
    assert f_spot[1][0].ap_name() == str(f0)
    assert f_spot[1][1].ap_name() == str(f1)
    assert f_spot[0].ap_name() == str(f2)
