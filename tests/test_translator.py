import pylta.variables
import pylta.terms
import pylta.formulas
import pylta.translator

def test_param():
    n = pylta.variables.Parameter("n")
    term_n = pylta.terms.TermParameter(n)
    smt_n = pylta.translator.to_smt(term_n, index=2)
    n2 = pylta.translator.from_smt(smt_n, expected_index=2)
    # Parameters should not be labelled with the index
    assert "2" not in str(smt_n)
    assert n == n2.content

def test_state():
    layer = pylta.variables.Layer("layer2")
    state = pylta.variables.State(layer=layer, name="state_45")
    term_state = pylta.terms.TermState(state)
    smt_state = pylta.translator.to_smt(term_state, index=7)
    term_state2 = pylta.translator.from_smt(smt_state, expected_index=7)
    assert "7" in str(smt_state)
    assert state == term_state2.content

def test_edge():
    src_layer = pylta.variables.Layer("src_layer")
    src = pylta.variables.State(layer=src_layer, name="0")
    dest_layer = pylta.variables.Layer("dest_layer")
    dest = pylta.variables.State(layer=dest_layer, name="0")
    edge = pylta.variables.Edge(src=src, dest=dest)
    term_edge = pylta.terms.TermEdge(edge)
    smt_edge = pylta.translator.to_smt(term_edge, index=54)
    term_edge2 = pylta.translator.from_smt(smt_edge, expected_index=54)
    assert "54" in str(smt_edge)
    assert edge == term_edge2.content

def test_constant():
    term = pylta.terms.TermConstant(0)
    smt_term = pylta.translator.to_smt(term, index=0)
    term2 = pylta.translator.from_smt(smt_term, expected_index=0)
    assert term.value == term2.value
    
def test_constant2():
    term = pylta.terms.TermConstant(-456)
    smt_term = pylta.translator.to_smt(term, index=65)
    term2 = pylta.translator.from_smt(smt_term, expected_index=65)
    assert term.value == term2.value

def test_negation():
    term = pylta.terms.Product(
        pylta.terms.TermConstant(-1),
        pylta.terms.TermConstant(-3)
    )
    smt_term = pylta.translator.to_smt(term, index=7)
    term2 = pylta.translator.from_smt(smt_term, expected_index=7)
    assert str(term2) == "- - 3"

def test_multiplication():
    left = pylta.terms.TermConstant(0)
    n = pylta.variables.Parameter("n")
    right = pylta.terms.TermParameter(n)
    term = pylta.terms.Product(left, right)
    smt_term = pylta.translator.to_smt(term, index=45)
    term2 = pylta.translator.from_smt(smt_term, expected_index=45)
    assert str(term) == str(term2)

def test_addition():
    left = pylta.terms.TermParameter(pylta.variables.Parameter("p"))
    right = pylta.terms.TermConstant(-7)
    term = pylta.terms.Sum(left, right)
    smt_term = pylta.translator.to_smt(term, index=3)
    term2 = pylta.translator.from_smt(smt_term, expected_index=3)
    assert str(term) == str(term2)

def test_next():
    index = 64
    state = pylta.terms.TermState(
        pylta.variables.State(layer=pylta.variables.Layer("a"), name="s")
    )
    next_state = pylta.terms.NextLayer(state)
    smt_next_state = pylta.translator.to_smt(next_state, index=index)
    assert str(index+1) in str(smt_next_state)
    next_state2 = pylta.translator.from_smt(smt_next_state, expected_index=index)
    assert str(next_state) == str(next_state2)
    state2 = pylta.translator.from_smt(smt_next_state, expected_index=index+1)
    assert str(state) == str(state2)

def test_global_var():
    index = 12
    name = "name"
    bool_var = pylta.variables.BooleanVar(name)
    formula = pylta.formulas.GlobalVar(bool_var)
    smt_formula = pylta.translator.to_smt(formula, index)
    assert name in str(smt_formula)
    assert str(index) not in str(smt_formula) #global variables should not be indexed
    formula2 = pylta.translator.from_smt(smt_formula, index)
    assert formula.name == formula2.name

def test_predicate():
    index = 13
    layer = pylta.variables.Layer("foo")
    predicate = pylta.variables.Predicate(layer, "bar")
    formula = pylta.formulas.NamedPredicate(predicate)
    smt_formula = pylta.translator.to_smt(formula, index)
    assert str(predicate) in str(smt_formula)
    assert str(index) in str(smt_formula)
    formula2 = pylta.translator.from_smt(smt_formula, index)
    assert formula.predicate == formula2.predicate
    
def test_gt():
    left = pylta.terms.TermParameter(pylta.variables.Parameter("p"))
    right = pylta.terms.Sum(
        pylta.terms.TermConstant(-7),
        pylta.terms.TermState(
            pylta.variables.State(
                layer=pylta.variables.Layer("a"),
                name="s"
            )
        )
    )
    formula = pylta.formulas.GreaterThan(left, right)
    smt_formula = pylta.translator.to_smt(formula, index=1)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=1)
    # pysmt convert left > right into right < left
    assert str(formula2) == "- 7 + a.s < p"

def test_ge():
    left = pylta.terms.TermParameter(pylta.variables.Parameter("p"))
    right = pylta.terms.Sum(
        pylta.terms.TermConstant(-7),
        pylta.terms.TermState(
            pylta.variables.State(
                layer=pylta.variables.Layer("a"),
                name="s"
            )
        )
    )
    formula = pylta.formulas.GreaterEqual(left, right)
    smt_formula = pylta.translator.to_smt(formula, index=1)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=1)
    # pysmt convert left > right into right < left
    assert str(formula2) == "- 7 + a.s <= p"

def test_equal():
    left = pylta.terms.TermParameter(pylta.variables.Parameter("p"))
    right = pylta.terms.Sum(
        pylta.terms.TermConstant(-7),
        pylta.terms.TermState(
            pylta.variables.State(
                layer=pylta.variables.Layer("a"),
                name="s"
            )
        )
    )
    formula = pylta.formulas.Equal(left, right)
    smt_formula = pylta.translator.to_smt(formula, index=1)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=1)
    assert str(formula2) == str(formula)

def test_not_equal():
    left = pylta.terms.TermParameter(pylta.variables.Parameter("p"))
    right = pylta.terms.Sum(
        pylta.terms.TermConstant(-7),
        pylta.terms.TermState(
            pylta.variables.State(
                layer=pylta.variables.Layer("a"),
                name="s"
            )
        )
    )
    formula = pylta.formulas.NotEqual(left, right)
    smt_formula = pylta.translator.to_smt(formula, index=1)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=1)
    # pysmt encodes left != right with NOT left == right
    assert str(formula2) == "! p == - 7 + a.s"

def test_le():
    left = pylta.terms.TermParameter(pylta.variables.Parameter("p"))
    right = pylta.terms.Sum(
        pylta.terms.TermConstant(-7),
        pylta.terms.TermState(
            pylta.variables.State(
                layer=pylta.variables.Layer("a"),
                name="s"
            )
        )
    )
    formula = pylta.formulas.LowerEqual(left, right)
    smt_formula = pylta.translator.to_smt(formula, index=1)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=1)
    assert str(formula2) == str(formula)
    
def test_lt():
    left = pylta.terms.TermParameter(pylta.variables.Parameter("p"))
    right = pylta.terms.Sum(
        pylta.terms.TermConstant(-7),
        pylta.terms.TermState(
            pylta.variables.State(
                layer=pylta.variables.Layer("a"),
                name="s"
            )
        )
    )
    formula = pylta.formulas.LowerThan(left, right)
    smt_formula = pylta.translator.to_smt(formula, index=1)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=1)
    assert str(formula2) == str(formula)

def test_false_():
    formula = pylta.formulas.FormulaConstant(False)
    smt_formula = pylta.translator.to_smt(formula, index=4)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=4)
    assert str(formula) == str(formula2)

def test_true_():
    formula = pylta.formulas.FormulaConstant(True)
    smt_formula = pylta.translator.to_smt(formula, index=4)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=4)
    assert str(formula) == str(formula2)

def test_not():
    formula = pylta.formulas.Not(pylta.formulas.FormulaConstant(False))
    smt_formula = pylta.translator.to_smt(formula, index=4)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=4)
    assert str(formula) == str(formula2)

def test_and():
    formula = pylta.formulas.And(
        pylta.formulas.FormulaConstant(True),
        pylta.formulas.FormulaConstant(False)
    )
    smt_formula = pylta.translator.to_smt(formula, index=4)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=4)
    assert str(formula) == str(formula2)

def test_or():
    formula = pylta.formulas.Or(
        pylta.formulas.FormulaConstant(True),
        pylta.formulas.FormulaConstant(False)
    )
    smt_formula = pylta.translator.to_smt(formula, index=4)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=4)
    assert str(formula) == str(formula2)

def test_implies():
    formula = pylta.formulas.Implies(
        pylta.formulas.FormulaConstant(True),
        pylta.formulas.FormulaConstant(False)
    )
    smt_formula = pylta.translator.to_smt(formula, index=4)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=4)
    assert str(formula) == str(formula2)

def test_iff():
    formula = pylta.formulas.Iff(
        pylta.formulas.FormulaConstant(True),
        pylta.formulas.FormulaConstant(False)
    )
    smt_formula = pylta.translator.to_smt(formula, index=4)
    formula2 = pylta.translator.from_smt(smt_formula, expected_index=4)
    assert str(formula) == str(formula2)


