import pytest
import pylta.parser, pylta.commands

def test_declare_parameters():
    input_str = "PARAMETERS: n, t, f00\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.DeclareParameter)
    assert str(command) == input_str

def test_declare_parameter_relation():
    input_str = "PARAMETER_RELATION: n < 2 * t & (t > 1 | bla)\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.DeclareParameterRelation)
    assert str(command) == input_str

def test_declare_layers():
    input_str = "LAYERS: R, P, A\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.DeclareLayers)
    assert str(command) == input_str

def test_declare_states():
    input_str = "STATES: R.0, R.sadwe, P.test\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.DeclareStates)
    assert str(command) == input_str
    
def test_rule():
    input_str = "CASE R.a0:\n  IF R.a0 + R.1 * 2 - (3 + 4) > n -> TRUE "\
        "THEN p.q\n  IF - r.a1 * (7 + 3) != d & a THEN q.state\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.Rule)
    assert str(command) == input_str

def test_bind_term():
    input_str = "TERM big_term: 2 * (n - - 3 * (foo - bar)) - (2 - 1)\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.BindTerm)
    assert str(command) == input_str
    
def test_bind_formula():
    input_str = "FORMULA f0: x - 2 + 3 > 3 * t U 2 * (y - 1 - x) <= "\
        "- 2 * n U X G F FALSE | ! ! TRUE\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.BindFormula)
    assert str(command) == input_str

def test_bind_formula2():
    input_str = "FORMULA a_01: EDGE(p.a, r.qwerty) >= 3\n"
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.BindFormula)
    assert str(command) == input_str

def test_verify():
    input_str = ("WITH\n"
                 "  init.all0: init.0 + f == n & t > 0\n"
                 "  inter.enough: inter.s + f > t\n"
                 "  acc.one: acc.s > 0\n"
                 "VERIFY: init.all0 U X X ! acc.one\n")
    commands = pylta.parser.parse(input_str)
    assert len(commands) == 1
    command = commands[0]
    assert isinstance(command, pylta.commands.Verify)
    assert str(command) == input_str
