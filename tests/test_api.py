import pylta.api as ltapi

def test_rel_broadcast():
    lta = ltapi.lta()

    n = lta.add_parameter()
    t = lta.add_parameter()
    f = lta.add_parameter()

    lta.add_parameter_relation(2*t < n)
    lta.add_parameter_relation(f <= t)
    
    initial = lta.add_layer()
    ini_0 = lta.add_state(initial)
    ini_1 = lta.add_state(initial)
    
    inter = lta.add_layer()
    inter_s = lta.add_state(inter)

    acc = lta.add_layer()
    acc_s = lta.add_state(acc)
    
    edge_ini0 = lta.get_edge(ini_0, inter_s)
    edge_ini1 = lta.get_edge(ini_1, inter_s)
    edge_inter = lta.get_edge(inter_s, acc_s)

    lta.set_guard(edge_ini0, ini_1 + f > t)
    lta.set_guard(edge_ini1, ltapi.true())
    lta.set_guard(edge_inter, inter_s + f >= n - t)

    assert len(list(lta.parameters())) == 3
    assert set([str(p) for p in lta.parameters()]) == set([str(n), str(t), str(f)])

    assert len(list(lta.get_parameter_relations())) == 2
    assert str(list(lta.get_parameter_relations())[0]) == str(2*t < n)
    assert str(list(lta.get_parameter_relations())[1]) == str(f <= t)
    
    assert len(list(lta.layers())) == 3 + 1 #an empty layer is always added
    assert str(lta.first_layer()) == str(initial)
    assert str(lta.next_layer(initial)) == str(inter)
    assert str(lta.next_layer(inter)) == str(acc)
    empty_layer = lta.get_layer("")
    assert str(lta.next_layer(acc)) == str(empty_layer)
    assert str(lta.next_layer(empty_layer)) == str(empty_layer)

    assert len(list(lta.states(initial))) == 2
    assert set([str(s) for s in lta.states(initial)]) == set([str(ini_0), str(ini_1)])
    assert len(list(lta.states(inter))) == 1
    assert str(list(lta.states(inter))[0]) == str(inter_s)
    assert len(list(lta.states(acc))) == 1
    assert str(list(lta.states(acc))[0]) == str(acc_s)
    assert len(list(lta.states(empty_layer))) == 0

    assert len(list(lta.out_edges(ini_0))) == 1
    assert str(list(lta.out_edges(ini_0))[0]) == str(edge_ini0)
    assert len(list(lta.out_edges(ini_1))) == 1
    assert str(list(lta.out_edges(ini_1))[0]) == str(edge_ini1)
    assert len(list(lta.out_edges(inter_s))) == 1
    assert str(list(lta.out_edges(inter_s))[0]) == str(edge_inter)
    assert len(list(lta.out_edges(acc_s))) == 0
    
    assert str(lta.get_guard(edge_ini0)) == str(ini_1 + f > t)
    assert str(lta.get_guard(edge_ini1)) == str(ltapi.true())
    assert str(lta.get_guard(edge_inter)) == str(inter_s + f >= n - t)

    checker = ltapi.checker(lta)
    initial_cond = checker.add_predicate(initial, ini_0 + ini_1 + f == n)
    all0 = checker.add_predicate(initial, ini_0 + f == n)
    one_acc = checker.add_predicate(acc, acc_s > 0)

    
