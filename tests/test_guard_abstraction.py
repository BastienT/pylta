import pytest

import pylta.variables
import pylta.paths
import pylta.exceptions
import pylta.parser
import pylta.commands
import pylta.interpreter
import pylta.guard_abstraction

def test_rel_broadcast():
    input_str = ("PARAMETERS: n, t, f\n"
                 "PARAMETER_RELATION: 2*t < n & f <= t\n"
                 "LAYERS: init, inter, acc\n"
                 "STATES: init.0, init.1, inter.s, acc.s\n"
                 "CASE init.0:\n"
                 "  IF init.1 + f > t THEN inter.s\n"
                 "CASE init.1:\n"
                 "  IF TRUE THEN inter.s\n"
                 "CASE inter.s:\n"
                 "  IF inter.s + f >= n - t THEN acc.s\n")
    program = pylta.parser.parse(input_str)
    interp = pylta.interpreter.Interpreter()
    interp.run(program)

    n = pylta.variables.Parameter("n")
    assert interp.lta.is_parameter(n)
    t = pylta.variables.Parameter("t")
    assert interp.lta.is_parameter(t)
    f = pylta.variables.Parameter("f")
    assert interp.lta.is_parameter(f)
    
    init = pylta.variables.Layer("init")
    assert interp.lta.is_layer(init)
    init0 = pylta.variables.State(init, "0")
    assert interp.lta.is_state(init0)
    init1 = pylta.variables.State(init, "1")
    assert interp.lta.is_state(init1)
    
    acc = pylta.variables.Layer("acc")
    assert interp.lta.is_layer(acc)
    acc_s = pylta.variables.State(acc, "s")
    assert interp.lta.is_state(acc_s)

    ga = pylta.guard_abstraction.GuardAbstraction(interp.lta, 0, 3)
    
    p_init = pylta.variables.Predicate(init, "initial")
    f_init = pylta.formulas.Equal(
        pylta.terms.Sum(
            pylta.terms.TermState(init0),
            pylta.terms.TermState(init1),
            pylta.terms.TermParameter(f)
        ),
        pylta.terms.TermParameter(n)
        )
    ga.add_predicate(p_init, f_init)

    p_one1 = pylta.variables.Predicate(init, "one1")
    f_one1 = pylta.formulas.GreaterThan(
        pylta.terms.TermState(init1),
        pylta.terms.TermConstant(0)
    )
    ga.add_predicate(p_one1, f_one1)

    p_acc = pylta.variables.Predicate(acc, "one")
    f_acc = pylta.formulas.GreaterThan(
        pylta.terms.TermState(acc_s),
        pylta.terms.TermConstant(0)
    )
    ga.add_predicate(p_acc, f_acc)


    path = ga.predicates.abstract_path(0, 3)
    path[0][p_init] = True
    path[2][p_acc] = True
    sat, completed_path = ga.complete_valuation(path)
    assert sat
    assert completed_path[0][p_init]
    assert completed_path[2][p_acc]
    assert p_one1 in completed_path[0]
    concrete_path = ga.concretise(path)
    assert 2 * concrete_path.get_param_value(t) < concrete_path.get_param_value(n)
    assert concrete_path.get_param_value(f) <= concrete_path.get_param_value(t)
    assert (concrete_path[0].get_state_value(init0) +
            concrete_path[0].get_state_value(init1) +
            concrete_path.get_param_value(f) ==
            concrete_path.get_param_value(n))
    assert concrete_path[2].get_state_value(acc_s) > 0
    
    path[0][p_one1] = False
    sat, core = ga.complete_valuation(path)
    assert not sat
    assert not core[0][p_one1]
    assert core[2][p_acc]
    assert p_init not in core[0]
